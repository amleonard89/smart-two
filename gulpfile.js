var gulp = require('gulp');
var server = require( 'gulp-develop-server');
var exec = require('child_process').exec;

gulp.task( 'start', function() {
    server.listen( { path: './server.js' } );
});

gulp.task( 'restart', function() {
    gulp.watch( [ './app.js' ], server.restart );
});

gulp.watch( [
    './views/**/*.html',
    './views/**/*.css',
    './libs/**/*.js',
    './libs/*.js',
    './router/**/*.js',
    './*.js'
], server.restart );