var assert = require('assert');
var funcRes = require('./libs/helpers.js');

describe('timeConversion', function() {
    it('can properly convert times to objects', function(done){
        var time1 = '8:00 AM';
        var time2 = '12:00 PM';
        var time3 = '12:00 AM';
        var time4 = '7:30 PM';
        var time5 = '1:30 PM';
        var time6 = '1:30 AM';
        
        assert.deepEqual(funcRes.getHoursMinutes(time1), { h : 8, m : 0});
        assert.deepEqual(funcRes.getHoursMinutes(time2), { h : 12, m : 0});
        assert.deepEqual(funcRes.getHoursMinutes(time3), { h : 0, m : 0});
        assert.deepEqual(funcRes.getHoursMinutes(time4), { h : 19, m : 30});
        assert.deepEqual(funcRes.getHoursMinutes(time5), { h : 13, m : 30});
        assert.deepEqual(funcRes.getHoursMinutes(time6), { h : 1, m : 30});
        
        done();
    });
});