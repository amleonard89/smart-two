var cfg = require(config + '/app');
var logger = require(libs + '/logger');

module.exports = function (app) {

    app.use('/'+cfg.adminPath, require('./routes/admin'));
    
    app.post('/isAvailable', function(req, res){
        var selloutModel = require(libs + '/model/sellout');

        var date = req.body.date;
        var time = req.body.time;
        var seats = +req.body.seats;
        
        selloutModel
         .isDatetimeAvailable(date, time, seats)
         .then(function(){
             res.send({ valid : true });        
         },
         function (err){
             var message = 'There was an error!';
             
             // HEY LISTEN. these hard-coded strings should be put in the database
             // or in a config file
             switch (err.message) {
                 case 'error_fully_booked':
                     message = 'Sorry, this time may be sold out.  Please call 855-200-7678 for availability.';
                     break;
                 
                 case 'error_over_book':
                     message = 'Sorry, there are only ' + err.leftover + ' seats left for this reservation time.  Please call 855-200-7678 for availability.';
                     break;
             }
             
             res.send({ valid : false, message: message });
         });
    });
    
    app.post('/reservation', function(req, res){
        var workshop = require(libs + '/workshop');
        
        var bookingData = req.body.booking;
        var cardData = req.body.card;

        workshop
         .processFrontEnd(bookingData, cardData)
         .then(function(data){
             logger.frontend.info(data.booking.toObject());
             logger.frontend.info(data.email);
             
             res.send({ error : false, confirmationCode : data.booking.confirmationCode });
        })
        .catch(function(err){
            console.log(err.stack);
            
            logger.frontend.error(err);
            logger.frontend.error(bookingData);
            
            res.send({ error : err.name, message : err.message });
        });
    });
};