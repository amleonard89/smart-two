var cfg = require(config + '/app');
var bodyParser = require('body-parser');

module.exports = function (app) {
    
    app.use('/'+cfg.adminPath+'schedule/pdf', bodyParser.json({limit: '16mb'}));
    app.use('/'+cfg.adminPath+'schedule/pdf', bodyParser.urlencoded({limit: '16mb', extended: true}));

};