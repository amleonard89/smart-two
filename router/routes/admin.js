    var express = require('express');
    var parse = require('parse').Parse;
    var Q = require('q');
    var moment = require('moment-timezone');
    
    var cfg = require(config + '/app');
    var workshop = require(libs+'/workshop');
    
    var bookingsModel = require(libs+'/model/booking');
    var tripsModel = require(libs+'/model/trip');
    var selloutModel = require(libs+'/model/sellout');
        
    var router = express.Router();
    
    /***********
     * Authenetication middleware for all authenticated routes
     */
    var adminAuth = function(req, res, next) {
        if (parse.User.current()) {
            next(); return;
        }
        else if (req.cookies._st_userToken) {
            parse.User.become(req.cookies._st_userToken)
            .then(function(user){
                next();
            }, function(error){
                res.redirect('/'+cfg.adminPath+'/login');
                res.end();
            });
        }
        else {
            res.redirect('/'+cfg.adminPath+'/login');
            res.end();
        }
    };
    
    /*************
     * Non-authenticated routes
     */
    
    router.get('/login', function (req, res, next) {
        res.clearCookie('_st_userToken', { path: '/', httpOnly: false});
        res.render('admin/login');
    });


    /*************
     * Authneticated routes
     */
    
    // Main pages
    router.get(['/', '/create', '/create/:code'], adminAuth, function(req, res, next){
        workshop.getWorkingData().then(function(data) {
            var viewData = {
                locationData : data.locations,
                locKeys : data.location_key,
                priceData : data.prices,
                linkData : data.origins,
                page : 'create'
            };
            
            if (req.params.code)
                viewData.code = req.params.code;
            
            res.render('admin/create', viewData);
        });
    });

    router.get('/view', adminAuth, function(req, res, next){
        res.render('admin/view', {page : 'view'});
    });

    router.get('/sellout', adminAuth, function(req, res, next){
        res.render('admin/sellout', {});
    });

    router.get('/schedule', adminAuth, function(req, res, next){
        res.render('admin/schedule', {});
    });


    // General purpose
    router.get('/workshop/data', adminAuth, function(req, res, next){
        workshop.getWorkingData()
            .then(function(data) {
                res.json(data);
                res.end();
            });
    });
    
    router.get('/workshop/times', adminAuth, function(req, res, next){
        workshop.getAllTimes()
            .then(function(data) {
                res.json(data);
                res.end();
            },
            function(err){
                console.log(err);
            });
    });
    
    router.get('/datatable/tripDataTable', adminAuth, function(req, res, next){
        var datatable = require(libs+'/datatable');
        
        datatable.tripDataTable(req.query).then(function(data){
            res.send(data);
        });
    });
    
    router.get('/datatable/bookingDataTable', adminAuth, function(req, res, next){
        var datatable = require(libs+'/datatable');
        
        datatable.bookingDataTable(req.query).then(function(data){
            res.send(data);
        });
    });
    
    // Booking and trip related
   
    router.post('/booking', adminAuth, function(req, res, next) {
        var bData = req.body.booking;
        var pData = req.body.payment;

        bookingsModel
         .addFull(bData, pData)
         .then(function(data){
             //console.log(data);
             var typeStr = data.booking.quote ? 'Quote' : 'Booking';
             
             data.message = [typeStr+' Successfully Saved'];
             
             if (data.payment) {
                 require(libs+'/email')
                  .sendEmail(data.booking)
                  .then(function(email){
                      data.message.push('E-Mail Successfully Sent');
              
                      res.json(data);
                 },
                 function(err){
                     console.log('/booking E-Mail Error');
                     console.log(err);
                     
                     res.json(data);
                 });
             }
             else {
                res.json(data);
            }
        })
        .catch(function(err){
            console.log(err);
            err.error = true;
            res.json(err);
        });
    });

    router.get('/booking/:code', adminAuth, function(req, res, next) {
        var code = req.params.code;

        bookingsModel.getByCode(code).then(function(data){
            res.json({booking : data});
        });
    });

    router.post('/booking/:code/note', adminAuth, function(req, res, next) {
        var code = req.params.code;
        var notes = req.body.notes;

        bookingsModel.findOneAndUpdate({confirmationCode : code},
            { notes : notes },
            { new : true },
            function(err, data){
                if (err)
                    console.log(err);

                res.json(data);
                res.end();
            });
    });

    router.post('/booking/:code/email', adminAuth, function(req, res, next) {
        var email = require(libs + '/email');

        var code = req.params.code;
        var title = req.body.title;

        email.sendEmail(code, title)
         .then(function(data){
             res.json({ message : 'Email Successfully Sent'});
         },
         function(err){
             res.json(err);
         });
    });

    router.post('/booking/:code/cancel', adminAuth, function(req, res, next) {
        var email = require(libs + '/email');
        
        var code = req.params.code;
        var validCode = req.body.code;

        if (code != validCode)
            return;
        
        bookingsModel.findOneAndUpdate({confirmationCode : code},
            { cancelled : true },
            { new : true },
            function(err, data){
                if (err)
                    console.log(err);
                
                var response = {
                    booking : data,
                    message : ["Reservation Successfully Canceled"]
                };
                
                email.sendEmail(code, 'Your Cancelled Shuttle Reservation')
                 .then(function(data){
                    response.message.push('Email Successfully Sent');
            
                    res.json(response);
                    res.end();  
                },
                function(err){
                    res.json(err);
                });              
            });
    });
    
    router.post('/trips/dates', adminAuth, function(req, res, next) {
        var dates = req.body.dates || [];

        var start = [];
        var end = [];

        for (var i=0; i<dates.length; i++) {
            var _date = dates[i].date;

            if (+dates[i].time) {
                start.push(moment.tz(_date, cfg.TZ).toDate());
                end.push(null);
            }
            else {
                start.push(moment.tz(_date, cfg.TZ).startOf('day').toDate());
                end.push(moment.tz(_date, cfg.TZ).endOf('day').toDate());
            }
        }
        
        tripsModel
        .byDateRange(start, end, {
            subTrips : false,
            where : {
                quote : { $ne : true },
                cancelled : { $ne : true }
            }
        })
        .then(function(data){
            res.json({trips : data});
            res.end();
        })
        .catch(function(err){
            console.log(err);
            console.log(err.stack);
        });
    });

    // Sellout related
    router.post('/sellout/add', adminAuth, function(req, res, next) {
        var data = req.body.entry;
        
        data._id = data._id || (new (require('mongoose').Types.ObjectId));
        
        selloutModel.findOneAndUpdate(
            { _id : data._id },
            data,
            { 'upsert' : true, 'new' : true }
         )
         .then(function(sellout){
             res.json(sellout);
         }, function(err){
             console.log(err);
             console.log(err.stack);
         });
    });
    
    router.get('/sellout/delete/:id', adminAuth, function(req, res, next) {
        var id = req.params.id;
        
        selloutModel.findOneAndRemove({ _id : id })
        .then(function(sellout){
            res.json(sellout);
        });
    });
    
    router.get('/sellout/byDates', adminAuth, function(req, res, next) {
        var start = moment.tz(req.query.start, 'YYYY-M-D', cfg.TZ);
        var end = req.query.end ?
            moment.tz(req.query.end, 'YYYY-M-D', cfg.TZ) :
            moment.tz(start, cfg.TZ).endOf('day');
        
        selloutModel
        .byDateRange(start.toDate(), end ? end.toDate() : null)
        .then(function(data){ 
            res.json(data);
        });
    });
    
    router.get('/sellout/calendar', adminAuth, function(req, res, next) {
        var start = moment.tz(req.query.start, 'YYYY-M-D', cfg.TZ);
        var end = moment.tz(req.query.end, 'YYYY-M-D', cfg.TZ);

        selloutModel
        .byDateRange(start.toDate(), end.toDate())
        .then(function(data){ 
            var events = [];

            for (var i=0; i<data.length; i++) {
                var entry = data[i];

                var _start = moment.tz(entry.start, cfg.TZ);
                var _end = moment.tz(entry.end, cfg.TZ);
                
                // More DST joy
                if (_start.isDST()) _start.subtract(1, 'h');
                if (_end.isDST()) _end.subtract(1, 'h');
                
                events.push({
                    id : entry._id,
                    title : entry.seats + ' Seats',
                    start : _start.format(),
                    end : _end.format()
                });
            }

            res.json(events);
        });
    });
    
    // Staff related
    router.get('/staff', adminAuth, function(req, res, next){
        var staff = require(libs+'/model/staff');
        
        staff.getDriversNStaff().then(function(data) {
            res.send(data);
        });
    });
    
    // Schedule related
    router.post('/driver/:id', adminAuth, function(req, res, next){
        var tripsModel = require(libs+'/model/trip');
        
        var tripId = req.params.id;
        
        var drivers = req.body.drivers;
        var driverNotes = req.body.driverNotes;
                
        tripsModel.findOne({ _id : tripId}, function(err, doc){
            doc.saveDrivers(drivers, driverNotes);
        });
    });
 
    router.post('/scheduleNotes/:id', adminAuth, function(req, res, next){
        var tripsModel = require(libs+'/model/trip');
        
        var tripId = req.params.id;
        
        var scheduleNotes = req.body.scheduleNotes;
                
        tripsModel.findOneAndUpdate(
            { _id : tripId},
            { scheduleNotes : scheduleNotes },
            { new : true }, 
            function(err, doc){
                res.json(doc);
            });
    });
    
    router.post('/scheduleStaff/date', adminAuth, function(req, res, next){
        var scheduleStaffModel = require(libs+'/model/scheduleStaff');
        
        var dayUnix = +req.query.day;
        var entries = req.body.staff;
        
        scheduleStaffModel.saveByDate(dayUnix, entries)
         .then(function(data){
             res.json(data);
        });
    });
    
    router.get('/scheduleDays', adminAuth, function(req, res, next){
        var scheduleStaffModel = require(libs+'/model/scheduleStaff');
        var tripsModel = require(libs+'/model/trip');
        
        var start = req.query.start;
        var end = req.query.end;

        var startDate = moment.tz(start, 'M/D/YYYY', 'America/New_York');
        var endDate = moment.tz(end, 'M/D/YYYY', 'America/New_York').endOf('day');
        
        //in the database dates are stored by offset, not by actual daytime, so we manually compensate
        if (startDate.isDST()) startDate.add(1, 'h');
        if (endDate.isDST()) endDate.add(1, 'h');
        
        Q.spread([
            scheduleStaffModel.byDateRange(startDate.toDate(), endDate.toDate()),
            tripsModel.byDateRange(startDate.toDate(), endDate.toDate(), {
                where: {
                    quote : {$ne : 1},
                    cancelled : {$ne : 1}
                }
            })
        ],
        function(scheduleStaff, trips){
            res.send({
                trips    : trips,
                schedule : scheduleStaff
            });
        });      
    });
    
    router.post('/schedule/pdf', adminAuth, function(req, res, next){
        var pdf = require(libs+'/schedule');
        
        var start = req.body.start;
        var end = req.body.end;
        var html = req.body.html;
        
        pdf.makeSchedulePDF(start, end, html)
            .then(function(data){
            
            res.json({
                link : data.link
            });
        });
    });
    
    module.exports = router;
