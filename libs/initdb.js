var db;
var dbCfg = require(config + '/db');
var mongoose = require('mongoose');
var DataTable = require('mongoose-datatable');

mongoose.connect(dbCfg.url);
mongoose.Promise = require('q').Promise; // IMPORTANT. remember we will use Q's .catch instead of MPromise onReject
mongoose.plugin(DataTable.init);

db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
    console.log('Mongoose Connection Established ('+dbCfg.url+')');
});

module.exports = {
    connection : db,
    url : dbCfg.url
};