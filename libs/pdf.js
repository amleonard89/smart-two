var cfg = require(config+'/app');

var Q = require('q');
var wkhtmltopdf = require('wkhtmltopdf');

exports.htmlToPDF = function(filename, html, options) {
    var pdf = require('wkhtmltopdf');
    var def = Q.defer();
    
    if (cfg.pdf_command) // used mostly on a local windows server
        pdf.command = cfg.pdf_command;
    
    if (!options)
        options = {};
    
    var path = 'dl/' + filename + (filename.indexOf('.pdf') === -1 ? '.pdf' : '');
    var output = cfg.staticPath + path;
    
    options.output = base + cfg.staticPath + path;
    
    wkhtmltopdf(html, options,
        function(code, signal) {
            def.resolve({
                link : path,
                path : output,
                code : code,
                signal : signal
            });
        }
    );  
        
    return def.promise;
};