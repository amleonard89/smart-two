var mongoose = require('mongoose');

var schemaPrice = new mongoose.Schema({
    from            : String, //schemaLocation.key
    to              : String, //schemaLocation.key
    passengers      : Number,
    price           : Number,
    specials        : [{
        to               : [String], //schemaLocation.key
        from             : [String], //schemaLocation.key
        price            : Number
    }]
});
    
    
module.exports = mongoose.model('price', schemaPrice);