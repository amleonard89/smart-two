var mongoose = require('mongoose');
var _ = require('lodash');

var cfg = require(config+'/app');
var s2 = require(libs+'/helpers');
var types = require(libs+'/travel_types');
var payment = require(libs+'/payment');
var logger = require(libs + '/logger');

var tripsModel = require(libs+'/model/trip');
var placesModel = require(libs+'/model/place');

var schemaBooking = new mongoose.Schema({
    name                    : { type: String, required : true },
    email                   : [String],
    phoneNumber             : [{type : String, trim : true }],
    specialRequest          : String,
    confirmationCode        : String,
    totalPrice              : Number,
    totalPaid               : Number,
    dateCreated             : {type: Date, default: Date.now},
    trips                   : [{
            type       :   mongoose.Schema.Types.ObjectId,
            ref        :   'trips'
    }],
    referral                : String,
    notes                   : String,
    payment                 : [{
            paidAmount  : Number,
            paidDate    : Date,
            authNum     : String,
            transTag    : String
    }],
    quote                   : { type: Boolean, default: 0 },
    cancelled               : { type: Boolean, default: 0 },
    legacy                  : Boolean
}); 

var postSave = function(doc) {
    doc.populate('trips', function(err, data){

        for (var i=0; i<data.trips.length; i++) {
            var trip = data.trips[i];
            
            trip.dateCreated = data.dateCreated;
            trip.quote = data.quote;
            trip.cancelled = data.cancelled;

            trip.save();
        }
    });
};

schemaBooking.post('save', postSave);
schemaBooking.post('findOneAndUpdate', postSave);

schemaBooking.statics.addFrontEnd = function(data, paymentInfo){
    var model = this;

    return placesModel
     .placesByString()
     .then(function(placeStrings){
        var travelTypeId = +data.type;
        var numberOfTravelers = +data.numberOfTravelers;

        var places = placeStrings.places;
        var locations = placeStrings.locations;
        
        var phone = data.phoneNumber; //console.log(this);
        
        var booking = new model({
            name                    : data.name,
            email                   : data.email,
            phoneNumber             : typeof phone === 'string' ? phone.trim().replace(/\s/g, '').split(/[\s\,\/]/g) : phone,
            specialRequest          : data.specialRequest,
            confirmationCode        : data.confirmationCode,
            totalPrice              : +data.price,
            dateCreated             : ('dateCreated' in data) ? new Date(data.dateCreated) : new Date(Date.now()),
            trips                   : [],
            legacy                  : true
        });

        if (!booking.confirmationCode) {
            booking.confirmationCode = s2.createCode();
        }
        
        if (!booking.totalPrice || isNaN(booking.totalPrice)) {
            booking.totalPrice = s2.getPrice(numberOfTravelers, travelTypeId);
        }

        var TZ =  ' GMT' + cfg.TZ_offset;
        switch (travelTypeId) {
            case types.CRUISE_SHIP_TO_AIRPORT:
                var dateTime = new Date(data.cruiseInfo.date+' '+data.cruiseInfo.time+TZ);

                var cruiseKey = String(data.cruiseInfo.ship).toLowerCase().replace(/[^A-Za-z0-9]/g, '');
                var airKey = String(data.airline).toLowerCase().replace(/[^A-Za-z0-9]/g, '');

                var tripId = mongoose.Types.ObjectId();
                booking.trips.push(tripId);

                var trip = new tripsModel({
                    _id                 : tripId,
                    dateTime            : dateTime,
                    booking             : booking._id,
                    origin                  : {
                        location : places[cruiseKey] ? places[cruiseKey].loc : locations.cruise,
                        place : places[cruiseKey] ? places[cruiseKey].id : null
                    },
                    destination         : {
                        location : places[airKey] ? places[airKey].loc : locations.airport,
                        place : places[airKey] ? places[airKey].id : null
                    },
                    dateCreated : booking.dateCreated,
                    travelers : numberOfTravelers
                });

                trip.save();

                break;

            case types.AIRPORT_TO_CRUISE_SHIP:
                var dateTime = new Date(data.airportInfo.date+' '+data.airportInfo.time+TZ);

                var cruiseKey = String(data.cruiseShip).toLowerCase().replace(/[^A-Za-z0-9]/g, '');
                var airKey = String(data.airportInfo.airline).toLowerCase().replace(/[^A-Za-z0-9]/g, '');

                var tripId = mongoose.Types.ObjectId();
                booking.trips.push(tripId);

                var trip = new tripsModel({
                    _id                 : tripId,
                    dateTime            : dateTime,
                    booking             : booking._id,
                    origin                  : {
                        location : places[airKey] ? places[airKey].loc : locations.airport,
                        place : places[airKey] ? places[airKey].id : null,
                        custom : {
                            customFlightnum : data.airportInfo.flightNumber
                        }
                    },
                    destination         : {
                        location : places[cruiseKey] ? places[cruiseKey].loc : locations.cruise,
                        place : places[cruiseKey] ? places[cruiseKey].id : null
                    },
                    dateCreated : booking.dateCreated,
                    travelers : numberOfTravelers
                });

                trip.save();

                break;

            case types.ROUNDTRIP_CRUISE_AIRPORT:
                var dateTime = new Date(data.airportInfo.date+' '+data.airportInfo.time+TZ);
                var dateTime2 = new Date(data.cruiseInfo.date+' '+data.cruiseInfo.time+TZ);

                var cruiseKey = String(data.cruiseInfo.ship).toLowerCase().replace(/[^A-Za-z0-9]/g, '');
                var airKey = String(data.airportInfo.airline).toLowerCase().replace(/[^A-Za-z0-9]/g, '');

                var tripId = mongoose.Types.ObjectId();
                var tripId2 = mongoose.Types.ObjectId();

                booking.trips.push(tripId);
                booking.trips.push(tripId2);

                var trip = new tripsModel({
                    _id                 : tripId,
                    dateTime            : dateTime,
                    booking             : booking._id,
                    origin                  : {
                        location : places[airKey] ? places[airKey].loc : locations.airport,
                        place : places[airKey] ? places[airKey].id : null,
                        custom : {
                            customFlightnum : data.airportInfo.flightNumber
                        }
                    },
                    destination         : {
                        location : places[cruiseKey] ? places[cruiseKey].loc : locations.cruise,
                        place : places[cruiseKey] ? places[cruiseKey].id : null
                    },
                    dateCreated : booking.dateCreated,
                    travelers : numberOfTravelers
                });

                var trip2 = new tripsModel({
                     _id                 : tripId2,
                    dateTime             : dateTime2,
                    booking              : booking._id,
                    origin                  : {
                        location : places[cruiseKey] ? places[cruiseKey].loc : locations.cruise,
                        place : places[cruiseKey] ? places[cruiseKey].id : null
                    },
                    destination         : {
                        location : places[airKey] ? places[airKey].loc : locations.airport,
                        place : places[airKey] ? places[airKey].id : null
                    },
                    dateCreated : booking.dateCreated,
                    travelers : numberOfTravelers
                });

                trip.save();
                trip2.save();

                break;

            case types.AIRPORT_TO_HOTEL:
                var dateTime = new Date(data.airportInfo.date+' '+data.airportInfo.time+TZ);

                var hotelKey = String(data.hotel).toLowerCase().replace(/[^A-Za-z0-9]/g, '');
                var airKey = String(data.airportInfo.airline).toLowerCase().replace(/[^A-Za-z0-9]/g, '');

                var tripId = mongoose.Types.ObjectId();
                booking.trips.push(tripId);

                var trip = new tripsModel({
                    _id                 : tripId,
                    dateTime            : dateTime,
                    booking             : booking._id,
                    origin                  : {
                        location : places[airKey] ? places[airKey].loc : locations.airport,
                        place : places[airKey] ? places[airKey].id : null,
                        custom : {
                            customFlightnum : data.airportInfo.flightNumber
                        }
                    },
                    destination         : {
                        location : places[hotelKey] ? places[hotelKey].loc : locations.hotel,
                        place : places[hotelKey] ? places[hotelKey].id : null
                    },
                    dateCreated : booking.dateCreated,
                    travelers : numberOfTravelers
                });

                trip.save();
                break;

            case types.THREE_WAY_TRIP:
                var dateTime = new Date(data.airportInfo.date+' '+data.airportInfo.time+TZ);
                var dateTime2 = new Date(data.cruiseInfo.date+' '+data.cruiseInfo.time+TZ);

                var cruiseKey = String(data.cruiseInfo.ship).toLowerCase().replace(/[^A-Za-z0-9]/g, '');
                var airKey = String(data.airportInfo.airline).toLowerCase().replace(/[^A-Za-z0-9]/g, '');
                var hotelKey = String(data.hotel).toLowerCase().replace(/[^A-Za-z0-9]/g, '');

                var tripId = mongoose.Types.ObjectId();
                var tripId2 = mongoose.Types.ObjectId();

                booking.trips.push(tripId);
                booking.trips.push(tripId2);

                var trip = new tripsModel({
                    _id                 : tripId,
                    dateTime            : dateTime,
                    booking             : booking._id,
                    origin                  : {
                        location : places[airKey] ? places[airKey].loc : locations.airport,
                        place : places[airKey] ? places[airKey].id : null,
                        custom : {
                            customFlightnum : data.airportInfo.flightNumber
                        }
                    },
                    destination         : {
                        location : places[hotelKey] ? places[hotelKey].loc : locations.hotel,
                        place : places[hotelKey] ? places[hotelKey].id : null
                    },
                    dateCreated : booking.dateCreated,
                    travelers : numberOfTravelers
                });

                var trip2 = new tripsModel({
                    _id                 : tripId2,
                    dateTime            : dateTime2,
                    booking             : booking._id,
                    origin                  : {
                        location : places[cruiseKey] ? places[cruiseKey].loc : locations.cruise,
                        place : places[cruiseKey] ? places[cruiseKey].id : null
                    },
                    destination         : {
                        location : places[airKey] ? places[airKey].loc : locations.airport,
                        place : places[airKey] ? places[airKey].id : null
                    },
                    dateCreated : booking.dateCreated,
                    travelers : numberOfTravelers
                });

                trip.save();
                trip2.save();
                break;
        }

        // Handled by workshop now
        /*if (paymentInfo) {
            if (paymentInfo.transaction_approved == 1) {
                booking.addPayment(
                    paymentInfo.amount,
                    paymentInfo.authorization_num,
                    paymentInfo.transaction_tag
                );
            }
            else {
                if (trip) trip.remove();
                if (trip2) trip2.remove();
                
                throw {
                    name: 'PaymentError',
                    message: paymentInfo.bank_message
                };
            }
        }*/

        return booking
         .save()
         .then(function (doc) {
            return doc;
         },
         function(err){
            if (trip) trip.remove();
            if (trip2) trip2.remove();
            
            throw err;
        });
    });
};

schemaBooking.statics.addFull = function(bData, pData) {
    var booking;
    var bookingModel = this;
    
    var getDoc = this
            .findOne({_id : bData._id})
            .populate('trips')
            .exec();

    return getDoc.then(
    function(doc) {
        booking = doc ? doc : new bookingModel();

        booking.name                  = bData.data.name;
        booking.email                 = bData.data.email;
        booking.phoneNumber           = bData.data.phoneNumber;
        booking.specialRequest        = bData.data.specialRequest;
        booking.referral              = bData.data.referral;
        
        if (!booking.dateCreated)
            booking.dateCreated       = new Date();

        if (bData.data.hasOwnProperty('notes')) {
            booking.notes = bData.data.notes;
        }
        
        if (bData.quote)
            booking.quote = true;
        
        var bookingId = null;

        if (bData._id) {
            bookingId = bData._id;
        }
        else {
            booking.confirmationCode = s2.createCode();
        }
        
        booking._original = booking.toObject();
        
        var log = [];
        
        var totalPrice = 0;
        
        var trips = [];
        var tripIds = [];
        var deleteIds = [];
        var tripIndex = {};
        
        for (var i=0; i<bData.trips.length; i++) {
            var _id = bData.trips[i].id;
            
            tripIndex[_id] = i;
        }

        for (var j=0; j<booking.trips.length; j++) {
            var _id = booking.trips[j]._id.toString();
            
            if (!(_id in tripIndex)) {
                deleteIds.push(_id);
                
                log.push({
                    path : 'trips',
                    addition: -1,
                    valOld : booking.trips[j].dateTime
                });
            }
        }
        
        for (var i in bData.trips) {
            var tripId = bData.trips[i].id;
            var trip = bData.trips[i].data;
            var datetime = new Date(trip.date.dateStr + ' '+trip.date.timeStr + ' '+cfg.TZ_offset);
            
            totalPrice += trip.priceNum;

            var newTrip = tripId && booking.trips[tripIndex[tripId]] ?
                    booking.trips[tripIndex[tripId]] :
                    new tripsModel();
            
            newTrip._original = newTrip.toObject();
            
            newTrip.dateTime    = datetime;
            newTrip.booking     = booking._id;
            newTrip.priceId     = trip.priceId ? trip.priceId : null;
            newTrip.price       = trip.priceNum;
            newTrip.travelers   = trip.travelers;
            newTrip.origin.location = trip.origin.loc;
            newTrip.origin.place = !trip.origin.place ? null : trip.origin.place;
            newTrip.destination.location  = trip.dest.loc;
            newTrip.destination.place = !trip.dest.place ? null : trip.dest.place;
            newTrip.dateCreated  = booking.dateCreated;
            
            if (tripId)
                newTrip._id = tripId;

            var scan = {'origin' : 'origin', 'dest' : 'destination'};
            for (var j in scan) {
                v = scan[j];

                if (trip[j].cust) {
                    _.merge(newTrip[v].custom, trip[j].cust);
                }
            }

            tripIds.push(newTrip._id);
            
            newTrip.save();
            newTrip._index = i;
            
            trips.push(newTrip);
            
            log.push(newTrip);
        }

        for (var i=0; i<deleteIds.length; i++) {
            tripsModel.remove({_id : deleteIds[i]}).exec();
        }

        booking.trips = trips;
 
        if (bData.data.hasOwnProperty('totalPrice')) {
            booking.totalPrice = bData.data.totalPrice;
        }
        else {
            booking.totalPrice = totalPrice;
        }
        
        var data = {
            booking : null,
            payment : null
        };
        
        log.push(booking);
        
        //var saveLogs = logDirtyFields(booking._id, log);
        
        return booking
        .save()

        .then(function (doc) {
            console.log("Saved information successfully");
            
            data.booking = doc;

            if (pData)
                return payment.smartTransaction(doc.name, pData.cc_number, pData.cc_exp, pData.amountDue);

            return false;
        },
        function(err) {
            throw {
                name : 'Booking Error',
                message : 'The booking could not be saved!',
                log : err
            };
        })
            
        .then(function(paymentDetails){
            if (paymentDetails)
            {
                if (paymentDetails.transaction_approved == 1) {
                    if (!booking.totalPaid)
                        booking.totalPaid = 0;
                    
                    if (booking.quote == true) {
                        booking.dateCreated = new Date(Date.now());
                    }
                    
                    var payment = booking.addPayment(
                        +paymentDetails.amount,
                        paymentDetails.authorization_num,
                        paymentDetails.transaction_tag
                    );

                    return booking.save().then(function(){
                        data.payment  = payment;
                    });
                }
                else {
                    throw {
                        name : 'Payment Error',
                        message : paymentDetails.body,
                        log : paymentDetails
                    };
                }
            }
            
            return true;
        })
        
        .then(function(){
            return data;
        })
        
        .catch(function(e){
            logger.backend.error(e);
            console.log('Payment Error: Phase 1');
            throw e;
        });
    })
    
    .catch(function(err){
        logger.backend.error(err);
        console.log('Booking Error: Phase 2');
        throw err;
    });
};

schemaBooking.statics.getByCode = function(code) {
    return this
        .findOne({confirmationCode : code})
        .populate('trips')
        .exec()
        .then(function(booking){
            return booking;
        })
        .catch(function(e){
            console.log(e.stack);
        });
};

schemaBooking.methods.addPayment = function(amount, authorization, tag) {
    var payment = {
        paidAmount : amount,
        paidDate : new Date(Date.now()),
        authNum : authorization,
        transTag : tag
    };

    this.payment.push(payment);
    
    if (!this.totalPaid)
        this.totalPaid = amount;
    else
        this.totalPaid += amount;
    
    this.quote = false;
    //return this.save();
    return payment;
};

schemaBooking.methods.removeFull = function() {
    var _this = this;
    
    return tripsModel
     .find({ booking : this._id })
     .remove()
     .then(function(data){
         return _this.remove();
    });
};

module.exports = mongoose.model('bookings', schemaBooking);