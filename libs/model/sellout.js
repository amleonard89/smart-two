var mongoose = require('mongoose');
var moment = require('moment-timezone');

var cfg = require(config + '/app');

var s2 = require(libs + '/helpers');
var tripsModel = require(libs + '/model/trip');

var schemaSellout = new mongoose.Schema({
    start           : Date,
    end             : Date,
    seats           : Number
});
    
schemaSellout.statics.byDateRange = function(start, end) {
    var where = s2.dateQueries(start, end, 'start');
    
    return this
        .find(where)
        .sort({start : "asc", end : "asc" })
        .exec()
        .then(function(data){
            return data ? data : null;
    });
};

schemaSellout.statics.byDatetime = function(datetime) {
    var where = {
        start : { $lte : datetime },
        end : { $gte : datetime }
    };

    return this
        .find(where)
        .sort({start : "asc", end : "asc" })
        .exec()
        .then(function(data){
            return data ? data[0] : null; //there should only be one
    });
};

schemaSellout.statics.isDatetimeAvailable = function(date, time, seats) {
    var datetime;
    
    if (arguments.length === 1) {
        datetime = date;
        seats = time;
    }
    else {
        datetime = moment.tz(date+' '+time, 'YYYY/MM/DD h:mmA', cfg.TZ);
        
        if (datetime.isDST()) // they are not created as DST sensitive
            datetime.add(1, 'h');
    }
    
    /*******
     * The following is the logical order of how we will see if the date is available
     *  1: See if there is any "sellout" information for the date/time
     *  2: If we do, use the amount of seats for THAT entry to determine
     *      how many seats we have left, in not, we use the default seats
     *  3: Get all the trips on that date/time, and add them up
     *  4: Return simply true if it's available, throw error (reject) if not
     */
    return this
    
     .byDatetime(datetime.toDate()) //1
        
     .then(function(data){
        return data ? data.seats : cfg.defaultSeats; //2
     })

     .then(function(maxSeats){ //3
        var totalSeatsUsed = 0;

        return tripsModel
        .byDateRange(datetime.toDate(), datetime.toDate(), {
            subTrips : false,
            where : {
                quote : { $ne : true },
                cancelled : { $ne : true }
            }
        })
        .then(function(trips) {
            for (var i=0; i<trips.length; i++) {
                totalSeatsUsed += trips[i].travelers;
            }
            
            if (totalSeatsUsed >= maxSeats) {
                throw {
                    message: 'error_fully_booked',
                    leftover : 0
                };
            }
            else if (totalSeatsUsed + seats > maxSeats) {
                throw {
                    message: 'error_over_book',
                    leftover : maxSeats - totalSeatsUsed
                };
            }
        });
     })

     .then(
     function(){
        return true;        
     },
     function (err){
         throw err;
     }); //4
};

module.exports = mongoose.model('sellouts', schemaSellout);