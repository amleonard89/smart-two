var mongoose = require('mongoose');
var Q = require('q');
var _ = require('lodash');

var s2 = require(libs+'/helpers');
var bookingsModel = require(libs+'/model/booking');

var schemaTrip = new mongoose.Schema({
    dateTime                : { type : Date, index: true},
    booking                 : { type : mongoose.Schema.Types.ObjectId, ref : 'bookings', index : true }, //schemaBooking
    travelers               : { type : Number },
    price                   : { type : Number, default : null },
    priceId                 : { type : mongoose.Schema.Types.ObjectId, default : null }, //schemaPrice
    origin                  : {
        location            : mongoose.Schema.Types.ObjectId, //schemaLocation
        place               : mongoose.Schema.Types.ObjectId, //schemaPlace
        custom              : {
            customAddress       : {
                address         : String,
                city            : String,
                state           : String,
                zip             : String
            },
            customFlightnum     : String
        }
    },
    destination             : {
        location            : mongoose.Schema.Types.ObjectId, //schemaLocation
        place               : mongoose.Schema.Types.ObjectId, //schemaPlace
        custom              : {
            customAddress       : {
                address         : String,
                city            : String,
                state           : String,
                zip             : String
            },
            customFlightnum     : String
        }
    },
    drivers                  : [{ type : String, default : null }],
    driverNotes              : String,
    scheduleNotes            : { type: String, default: null },
    quote                    : { type: Boolean, default: 0 }, // updated from booing
    cancelled                : { type: Boolean, default: 0 }, // updated from booking
    dateCreated              : { type : Date, default: Date.now, index: true }
});

var preSave = function(doc) {
};

schemaTrip.post('save', preSave);
schemaTrip.post('findOneAndUpdate', preSave);

schemaTrip.methods.saveDrivers = function(drivers, driverNotes) {
    if (drivers && typeof drivers === 'string')
        drivers = [drivers];
    
    if (drivers)
        this.drivers = drivers;
    if (typeof driverNotes !== 'undefined')
        this.driverNotes = driverNotes;
    
    return this.save();
};

schemaTrip.statics.byDateRange = function(start, end, options) {
    var async = require('async'),
        where,
        settings = {
            subTrips : true,
            where: false
        };
    
    settings = _.merge(settings, options);
    where = s2.dateQueries(start, end, 'dateTime');
    
    if (settings.where) {
        where = _.merge(where, settings.where);
    }
    
    return this.find(where)
        .populate('booking')
        .sort({'dateTime' : 'asc'})
        .exec()
        .then(function(data){
            if (!settings.subTrips) //if we don't need to load subtrips...
                return data; //just return the data. trips.booking.trips will be array of ObjectIds
            
            var def = Q.defer();
            
            async.map(data,
                function(trip, done){
                  trip.booking.populate('trips', done);
                },
                function(err){
                    if (err)
                        return def.reject(err);
                    
                    def.resolve(data);
                }
            );

            return def.promise;
        });
};

module.exports = mongoose.model('trips', schemaTrip);