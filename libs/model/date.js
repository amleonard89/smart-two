var mongoose = require('mongoose');

var dateSchema = mongoose.Schema(
{
    date : {
        date                         : String,
        reservationTime              : String,
        price                        : Number,
        confirmationCode             : {type : String, required : true},
        travelTypeId                 : String,
        numberOfTravelers            : String
    }
});

    
module.exports = mongoose.model('date', dateSchema);