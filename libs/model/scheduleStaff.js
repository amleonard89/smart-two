var mongoose = require('mongoose');
var moment = require('moment-timezone');
var Q = require('q');
var helpers = require(libs+'/helpers');

var model;

var schemaScheduleStaff = new mongoose.Schema({
    start        : Date,
    end          : Date,
    name         : String
});
    
schemaScheduleStaff.statics.saveByDate = function(day, entries) {
    var dayUnix = day;
    var dayUnixEnd = dayUnix + (60*60*24*1000);
    
    var model = this;
    
    entries = entries || [];
    
    return this.remove({
        start : {
            $gt : dayUnix,
            $lt : dayUnixEnd
        }
    })
    .exec()
    .then(function(){
        var save = [];

        for (var i=0; i<entries.length; i++) {
            var entry = entries[i];
            var start_moment = moment(dayUnix);
            var end_moment = moment(dayUnix);
            
            start_moment.add(helpers.getHoursMinutes(entry.start));
            end_moment.add(helpers.getHoursMinutes(entry.end));

            var entry = new model({
                name : entry.name,
                start : start_moment.toDate(),
                end : end_moment.toDate()
            });
            
            save.push(entry.save());
        }
        return Q.all(save)
            .then(function(data) {
               return data;
        });
    })
    .catch(function(err){
        console.log(err.stack);
    });
};

schemaScheduleStaff.statics.byDateRange = function(start, end) {
    var schedule = this
                .find({'start' : {$gt : start , $lt : end}})
                .sort({'start' : 'asc'})
                .exec();
        
    return schedule;
};

module.exports = mongoose.model('scheduleStaff', schemaScheduleStaff);