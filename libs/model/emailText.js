var mongoose = require('mongoose');

var schemaEmailText = new mongoose.Schema({
    key         :   String,
    textDefault :   String,
    textSpecial     :   [{
        text        : String,
        placeIds    : [mongoose.Schema.Types.ObjectId],
        locationIds : [mongoose.Schema.Types.ObjectId]
    }],
    prio        :   Number
},
{
    collection: 'emailText'
});
    
schemaEmailText.statics.list = function() {
    return this.find({}).sort({'prio' : 'desc'}).exec();
};

module.exports = mongoose.model('emailText', schemaEmailText);