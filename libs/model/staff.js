var mongoose = require('mongoose');

var schemaStaff = new mongoose.Schema({
    drivers        :   [String],
    staff          :   [String]
},
{
    collection: 'staff'
});
    
schemaStaff.statics.getDriversNStaff = function() {
    return this.findOne()
        .exec()
        .then(function(data){
            data.drivers.sort();
            data.staff.sort();
            
            return data;
    });
};

module.exports = mongoose.model('staff', schemaStaff);