var mongoose = require('mongoose');

var schemaLocation = new mongoose.Schema({
    label       :   String,
    selectLabel :   String,
    emailLabel  :   {type: String, default : null},
    key         :   String,
    prefix      :   {type: String, default : null},
    suffix      :   {type: String, default : null},
    times       :   [String],
    timesNonStd :   [String],
    fields      :   mongoose.Schema.Types.Mixed,
    internals   :   mongoose.Schema.Types.Mixed,
    sort        :   Number
});
    
    
module.exports = mongoose.model('location', schemaLocation);