var mongoose = require('mongoose');

var types = require(libs+'/travel_types');
var s2 = require(libs+'/helpers');

var reservationsSchema = new Schema(
{
    reservation : {
        name                   : {type: String, default : ''},
        email                  : {type: String, default : ''},
        phoneNumber            : {type: String, default : ''},
        specialRequest         : {type: String, default : ''},
        travelType             : {type: String, default : ''},
        date                   : {type: String, default : ''},
        time                   : {type: String, default : ''},
        confirmationCode       : {type: String, default : ''},
        numberOfTravelers      : {type: String, default : ''},
        airline                : {type: String, default : ''},
        ship                   : {type: String, default : ''},
        flightNumber           : {type: String, default : ''},
        hotelName              : {type: String, default : ''},

        dateCreated            : {type: Date, default: Date.now()}
    }
});

reservationsSchema.static.getReservationByCode = function(code, callback) {
    var dates = require(libs+'/models/date');
    
    var _code = code.trim();

    return this.find({
        'reservation.confirmationCode' : _code
    })
    .exec()
    .then(function (doc)
    {
        var bodyData = {};
        var tmpTravelType = 0;
        var tmpDate = '';
        var tmpTime = '';

        for (var i in doc) {
            var _res = doc[i].reservation.toObject();

            bodyData.email = _res.email;
            bodyData.phone = _res.phoneNumber;
            bodyData.confirmationNumber = _res.confirmationCode;

            bodyData.name = _res.name;
            bodyData.numberOfTravelers = +_res.numberOfTravelers;
            bodyData.specialRequest = _res.specialRequest == 'Special Requests/Comments' ? 'None' : _res.specialRequest;

            if (_res.hasOwnProperty('hotelName') && _res.hotelName!='N/A') bodyData.hotel = _res.hotelName.trim();
            if (_res.hasOwnProperty('airline') && _res.airline!='' && _res.airline!='N/A') bodyData.airline = _res.airline.trim();
            if (_res.hasOwnProperty('ship') && _res.ship!=''&& _res.ship!='N/A') bodyData.cruiseShip = _res.ship.trim();
            if (_res.hasOwnProperty('flightNumber') &&  _res.flightNumber!='N/A') bodyData.flightNumber = _res.flightNumber;

            tmpTravelType = types.fromString(_res.travelType);
            tmpDate = _res.date;
            tmpTime = _res.time;

            bodyData.dateCreated = _res.dateCreated;
        }

        bodyData.cruiseDate = null;
        bodyData.cruiseTime = null;
        bodyData.flightDate = null;
        bodyData.flightTime = null;

        return dates.find({
            'date.confirmationCode' : _code
        })
        .exec()
        .then(function (doc2)
        {
            var travelTypes = [];

            if (doc2.length == 0) {
                travelTypes.push(tmpTravelType);
                switch (tmpTravelType) {
                    case types.CRUISE_SHIP_TO_AIRPORT:
                        bodyData.cruiseDate = tmpDate;
                        bodyData.cruiseTime = tmpTime;
                    break;
                    case types.AIRPORT_TO_CRUISE_SHIP:
                        bodyData.flightDate = tmpDate;
                        bodyData.flightTime = tmpTime;
                    break;
                    case types.AIRPORT_TO_HOTEL:
                        bodyData.flightDate = tmpDate;
                        bodyData.flightTime = tmpTime;
                    break;
                }
            }
            else {
                for (var i in doc2) {
                    var typeId = parseInt(doc2[i].date.travelTypeId);
                    var time = s2.uniformTimeReadable(doc2[i].date.reservationTime, true);
                    var dateObj = new Date(doc2[i].date.date);
                    var date = s2.twoDigit(dateObj.getMonth()+1) + '/' + s2.twoDigit(dateObj.getDate()) + '/' + dateObj.getFullYear();
                    travelTypes.push(typeId);

                    switch (typeId) {
                        case types.CRUISE_SHIP_TO_AIRPORT:
                            bodyData.cruiseDate = date;
                            bodyData.cruiseTime = time;
                        break;
                        case types.AIRPORT_TO_CRUISE_SHIP:
                            bodyData.flightDate = date;
                            bodyData.flightTime = time;
                        break;
                        case types.AIRPORT_TO_HOTEL:
                            bodyData.flightDate = date;
                            bodyData.flightTime = time;
                        break;
                    }
                }
            }
            travelTypes.sort();

            var travelTypeId;
            if (travelTypes.length === 1) {
                travelTypeId = travelTypes[0];
            }
            else {
                if (travelTypes.indexOf(types.AIRPORT_TO_HOTEL) !== -1)
                    travelTypeId = 4;
                else
                    travelTypeId = 2;
            }

            bodyData.travelTypeId = travelTypeId;
            bodyData.price = s2.getPrice(bodyData.numberOfTravelers, travelTypeId);

            if (typeof callback == 'function') {
                callback(bodyData, doc);
            }
            
            return bodyData;
        });
    });
};

reservationsSchema.static.convertOldReservations = function() {
    var places = require(libs+'/models/place');
    var bookings = require(libs+'/models/booking');
    
    var codes = [];
    
    places.placesByString().then(function(placeStrings){
       
        this.find({'converted' : null}, function(err, rows) {
            for (var i in rows) {
                var row = rows[i];
                var code = row.reservation.confirmationCode;

                if (codes.indexOf(code) !== -1)
                    continue;

                codes.push(code);

                var object = this.getObjectByCode(code, function(doc, original){
                    var data = doc;

                    data.confirmationCode = doc.confirmationNumber;
                    data.phoneNumber = doc.phone;
                    data.totalPrice = doc.price;
                    data.type = doc.travelTypeId;
                    delete doc.phone;
                    delete doc.confirmationNumber;
                    delete doc.travelTypeId;

                    switch (data.type) {
                        case types.CRUISE_SHIP_TO_AIRPORT:
                            data.cruiseInfo = {
                                ship : data.cruiseShip,
                                date : data.cruiseDate,
                                time : data.cruiseTime
                            };

                            delete data.cruiseDate;
                            delete data.cruiseTime;
                        break;

                        case types.AIRPORT_TO_CRUISE_SHIP:
                        case types.AIRPORT_TO_HOTEL:
                            data.airportInfo = {
                                airline : data.airline,
                                flightNumber : data.flightNumber,
                                date : data.flightDate,
                                time : data.flightTime
                            };

                            delete data.flightDate;
                            delete data.flightTime;
                            delete data.flightNumber;
                        break;

                        case types.ROUNDTRIP_CRUISE_AIRPORT:
                        case types.THREE_WAY_TRIP:
                            data.cruiseInfo = {
                                ship : data.cruiseShip,
                                date : data.cruiseDate,
                                time : data.cruiseTime
                            };

                            data.airportInfo = {
                                airline : data.airline,
                                flightNumber : data.flightNumber,
                                date : data.flightDate,
                                time : data.flightTime
                            };

                            delete data.cruiseShip;
                            delete data.cruiseDate;
                            delete data.cruiseTime;
                            delete data.flightDate;
                            delete data.flightTime;
                            delete data.flightNumber;
                        break;
                    }

                    var send = {body : {reservation : data}};

                    bookings.addBookingDocument(send, null, placeStrings);

                    this.update({'reservation.confirmationCode' : data.confirmationCode}, {converted : true}, {safe : false, strict : false, multi : true}, function(err, doc){
                        //console.log(err);
                    });
                });
            }
        });
        
    });
};

module.exports = mongoose.model('reservation', reservationsSchema);