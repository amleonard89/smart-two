var mongoose = require('mongoose');

var workshop = require(libs + '/workshop');

var schemaPlace = new mongoose.Schema({
    label       :   String,
    location    :   String, //schemaLocation.key
    address     :   {
        address     : String,
        city        : String,
        state       : String,
        zip         : String
    },
    prio        : { type: Number, default: 0 },
    internals   : { type: mongoose.Schema.Types.Mixed, default: null},
    labelOnly   : { type: Boolean, default: false}
});
    
schemaPlace.statics.placesByString = function() {
    return workshop.getWorkingData().then(function(data) {
        var placeData = {
            places : {},
            location : {}
        };

        for (var i in data.locations) {
            for (var j in data.locations[i].places) {
                var str = data.locations[i].places[j].label.toLowerCase().replace(/[^A-Za-z0-9]/g, '');
                    
                placeData.places[str] = {'id' : j.toString(), 'loc' : i.toString()};
            }
            placeData.location[data.locations[i].key] = data.locations[i]._id.toString();
        }
        
        return placeData;
    });
};

module.exports = mongoose.model('place', schemaPlace);