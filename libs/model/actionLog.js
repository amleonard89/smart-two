var mongoose = require('mongoose');

var schemaActionLog = new mongoose.Schema({
    datetime        : Date,
    parseUser       : String,
    userId          : mongoose.Schema.Types.ObjectId,
    link            : mongoose.Schema.Types.Mixed,
    fields          : [{
        path        : String,
        index       : { type : Number, default : null },
        valOld      : mongoose.Schema.Types.Mixed,
        valNew      : mongoose.Schema.Types.Mixed,
        addition    : { type : Number, default : null }
    }]
});
    
    
module.exports = mongoose.model('actionLog', schemaActionLog);