var mongoose = require('mongoose');

var schemaDayLimit = new mongoose.Schema({
    datetime : Date,
    seats : Number,
    complete : Boolean
});
    
    
module.exports = mongoose.model('dayLimit', schemaDayLimit);