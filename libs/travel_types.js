module.exports = {
    THREE_WAY_TRIP              : 4,
    AIRPORT_TO_HOTEL            : 3,
    ROUNDTRIP_CRUISE_AIRPORT    : 2,
    AIRPORT_TO_CRUISE_SHIP      : 1,
    CRUISE_SHIP_TO_AIRPORT      : 0,
    
    fromString : function(str) {
        switch (str) {
            case 'One Way to Hotel':
                return 3;

            case 'One Way to Airport':
                return 0;

            case 'One Way to Cruise':
                return 1;

            default:
                return 0;
        }
    }
};