var Q = require('q');
var moment = require('moment-timezone');
var _ = require('lodash');

var cfg = require(config + '/app');

var workshop = require(libs + '/workshop');
var emailTextModel = require(libs + '/model/emailText');
var s2 = require(libs + '/helpers');

exports.sendEmail = function(code, title) {
    var bookingsModel = require(libs + '/model/booking');
    var driver = require(libs + '/adapter/' + cfg.emailDriver);
    
    var getData = workshop.getWorkingData();
    var getEmailData = exports.getEmailTextData();
    var getBooking =  bookingsModel
            .findOne({confirmationCode : typeof code === 'string' ? code : code.confirmationCode})
            .populate('trips')
            .exec();

    return Q.spread([
        getData,
        getBooking,
        getEmailData
    ],
    function(schemaData, booking, emailText){

        if (booking == null) { 
            throw "No Valid Booking";
        }
        
        var html = smartHTMLEmail(booking, schemaData, emailText);
        
        title = title || 'Your Shuttle Reservation';
        
        var emailAddresses = booking.email.length ? booking.email : [];
        emailAddresses.push(cfg.salesEmail); //they always want a copy
        
        var objectAddresses = [];
        
        for (var i=0; i<emailAddresses.length; i++) {
            objectAddresses.push({
                email 	: emailAddresses[i],
                name 	: 'Valued Customer',
                type    : emailAddresses[i] == cfg.salesEmail ? 'bcc' : 'to'
            });
        }
        
        console.log(objectAddresses);
        return driver
         .sendEmail(html, title, objectAddresses)
         .then(function(result){
            console.log('E-Mail was successfully sent!');
            console.log(booking.confirmationCode);
            console.log('================');
            
            return result;
        },
        function(e){ //should be moved to use logger instead
            console.log(e);
            console.log(e.stack);
            console.log('Email was not sent!  Regarding  booking: ');
            console.log(booking.confirmationCode);
            console.log(booking.inspect());
            
            throw e;
        });
    })
    .catch(function(err){
        console.log(err.stack);
        console.log('EMAIL ERROR');
        console.log('Confirmation Code '+code);
        
        throw err; //pass it up!
    });
};

exports.getEmailTextData = function() {
    return emailTextModel.find({}).sort({'prio' : 'desc'}).exec();
};

function smartHTMLEmail(booking, schemaData, emailTextData)
{
    var fs = require('fs');
    var locationData = schemaData.locations;
    var pricesData = schemaData.prices;
    
    var liTimes = _smartTripTimes(booking, locationData);
    var liData = _smartExtract(booking, locationData);
    var tripType = _smartTripType(liTimes);

    var css = fs.readFileSync(GLOBAL.base+'/views/email/s2_email_style.css');
    
    var html = '';
    
    var plural = false;
    for (var i=0; i<booking.trips.length; i++) {
        if (booking.trips[i].travelers > 1)
            plural = true;
    }
    
    var text_tagline = booking.cancelled == 1
                        ? 'Your Shuttle Reservation Has Been Cancelled' : 'Your shuttle reservation is all set';
    var text_overview = booking.cancelled == 1
                        ? 'Cancelled Trip Overview' : 'Trip Overview';
    var text_check = booking.cancelled == 1
                        ? "We've cancelled your reservation as requested and processed a full refund which should reflect in your account within 3-5 business days. We hope we'll have the opportunity to serve you in the future and if we can be of any further assistance, please do not hesitate to contact us." :
                        //'We\'ve canclled your reservation as requested and processed a full refund which should reflect in your account within 3-5 business days. We hope we\'ll have the opportunity to serve '+(!plural ? 'you' : 'your party')+' in the future, and if we can be of any further assistance, please do not hesitate to contact us.' :
                          'Be sure to check your trip overview for accuracy and save your pickup instructions.';
    var text_paid = booking.cancelled == 1 ? 'Refunded' : 'Paid';
    var text_cost = booking.cancelled == 1 ? 'Refund' : 'Cost';
    
    
    html += '<head><style>'+css+'</style></head>';
    html += '<div id="smartEmailWrapper">';
    html += '<div id="logo"><a href=\"https://www.smart-two.com\"><img src="https://www.smart-two.com/smart-images/smart-two-logo.jpg" alt="Smart-Two.com Shuttles" /></a></div>'
        +   '<div class="logoSub">'+text_tagline+'!</div>'
        +   '<div class="checkSub">'+text_check+'</div>'
        +   '<article id="tripOverview">'
        +       '<div class="titleHeader">'+text_overview+'</div>'
        +       '<ul>'
        +           '<li>'
        +               '<b>Trip Type:</b> '+tripType
        +           '</li>';

    for (var list in liTimes) {
        var li = liTimes[list];
        
        var passLabel = (!liData.differentNum) ? '' :
                 ' ('+liData.num[list]+' Passenger'+(liData.num[list]>1 ? 's' : '')+')';
         
        html += '<li><b>'+li.label + passLabel+':</b> '+li.text+'</li>';
    }
    
    for (var list in liData.list) {
        var li = liData.list[list];
        
        html += '<li><b>'+li.label+':</b> '+li.text+'</li>';
    }

    var passStr = (liData.differentNum) ? '' :
                 ' ('+liData.num[0]+' Passenger'+(liData.num[0]>1 ? 's' : '')+')';
         
    html +=         '<li>'
        +               '<b>Reservation For:</b> '+booking.name
        +           '</li>'
        +           '<li>'
        +               '<b>Mobile Numbers:</b> '+booking.phoneNumber.join(', ')
        +           '</li>'
        +           '<li>'
        +               '<b>Confirmation #:</b> '+booking.confirmationCode
        +           '</li>'
        +           '<li>'
        +               '<b>'+text_cost+passStr+':</b> $'+booking.totalPrice.toFixed(2)+ ' USD ' + text_paid
        +           '</li>'
        +           '<li>'
        +               '<b>Special Requests/Comments:</b> '+(booking.specialRequest ? booking.specialRequest : 'None')
        +           '</li>'
        +       '</ul>'
        +   '</article>'
        +   '<article id="pickupInstr">'
        +       '<div class="titleHeader">Pick-Up Instructions</div>'
        +       '<ul>'
        +           '<li><b>Text Number:</b> 321-722-7678</li>'
        +           '<li><b>After-Hours Ext for Pickups:</b> 104</li>'
        +       '</ul>';

    if (liData.key['origin'].indexOf('airline')!==-1) {
        html += _smartAirportInstr();
    }
    
    if (liData.key['origin'].indexOf('cruise')!==-1 || liData.key['destination'].indexOf('cruise')!==-1) {
        html += _smartCruiseInstr();
    }
    
    html +=   '</article>'
        +   '<article id="faq">'
        +       '<div class="titleHeader">FAQ</div>'
        +       '<ul>'
        +           '<li>'
        +               '<b>What will my shuttle look like?</b> '
        +               'Click <a href="https://smart-two.com/smart-images/smart-two-vehicle.jpg" target="_blank">here</a> to see a picture.'
        +           '</li>'
        +           '<li>'
        +               '<b>What will my "navigator" be wearing?</b> '
        +               'Our signature look: Dark grey polo, white belt, khaki pants and white shoes.'
        +           '</li>'
        +           '<li>'
        +               '<b>Will there be others on the shuttle?</b> '
        +               'Yes. We are a shared shuttle service. We limit each vehicle to 8 passengers so there would only be a few great travelers just like yourself.'
        +           '</li>'
        +           '<li>'
        +               '<b>What if my flight is delayed?</b> '
        +               'Please contact us immediately. Upon your arrival at the Orlando Airport, we will schedule you for the next available shuttle. If someone in your party needs a separate shuttle due to a flight delay, there are additional costs.'
        +           '</li>'
        +           '<li>'
        +               '<b>What if I think I may miss my scheduled pickup at the cruise ship?</b> '
        +               "Please contact us immediately. As a courtesy, if you miss your shuttle, we will do our best to accommodate you based on availability. If after missing your shuttle, you choose to decline our next available pick-up time or take a taxi from the cruise port, no refund will be issued."
        +           '</li>'
        +           '<li>'
        +               '<b>Where do you pick up at the hotels?</b> '
        +               'At the front lobby. Please remember to arrive 10 minutes prior to your scheduled leave time.'
        +           '</li>'
        +           '<li>'
        +               '<b>What is the cancellation policy?</b> '
        +               'We understand things come up and plans change, so we offer a full refund if you contact us 48 hours prior to your scheduled pickup time.'
        +           '</li>'
        +           '<li>'
        +               '<b>What is a "no-show"?</b> '
        +               'A customer who is not at the pickup location as scheduled and fails to communicate with Smart-Two. No-shows are considered cancellations made less than 48 hours and no refunds will be issued.'
        +           '</li>'
        +       '</ul>'
        +   '</article>'
        +   '<div class="tiny">'
        +       'CONFIDENTIALITY NOTICE: This e-mail message, including any attachments, is for the sole use of the intended recipient(s) and may contain confidential and privileged information. Any unauthorized review, use, disclosure, or distribution is prohibited. If you have received this e-mail and are not the intended recipient, please contact the sender by replying to this e-mail and destroying all copies of the original message.'
        +   '</div>';
        html += '</div>';
        
        html = applyEmailText(emailTextData, html, liData.places, liData.locations);

        return html;
}

function _smartAirportInstr() {
    var html = '';
    
    html += '<div class="instrBlock" id="airportPickup">'
    +      '<div class="instrBlockHeader">Locate your shuttle at the Orlando International Airport</div>'
    +      '<p><b>Step 1:</b> _AIRPORT_STEP_1_'
    +      '<p><b>Step 2:</b> At LEVEL 1, <i>stay inside</i> the airport and face the car rental counters. Take a right and walk along the tile floors to the end of the car rental counters. Just after the MEARS rental counter you will see <b><u>_AIRPORT_DOOR_</u></b> on your left.'
    +      '<p><b>Step 3:</b> Please find seating nearby and your navigator will escort you to your shuttle upon arrival.'
    +  '</div>';
    
    return html;
}

function _smartCruiseInstr() {
    var html = '';
    
    html += '<div class="instrBlock" id="cruisePick">'
    +      '<div class="instrBlockHeader">Locate your shuttle at Port Canaveral Cruise Terminals</div>'
    +      '<p><b>8:15 AM Shuttle:</b> Please keep your luggage with you the night before to ensure you are able to meet your scheduled 8:15 AM shuttle.'
    +      '<p><b>10:35 AM Shuttle:</b> Please keep your luggage with you the night before to ensure you are able to meet your scheduled 10:35 AM shuttle.'
    +      '<p><b>Please Note:</b> Once you clear customs you will need to locate the prearranged shuttle area just outside the cruise terminal building. Please arrive 10 minutes prior to your scheduled leave time.'
    +  '</div>';
    
    return html;
}

function _smartTripType(liTimes) {
    var html = '';
    
    if (liTimes.length > 1) {
        html = 'Roundtrip';
    }
    else {
        html = 'One Way';
    }
    
    return html;
}

// This is for generating a human-readable list of each trip's times
function _smartTripTimes(booking, locationData)
{
    var li = [];
    var trips = booking.trips.toObject();
    
    for (var i in trips) {
        var trip = booking.trips[i];
        
        var originLabel = locationData[trip.origin.location].emailLabel ? locationData[trip.origin.location].emailLabel : locationData[trip.origin.location].label;
        var destLabel = locationData[trip.destination.location].emailLabel ? locationData[trip.destination.location].emailLabel : locationData[trip.destination.location].label;
        
        var tripDateDisplay = s2.dateDisplay(trip.dateTime);

        var tzDateDisplay = moment(trip.dateTime).tz('America/New_York');

        if (tzDateDisplay.isDST()) tzDateDisplay.subtract(1, 'hours');
            
        li.push({
            'label' : originLabel + ' to '+destLabel,
            'text' : tzDateDisplay.format('MM/DD/YYYY hh:mm A')
        });
    }
    
    return li;
}

// we grab a variety of data here to display information about origins, destinations, and numbers
function _smartExtract(booking, locationData)
{
    var data = {
        list : [],
        key  : {
            origin: [],
            destination: []
        },
        num : {},
        differentNum : false,
        places : [],
        locations : []
    };
    
    var trips = booking.trips.toObject();
    
    var lastNum = false;
    
    for (var i in trips) {
        var trip = booking.trips[i];
        var dirs = ['origin', 'destination'];
        
        data.num[i] = trip.travelers;
        
        if (!lastNum) {
            lastNum = trip.travelers;
        }
        else if (lastNum != trip.travelers) {
            data.differentNum = true;
        }
        
        for (var j in dirs) {
            var dir = dirs[j];
            var location = locationData[trip[dir].location];
            
            var key = location.key;
            var label = location.emailLabel ? location.emailLabel : location.label;
            var text = '';
                       
            data.key[dir].push(key);
            
            //if (key in data.list)
            //    continue;
            
            if (trip[dir].place && data.places.indexOf(trip[dir].place.toString())!==-1)
                continue;
            else if (!trip[dir].place && trip[dir].location && data.locations.indexOf(trip[dir].location.toString())!==-1)
                continue;
        
            if (trip[dir].place) {
                text += location.places[trip[dir].place].label;
            }

            if (trip[dir].custom != null && trip[dir].custom != 'null') {
                var custom = trip[dir].custom.toObject();
                
                if (custom.customFlightnum) {
                    label += ' and Flight Number';
                    text += ' #'+custom.customFlightnum;
                }

                if (custom.customAddress && !_.isEmpty(custom.customAddress)) {
                    var address = custom.customAddress;
                    text += address.address + ", " + address.city + ", " + address.state + " "+address.zip;
                }
            }
            
            if (!text) text = key == 'airline' ? 'NO FLIGHT' : 'None';
            
            data.list.push({
                label : label,
                text : text
            });
            
            if (trip[dir].place)
                data.places.push(trip[dir].place.toString());
        
            if (trip[dir].location)
                data.locations.push(trip[dir].location.toString());
        }
    }

    return data;
}

// This replaces placeholder (variable) text in a block of text with the appropriate replacement text
function applyEmailText(emailTextData, text, places, locations)
{
    places = places || [];
    locations = locations || [];
    
    for (var e in emailTextData) { // each entry from the database
        var eText = emailTextData[e];
        
        var key = eText.key;
        var replaceText = eText.textDefault;
        
        // soemtimes there are multiple options of text to show, each with their own conditions
        for (var j=0; j<eText.textSpecial.length; j++) {
            var special = eText.textSpecial[j];

            // if there are special "place" conditions to show this text
            for (var k=0; k<special.placeIds.length; k++) {
                if ( places.indexOf(special.placeIds[k].toString()) !== -1 ) {
                    replaceText = special.text; // we meet the place conditions! use this text instead
                    break;
                }    
            }
            
            // if there are special "location" conditions to show this text
            for (var k=0; k<special.locationIds.length; k++) {
                if ( locations.indexOf(special.locationIds[k].toString()) !== -1 ) {
                    replaceText = special.text; // we meet the location conditions! use this text instead
                    break;
                }
            }
            
            //add more conditions
        }
        
        // make the switch!
        var searchKey = new RegExp(key, 'ig');
        text = text.replace(searchKey, replaceText);
    }
    
    return text;
}