var winston = require('winston');

var _console = new (winston.Logger)({
    transports : [
        new (winston.transports.Console)(),
        new (winston.transports.File)({
            filename: base+'logs/console.log'
        })
    ]
});

var _frontend = new (winston.Logger)({
    transports : [
        new (winston.transports.File)({
            filename: base+'logs/frontend.log',
            json : false
        })
    ]
});

var _backend = new (winston.Logger)({
    transports : [
        new (winston.transports.File)({
            filename: base+'logs/backend.log',
            json : false
        })
    ]
});

module.exports = {
    'console' : _console,
    'frontend' : _frontend,
    'backend' : _backend
};