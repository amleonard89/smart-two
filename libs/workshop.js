var Q = require('q');
var _ = require('lodash');
var cache = new (require('node-cache'));
//var nodeCache = new cache();

cache.del('_fullLocationData');
cache.del('_fullTimeData');

var s2 = require(libs+'/helpers');

var bookingsModel = require(libs+'/model/booking');
var locationsModel = require(libs+'/model/location');
var placesModel = require(libs+'/model/place');
var pricesModel = require(libs+'/model/price');
var emailTextModel = require(libs+'/model/emailText');

exports.getWorkingData = function(callback) {
    var data = cache.get('_fullLocationData');
    
    if (data !== undefined) {

        if (typeof callback === 'function')
            callback(data);
        
        return Q.when(data); //creates promise with this data
    }
    
    var allData = {
        'locations' : {},
        'location_key' : {},
        'prices' : {},
        'origins' : {}
    };

    var getLocations = locationsModel.find().sort({sort : 1}).exec();
    var getPrices = pricesModel.find().exec();

    return getLocations.then(function(data){
        var placePromise = [];

        for (var i in data) {
            var _id = data[i]._id;

            allData.locations[_id] = data[i].toObject();
            allData.locations[_id].places = {};

            allData.location_key[data[i].key] = data[i]._id;
            
            allData.origins[data[i].key] = [];
            
            placePromise.push( placesModel.find().where({location : data[i].key}).sort({prio : 'desc', label : 'asc'}).exec() );

        }
        return Q.all(placePromise);
    })
    .then(function(placeSets){ // TODO: technically, we could just use mongooses's populate() function in getLocations
        for (var j in placeSets) {
            var places = placeSets[j];

            for (var i in places) {
                var place = places[i].toObject();
                var location_id = allData.location_key[place.location];
                var location = allData.locations[location_id];
                
                place.fullLabel =   (location.prefix ? location.prefix+' ' : '') +
                                    place.label +
                                    (location.suffix ? ' '+location.suffix : '');


                allData.locations[location_id].places[place._id] = place;
            }
        }
        return getPrices;
    })
    .then(function(prices){
        var pricesMod = [];
        for (var i in prices) {
            pricesMod[i] = prices[i].toObject();
            
            if (allData.origins[prices[i].from].indexOf(prices[i].to) === -1)
                allData.origins[prices[i].from].push(prices[i].to);
        }
        allData.prices = pricesMod;
        
        cache.set('_fullLocationData', allData);
        
        if (typeof callback === 'function') {
            callback(allData);
            return allData;
        }
        
        return allData;
    })
    .catch(function(err){
        console.log(err.stack);
    });
};

exports.getAllTimes = function() {
    var data = cache.get('_fullTimeData');
    
    if (data !== undefined) {
        return Q.when(data); //creates promise with this data
    }
    
    var getLocations = locationsModel
            .find()
            .select('times timesNonStd')
            .sort({sort : 1})
            .exec();
    
    return getLocations
    .then(function(data){
        var times = [];
        var timesObj = [];
        
        for (var i=0; i<data.length; i++) {
            var entry = data[i];
    
            times = times.concat(entry.times);
            times = times.concat(entry.timesNonStd);
        }
        
        times = _.uniq(times);
        
        for (i=0; i<times.length; i++) {
            timesObj.push(s2.getHoursMinutes(times[i]));
        }
        times = null; //garbage collect
        
        timesObj = _.sortBy(timesObj, ['h', 'm']);
        
        times = _.map(timesObj, s2.getHoursMinutesString);
        
        cache.set('_fullTimeData', times);
        
        return times;
    });
};

exports.processFrontEnd = function(frontData, card) {
    var paymentLib = require(libs + '/payment');
    var emailLib = require(libs + '/email');
    
    return bookingsModel
    
     .addFrontEnd(frontData)
     
     .then(function(booking){
        var number = card.cc_number;
        var expiry = card.cc_expiry;
         
        return paymentLib.smartTransaction(booking.name, number, expiry, booking.totalPrice)
          
        .then(function(payment){
            
            booking.addPayment(
                payment.amount,
                payment.authorization_num,
                payment.transaction_tag
            );
            
            return booking.save()
             .then(function(){
                return {
                    booking : booking,
                    payment : payment
               };
            },
             function(err){
                console.log(err);
                // if they made it this far better send that mess anyways
                return {
                    booking : booking,
                    payment : payment
               };
            });
         },
         function(err){
             console.log(err);
             booking.removeFull();
             
             throw err;
         });
     })
     
     .then(function(data){
         var booking = data.booking;
         var payment = data.payment;
         
         return emailLib
          .sendEmail(booking)
          .then(function(email){
              data.email = email;

              return data;
          },
          function(err){
              console.log(err.stack);
              throw err;
          });
     });
};