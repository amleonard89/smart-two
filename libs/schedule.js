var moment = require('moment-timezone');
var Q = require('q');

var cfg = require(config+'/app')
var pdf = require(libs+'/pdf');

exports.makeSchedulePDF = function(start, end, html) {
    var mStart = moment.tz(start, 'M/D/YYYY', 'America/New_York');
    var mEnd = moment.tz(end, 'M/D/YYYY', 'America/New_York');
    
    var pdfHeader = mStart.format('dddd, MMMM D, YYYY') + ' - '+mEnd.format('dddd, MMMM D, YYYY');
    var pdfFooter = "Safety First, Calm Home, Customer Care, Shuttle Beyond \n Luggage Reminders | \"Your Feedback Matters\"";
    
    var filename = 'schedule_'+mStart.format('MMDDYYYY')+'-'+mEnd.format('MMDDYYYY');
    
    var css = require("fs").readFileSync(GLOBAL.base+'/views/pdf/s2_schedule_email.css');
    
    html = '<style type="text/css">' + css + '</style>' + "\n" + html;
    
    var options = {
            'orientation' : 'Landscape',
            
            'header-center': pdfHeader.toUpperCase(),
            'header-font-size' : 22,
            'header-spacing' : 5,
            'margin-top' : 20,
            
            'footer-font-size' : 13,
            'footer-center' : pdfFooter,
            'footer-spacing' : 10,
            'margin-bottom' : 25,
            'footer-right' : 'Page [page] of [toPage]'
        };
        
    return pdf
    .htmlToPDF(filename, html, options)
    .then(function(data){
        return data;
    });
};