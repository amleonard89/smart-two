var requestify = require("requestify");

var e4 = require(config + '/e4');

var logger = require(libs + '/logger');

exports.smartTransaction = function (cardholder_name, cc_number, cc_expire, amount) {
    var gatewayId = e4.gatewayId;
    var password = e4.password;
    var apiURI = e4.apiUrl;
    
    var body = JSON.stringify ({
        'transaction_type'  : "00",
        'cardholder_name'   : cardholder_name,
        'cc_number'         : cc_number,
        'amount'            : amount,
        'cc_expiry'         : cc_expire,
        'gateway_id'        : gatewayId,
        'password'          : password
     });

    return requestify.request(apiURI, {
        method: "POST",
        body : body,
        headers: {
            'accept' : 'application/json',
            'content-type' : 'application/json; charset=utf-8'
        },
        dataType : 'application/json'
    })
    .then(
    function (requestifyResponse) {
        var data = requestifyResponse.getBody();
        logger.backend.info(data);

        if (data.transaction_approved != 1)
            throw {
                name : 'PaymentError',
                data : data,
                message : data.bank_message
            };
        
        return data;
    },
    function (err) {
        logger.backend.info(err);

        throw {
            name : 'PaymentError',
            message : err.body,
            data : err,
            stack : err.stack
        };
    }
    );
};