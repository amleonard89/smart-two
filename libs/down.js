var down = false;
var ips = ['24.253.3.881'];

module.exports = function(req, res, next) {
    if (!down) { 
        next();
        return;
    }
    
    if (ips.indexOf(req.ip) != -1) {
        next();
        return;
    }

    res.send('We are currently down for maintenance. Please return soon.');
    res.end();
};