var mandrill_cfg = require(config + '/mandrill');
var cfg = require(config + '/app');

var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill(mandrill_cfg.key);

var Q = require('q');

exports.sendEmail = function(html, title, to) {
    var defer = Q.defer();
    
    var from_email = cfg.salesEmail;
    var from_name = cfg.nameEmail;
    var to_email = to;
    
    var message = {
        'html'          : html,
        'subject'		: title,
        'from_email' 	: from_email,
        'from_name'		: from_name,
        'to'            : to_email,
        'headers' : {
            'Reply-To'	: from_email
        },
        "important": false,
        "track_opens": null,
        "track_clicks": null,
        "auto_text": null,
        "auto_html": null,
        "inline_css": true,
        "url_strip_qs": null,
        "preserve_recipients": null,
        "view_content_link": null,
        "tracking_domain": null,
        "signing_domain": null,
        "return_path_domain": null,
        "merge": true
    };
    
    var async = false;
    var ip_pool = "Main Pool";

    mandrill_client.messages.send({
        "message": message,
        "async": async,
        "ip_pool": ip_pool
    },
    function(result) {
        defer.resolve(result);
    },
    function(err){
        defer.reject(err);
    });
    
    return defer.promise;
};