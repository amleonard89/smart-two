exports.uniformTime = function(str) {
    if (!str) return '';
    return str.replace(':', ' ').split(' ').join('');
};

exports.uniformTimeReadable = function(str, twodigit) {
    if (!str) return '';

    var len = str.length;
    var ampm = str.substr(len-2, 2);
    len-=2;
    var min = str.substr(len-2, 2);
    len-=2;
    var hour = str.substr(0, len);

    if (twodigit == true)
            return exports.twoDigit(hour) + ':' + exports.twoDigit(min) + ' ' + ampm;
    else
            return hour + ':' + min + ' ' + ampm;
};

exports.getPrice = function(numOfTravelers, travelTypeId) {
    var prices = {
            2 : [69, 69, 129, 79, 139], 
            3 : [89, 89, 179, 89, 179],
            4 : [99, 99,199, 99, 199],
            5 : [119, 119, 229, 119, 229],
            6 : [139, 139, 269, 139, 269],
            7 : [159, 159, 299, 159, 299],
            8 : [169, 169, 329, 169, 329]
    };
	
    try {
        return prices[numOfTravelers][travelTypeId];
    }
    catch(err) {
        console.log('Couldnt find price - travelType: '+travelTypeId+', travelers: '+numOfTravelers);
        return 0;
    }
};

exports.sign = function(x){
    return x > 0 ? 1 : x < 0 ? -1 : 0;
};
    
exports.twoDigit = function(number) {
    if (+number < 10)
        return '0' + number;
    else
        return number;
};

exports.createCode = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    }).substring(1, 8).toUpperCase();
};

exports.dateDisplay = function(date) { // WARNING not DST aware
    var smOffset = ((+('-0500')/100) * 60); //replace with value loaded from config/app
    var myOffset = date.getTimezoneOffset();

    var displayOffset = (smOffset + myOffset)/60 * 100;
    var displayDate = new Date(date.toUTCString()+'-'+displayOffset);

    return displayDate;
};

exports.getHoursMinutes = function(str) {
    var regex = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):?([0-5][0-9])\s?(am|pm)?$/i;
    
    var matches = str.match(regex);
    
    var hours = +matches[1];
    var minutes = +matches[2];
    if (matches[3]) {
        if (matches[3].toLowerCase() == 'pm') {
            if (hours != 12)
                hours+=12;
        }
        else {
            if (hours == 12)
                hours = 0;
        }
    }
    
    return {
        h : hours,
        m : minutes
    };
};

exports.getHoursMinutesString = function(obj) {
    var ampm = 'AM';
        
    if (obj.h >= 12)
        ampm = 'PM';
    
    return (obj.h == 0 ? 12 : (obj.h % 13 + +(obj.h>12))) + ':' + exports.twoDigit(obj.m) + ' ' + ampm;
};

/***
 * This function takes either a start date and/or end date, or array of start dates and/or end dates
 * it creates a mongo readable query object based on whatever data it is passed
 * if an array of starts and ends are passed, its best for them to have the same number of
 * corresponding dates
 * 
 * @param {mixed} start
 * @param {mixed} end
 * @param {string} field
 * @returns Object condtiions
 */
exports.dateQueries = function(start, end, field) {
    field = field || 'dateTime';
    
    var opt = {};
    var where = {};
    
    if (Array.isArray(start)) {
        var conditions = []; // we're going to have multiple date ranges

        for (var i=0; i<start.length; i++) {
            var _s = start[i];
            var _e = end[i];

            // if there is no end date, or if the start and end are the same
            if (!_e || _s.getMilliseconds() === _e.getMilliseconds()) {
                opt = {};
                opt[field] = _s; // we are looking only for this one time
            }
            else {
                opt = {};
                opt[field] = { // we are looking for a range
                    $gte : _s,
                    $lte : _e
                };
            }
            
            conditions.push(opt);
        }
        
        // it will look for any of the conditions we have built
        // the number of conditions with be the same size as start.length
        where = {
            $or : conditions
        };
    }
    else
    {
        if (end) { // if there's an end date, we are looking for a range
            opt[field] = {
                $gte : start,
                $lte : end
            };
        }
        else { // if not, we are looking for a single date
            opt[field] = start;
        }
        where = opt; //ezpz
    }
    
    return where;
};

//fin