var mongoose = require('mongoose');
var DataTable = require('mongoose-datatable');
var moment = require('moment-timezone');
var Q = require('q');
var async = require('async');
var _ = require('lodash');

var cfg = require(config+'/app');

var bookingsModel = require(libs+'/model/booking');
var tripsModel = require(libs+'/model/trip');
var s2 = require(libs+'/helpers');

DataTable.enableDebug(false);
DataTable.enableVerbose(false);

// This filter can be used to take the data passsed along with the "query" from the
// request and build some queries based off of it

var parseFilter = function(query, options) {
    var filterPromise = [];
    var conditions = {};
    var settings = {
        'queryBookings' : true // unituitively named yes, but what it means is if we need to ADDITIONALLY query the bookings collection
                               // i.e. we are using the trips model as a base, but need to search by some data in the bookings collection
    };
    
    _.merge(settings, options);
    
    var bookingQuery = {};
    
    // if there is no search string, we can proceed with date filtering
    if (!query.search.value) {
        var start;
        var end;
        
        if (query.filter.sm_dateLength != 0)
        {
            var days = +query.filter.sm_dateLength;
            var clientDate = moment.tz(query.clientDate, 'ddd MMM D YYYY', cfg.TZ);
            
            start = clientDate.clone().startOf('day').subtract((days-(s2.sign(days) * 1)), 'days');
            end = clientDate.clone().endOf('day');
            
            if (days < 0) {
                end.add(Math.abs(days)-1, 'day');
            }
            if (query.filter.sm_byCreated && query.filter.sm_dateLength == 2) {
                end.subtract(1, 'day');
            }
        }
        else if (query.filter.sm_dateStart && query.filter.sm_dateEnd)
        {
            start = moment.tz(+query.filter.sm_dateStart, cfg.TZ)
                    .startOf('day');
            end = moment.tz(+query.filter.sm_dateEnd, cfg.TZ)
                    .endOf('day');
        }

        //Dates aren't stored as "real" DST aware dates, so in the database they are
        //technically off by one hour if they are for a time during DST
        if (start.isDST()) {
            start.add(1, 'h');
            end.add(1, 'h');
        }

        if (query.filter.sm_byCreated == 1 && settings.queryBookings == true)
        {
            _.merge(bookingQuery, {
                dateCreated : {
                    $gte : start.toDate(),
                    $lte : end.toDate()
                }
            });
        }
        else {
            conditions.dateTime = {
                $gte : start.toDate(),
                $lte : end.toDate()
            };
        }
        
    }
    else // if there is a search value, we disregard date filtering
    {
        if (settings.queryBookings == true) // this should only be true if we are querying by trips as a base
        {
            var search = query.search.value;
            var regex = new RegExp(search, 'i');

            // you see, the search is really only concerned with data from "bookings"
            // however, sometimes we are querying by trips as a base. So we need to manually search the
            // bookings collection, and use those ids to filter the trips
            var searchQuery = {
                $or: [
                    { name : regex },
                    { confirmationCode : regex },
                    { email :  regex },
                    { phoneNumber : regex },
                    { specialRequest : regex }
                ]
            };

            _.merge(bookingQuery, searchQuery);
        }
        else
        {
            //filterPromise.push(Q.when());
        }
    }
    
    var field_map = {
        quote: 'sm_quote',
        cancelled: 'sm_cancelled'
    };
    var filters = {};
    
    for (var key in field_map) {
        var filter_field = field_map[key];

        conditions[key] = 
            query.filter[filter_field] == 1 ?
            {$eq : true } :
            {$ne : true };
    }
    
    if (settings.queryBookings == true) {
        _.merge(bookingQuery, filters);
    }
    else {
        _.merge(conditions, filters);
    }
    
    // here, we actually pull the data from the bookings collection if we need to
    if (settings.queryBookings == true && !_.isEmpty(bookingQuery)) {
        filterPromise.push(
         bookingsModel
         .find(bookingQuery)
         .exec()
         .then(function(data){
             return { ids : data };
         })
        );
    }

    return Q.all(
        filterPromise.length > 0 ? filterPromise : Q(true) //once we have any secondary data we need
    )
    
    .then(function(data){ 
        for (var i=0; i<data.length; i++) {  
            if (data[0].ids) {
                var _ids = _.map(data[0].ids, '_id');
            
                if (!conditions.booking) //if we haven't already specified some bookings
                    conditions.booking = { $in : _ids };
                else
                    conditions.booking.$in = _.concat(conditions.booking.$in, _ids);
            }
        } 
            
        return conditions; //these conditions are not necessary used directly by a query, they can be interpretted first
    })
    
    .catch(function(err){
        console.log(err.stack);
    });
};

module.exports.tripDataTable = function(query) {
    var options = {
        select : [
            'dateTime',
            'origin',
            'destination',
            'travelers',
            'price',
            'priceId',
            'drivers'
        ]
    };

    return parseFilter(query, { queryBookings : true })
     .then(function(conditions) {
        var _datatable = new Q.defer();
        options.conditions = conditions;

        tripsModel.dataTable(query, options, function  (err, data) {
            if (err) {
                console.log(err);
                return _datatable.error(err);
            }
            
            data.tableType = 'trips';
            
            _datatable.resolve(data);
            // This is commented out because it was just too slow. Now we lazy-load this data on the front end
            /*async.map(data.data,
              function(trip, done){
                  var load = bookingsModel.findOne({_id : trip.booking});
                  
                  return load 
                    .populate('trips')
                    .exec()
                    .then(function(booking){
                        trip.booking = booking;
                        done(null, trip);
                  });
              },
              function(err){
                if (err) {
                    console.log(err);
                    return _datatable.error(err);
                }

                _datatable.resolve(data);
              });*/
        });
        
        return _datatable.promise;
    });
};

module.exports.bookingDataTable = function(query) {
    var options = {
        select : [
            'name',
            'specialRequest',
            'confirmationCode',
            'totalPrice',
            'totalPaid',
            'trips',
            'dateCreated',
            'phoneNumber',
            'email',
            'cancelled',
            'quote'
        ]
    };

    return parseFilter(query, { queryBookings : false })
     .then(function(conditions) {
        var _datatable = new Q.defer();
        var _conditions = {};

        if (conditions.dateTime) _conditions.dateCreated = conditions.dateTime;
        if (conditions.booking) _conditions.id = conditions.booking;
        
        options.conditions = _conditions;
        
        var field_map = {
            quote : 'sm_quote',
            cancelled : 'sm_cancelled'
        };
        
        for (var key in field_map) {
            var filter_field = field_map[key];
            
            options.conditions[key] = 
                query.filter[filter_field] == 1 ?
                {$eq : 1 } :
                {$ne : 1 };
        }
        
        bookingsModel.dataTable(query, options, function  (err, data) {
            if (err) {
                console.log(err);
                return _datatable.error(err);
            }

            data.tableType = 'bookings';
            
            async.map(data.data,
              function(booking, done){
                  var load = tripsModel.find({booking : booking._id});
                  
                  return load
                    .exec()
                    .then(function(trips){
                        booking.trips = trips;
                        done(null, booking);
                  });
              },
              function(err){
                if (err) {
                    console.log(err);
                    return _datatable.error(err);
                }

                _datatable.resolve(data);
              });
        });
        
        return _datatable.promise;
    })
    .catch(function(err){
        console.log(err.stack);
    });
};