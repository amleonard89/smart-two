function start ()
{
    GLOBAL.base = __dirname + '/';
    GLOBAL.config = __dirname + '/config';
    GLOBAL.libs = __dirname + '/libs';

    var BASE = '/en/transportation/orlando-airport/shuttles/port-canaveral';

    var cfg = require(config + '/app');
    var parse_cfg = require(config + '/parse');
    var express = require('express');
    var app = express();

    var bodyParser = require('body-parser');
    var swig = require('swig');

    var db = require(libs+'/initdb');

    require('parse').Parse.initialize(parse_cfg.init1, parse_cfg.init2);
    
    app.engine('html', swig.renderFile);
    app.set('view engine', 'html');
    app.set('views', __dirname + '/views');

    app.use(require('cookie-parser')());
    
    require('./router/filesize')(app);
    
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    
    app.use(function(req, res, next) { //console.log(req.protocol);
        swig.setDefaults({
            locals : {
                'base' : cfg.protocol + '://' + cfg.url + '/',
                'adminPath' : cfg.adminPath+'/'
            }
        });
        next();
    });

    require('./router')(app);
    
    app.use(require(libs+'/down'));
    
    app.all('/', function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Content-Type");
        res.redirect(301, BASE+'/index.html');
        res.end();
        //next();
    });

    app.use(require('compression')());
    app.listen(cfg.port);

    app.use(BASE, express.static(__dirname + '/public'));
    app.use(express.static(__dirname + '/public'));

    module.exports = app;
};

exports.start = start;
