(function ( $ ) {

    $.fn.bs_dropdowntext = function(text, bold) {
        bold = (typeof bold == 'undefined') ? true : bold;
        
        text = text ? text.trim() : $(this).text().trim();

        var dropdown = this.closest('.dropdown');
        var replace = dropdown.attr('data-replace');
        if (replace && replace!='') text = text.replace(replace, '');

        dropdown.find('button .unselected').html(text.trim());

        if (text && bold)
            dropdown.addClass('bold');
        else
            dropdown.removeClass('bold');
        //this.append("<span class='caret'></span>");

        return this;
    };

}( jQuery ));

sBook.init();

var sBookWorkshop = function() {
    this.form = null;
    this._ready = false;
    
    this.buttons = {
        save : null,
        clear : null,
        email : null,
        cancel : null
    };
    
    this.inputs = {
        name : null,
        email : null,
        phoneNumber : null,
        specialRequest : null,
        totalPrice : null,
        totalPaid : null,
        amountDue : null,
        notes : null
    };
    
    this.dropdowns = {
        referral : null,
        email_subject : null
    };
    
    this.inputsPayment = {
        cc_number : null,
        cc_exp : null
    };
    
    this.arrInput = {
        email : /[\,?\s]+/g
    };
    
    this.invalid = [];
    this.invalid_top = 0;
    
    var booking;
    
    this.init = function(_booking) {
        booking = _booking;
        
        this.form = $('#addResForm');
        this.buttons.save = $('#add-reservation-button');
        this.buttons.email = $('#email-button');
        this.buttons.cancel = $('#cancel-button');
        
        this.inputs.name = $('input[name=name]');
        this.inputs.email = $('input[name=email]');
        this.inputs.phoneNumber = $('input[name="phone-number[]"]');
        this.inputs.specialRequest = $(':input[name=special-request]');
        this.inputs.totalPrice = $('input[name=custom-price]');
        this.inputs.totalPaid = $('input[name=price-paid]');
        this.inputs.amountDue = $('input[name=price-due]');
        this.inputs.notes = $(':input[name=notes]');
        
        this.dropdowns.referral = $('#referral');
        this.dropdowns.email_subject = $('#emailHeader');

        this.inputsPayment.cc_number = $('input[name=card-number]');
        this.inputsPayment.cc_exp = $('input[name=card-expiration]');
    };
    
    this.val = function(index, subindex) {
        if (this.dropdowns.hasOwnProperty(index)) {
            return this.dropdowns[index].find('button').text().trim();
        }
        else if (this.inputs.hasOwnProperty(index)) {
            if (this.inputs[index].length > 1) {
                var val = [];
                this.inputs[index].each(function(){
                    val.push($(this).val());
                });
                
                return val;
            }
            
            var val = subindex ? this.inputs[index][subindex].val() : this.inputs[index].val();
            
            if (this.arrInput[index])
                return val.split(this.arrInput[index]);
            else
                return val;
        }
        else if (this.inputsPayment.hasOwnProperty(index)) {
            return this.inputsPayment[index].val();
        }
        return null;
    };
    
    this.set = function(index, oVal) {
        if (typeof oVal === 'undefined')
            return;
        
        var val = oVal;
        if (Array.isArray(val)) {
            val = val.join("\n");
        }
        
        if (this.dropdowns.hasOwnProperty(index)) {
            this.dropdowns[index].find('button').bs_dropdowntext(new String(val));
        }
        else if (this.inputs.hasOwnProperty(index)) {
            if (this.inputs[index].length > 1) {
                this.inputs[index].each(function(i){
                    if (i > oVal.length-1)
                        return false;
                    
                    $(this).val(oVal[i]);
                });
            }
            else {
                this.inputs[index].val(val);
            }
        }
        else if (this.inputsPayment.hasOwnProperty(index)) {
            this.inputsPayment[index].val(val);
        }
    };
    
    this.collectReservationObject = function() {
        cyclePricesFromDb();

        booking.data.name = this.val('name').trim();
        booking.data.notes = this.val('notes').trim();
        booking.data.email = this.val('email').trim();
        booking.data.specialRequest = this.val('specialRequest');
        booking.data.totalPrice = this.val('totalPrice');
        booking.data.referral = this.val('referral');

        booking.setPhoneNumber(this.val('phoneNumber').trim());

        this.form.find('.trip.active').each(function(i) {
            var trip = booking.getTrip(i);

            $(this).find('.trip-place').each(function() {
                var dir = $(this).attr('data-direction');

                $(this).find('.customData').each(function() {
                    var type = $(this).attr('data-type');

                    switch (type) {
                        case 'customFlightnum':
                            var flightnum = $(this).find('input[name=customFlightnum]').val();
                            trip.setCustomData(dir, type, flightnum);
                        break;

                        case 'customAddress':
                            var address = {
                                'address'   : $(this).find('input[name=address]').val(),
                                'city'      : $(this).find('input[name=city]').val(),
                                'state'     : $(this).find('input[name=state]').val(),
                                'zip'       : $(this).find('input[name=zip]').val()
                            };
                            trip.setCustomData(dir, type, address);
                        break;
                    }
                });
            });
        });

        return booking;
    };

    this.collectPaymentObject = function() {
        var num = this.val('cc_number');
        var exp = this.val('cc_exp');
        var amountDue = this.val('amountDue');
        
        return (!num && !exp) ? false : {
            amountDue : amountDue,
            cc_number : num,
            cc_exp : exp
        };
    };
    
    this.validateBooking = function(payment) {
        this.clearInvalidFields();

        var required = ['name', 'totalPrice', 'phoneNumber'];
        
        if (payment || (booking._id && booking.quote == false))
            required.push('email');
        
        for (var i=0; i<required.length; i++) {
            var field = required[i];
            
            if (typeof booking.data[field] === 'array') {
                if (booking.data[field].length === 0)
                    this.invalid.push(field);
            }
            else { 
                if (field === 'name') {
                    if (!booking.data[field] || booking.data[field].split(' ').length <= 1)
                        this.invalid.push(field);
                }
                else {
                    if (!booking.data[field] || booking.data[field] == '') {
                        this.invalid.push(field); console.log(field);
                    }
                }
            }
        }
        
        for (var i=0; i<booking.trips.length; i++){
            var trip = booking.getTrip(i);
            var dom = trip.dom();
            
            if (!trip.data.travelers)
                this.invalid.push(dom.find('.trip-traveler-amount').closest('.dropdown'));
            if (!trip.data.date.dateStr)
                this.invalid.push(dom.find('.date-picker'));
            if (!trip.data.date.timeStr)
                this.invalid.push(dom.find('.time-picker.dropdown'));
            if (!trip.data.origin.loc || !trip.data.dest.loc)
                this.invalid.push(dom.find('.direction-menu').closest('.dropdown'));
        };      
        
        this.processInvalid();
        
        return this.invalid.length === 0;
    };
    
    this.validatePayment = function(payment) {
        var required = ['amountDue', 'cc_number', 'cc_exp'];
        
        for (var i in required) {
            if (!required.hasOwnProperty(i)) continue;
            
            var field = required[i];
            
            if (!payment[field] || payment[field] == '')
                this.invalid.push(field);
        }
        
        if (payment['cc_exp'].length < 4)
            this.invalid.push('cc_exp');
        
        this.processInvalid();
        
        return this.invalid.length === 0;
    };
    
    this.processInvalid = function() {
        var _marked = false;
        
        for (var i=0; i<this.invalid.length; i++) {
            _marked = true;
            this.markInvalidField(this.invalid[i]);
        }
        if (_marked) {
            $(window).scrollTop(this.invalid_top - 80);
        }
    };
    
    this.clearInvalidFields = function() {
        this.invalid = [];
        this.invalid_top = 0;
        //console.log(this.form.find('.dropdown.has-error, .form-group.has-error'));
        this.form.find('.dropdown.has-error, .form-group.has-error').removeClass('has-error');
    };
    
    this.markInvalidField = function(index) {
        var field = [];
        
        if (this.dropdowns.hasOwnProperty(index)) {
            field.push(this.dropdowns[index].addClass('has-error'));
        }
        else if (this.inputs.hasOwnProperty(index)) {
            if (index === 'array') { //not used
                if (!this.val('name', 'name_first')) field.push(this.inputs[index]['name_first'].closest('.form-group').addClass('has-error'));
                if (!this.val('name', 'name_last')) field.push(this.inputs[index]['name_last'].closest('.form-group').addClass('has-error'));
            }
            else {
                this.inputs[index].each(function(j){
                    field.push($(this).closest('.form-group').addClass('has-error'));
                });
                
            }
        }
        else if (this.inputsPayment.hasOwnProperty(index)) {
            field.push(this.inputsPayment[index].closest('.form-group').addClass('has-error'));
            
        }
        else if (index.jquery) {
            field.push(index.addClass('has-error'));
        }
        
        //for (var i in field) { 
        for (var i=0; i<field.length; i++) {
            var pos = field[i].offset();

            if (pos.top < this.invalid_top || this.invalid_top === 0)
                this.invalid_top = pos.top;
        }
    };
    
    this.selectiveDisablePayment = function() {
        var payment = this.collectPaymentObject();
        
        if (payment)
            this.clearInvalidFields();
        
        var text = (typeof loadConfirmCode) === 'undefined' ? ['Save'] : [booking.quote ? 'Make' : 'Update'];
        
        if (payment && this.validatePayment(payment)) {
            text.push('and Pay Now');
        }
        
        this.buttons.save.text(text.join(' '));
    };
    
    this.selectiveEmailButton = function() {
        (typeof loadConfirmCode === 'undefined' && 
                booking.payments.length === 0 &&
                booking.legacy == false) ?
            this.buttons.email.closest('.email-form').hide() :
            this.buttons.email.closest('.email-form').show();
    };
    
    this.sendData = function(data) {
        var workshop = this;
        
        this.form.formDisable();
        
        var jsonData = JSON.stringify(data);
        
        var send = $.ajax ({
            url: "booking",
            type: "POST",
            contentType : 'application/json',
            dataType: "json",
            data : jsonData
        });
        
        return send.then(function(response){
            var data = response.booking || {};
            var payment = response.payment || false;
            
            if (data.confirmationCode) {
                $('#customCode').text(data.confirmationCode);
                loadConfirmCode = data.confirmationCode;
                booking.confirmationCode = data.confirmationCode;
            }
            if (data._id) {
                booking._id = data._id;
            }
            
            if (data.trips) {
                for (var i=0; i<data.trips.length; i++) {
                    var trip_id = data.trips[i]._id;
                    
                    booking.getTrip(i).id = trip_id;
                }
            }
            if (payment) {
                booking.addPayment(payment);
                booking.setPricePaid(data.totalPaid);
                workshop.set('totalPaid', booking.getPricePaid());
                generatePaymentHtml();
                updateAmountDue();
            }
            
            if (response.message && Array.isArray(response.message))
                bucketNotify({ text: response.message.join(' | '), type : 'success' });
                      
            watchBooking();
            
            if (response.error)
                return $.Deferred().reject(response);                
                
            return response;
        })
        .fail(function(err){
            console.log(err);
            bucketNotify({ text: '<u>'+err.name+'</u> - ' + err.message, type : 'error' });
        })
        .always(function(){
            workshop.form.formEnable();
            workshop.selectiveEmailButton();
        });
    };
    
    this.sendEmail = function(options) {
        options = options || {};
        
        if (loadConfirmCode) {
            return $.ajax({
                url : 'booking/'+loadConfirmCode+'/email',
                data : options,
                type: 'POST',
                success : function(response) {
                    if (response.error)
                        bucketNotify({ text: response.message, type : 'error' });
                    else
                        bucketNotify({ text: response.message, type : 'success' });
                }
            });
        }
        return $.Deffered.resolve();
    };
    
    this.cancel = function() {
        return $.ajax({
            url : 'booking/'+loadConfirmCode+'/cancel',
            data : { code : loadConfirmCode },
            type: 'POST'
        })
        .then(function(data){
            var _booking = data.booking;
            if (_booking.cancelled) {
                booking.cancelled = true;
                watchBooking();
                
                if (data.message)
                    bucketNotify({ text: data.message.join(' | '), type : 'success' });
            }
            else {
                if (data.error)
                    bucketNotify({ text: data.message, type : 'error' });
            }
            
            return data;
        });
    };
    
    this.ready = function(state) {
        if (state) {
            this._ready = true;
            return true;
        }
        
        return this._ready;
    };
    
    return this;
};

var workshop = new sBookWorkshop();

$(document).ready( function() {
    workshop.init(booking);
    workshop.selectiveEmailButton();
    workshop.selectiveDisablePayment();
    
    workshop.buttons.save.click( function (e) {
        e.preventDefault();

        workshop.clearInvalidFields();
        workshop.collectReservationObject();
        var payment = workshop.collectPaymentObject();
        
        if (payment && !workshop.validatePayment(payment))
            return;
        
        if (!workshop.validateBooking(payment))
            return;
               
        var executeSend = function() {
            var sendData = { booking : booking };
            
            if (payment)
                sendData.payment = payment;
        
            workshop.sendData(sendData).then(function(){
                watchBooking();
            });
        };
        
        if (typeof loadConfirmCode === 'undefined' && !payment) {
            bootbox.dialog({
                message: 'Would you like to save?',
                title: 'Save',
                buttons: {
                    quote : {
                        label : 'Quote',
                        'callback' : function(e){
                            booking.quote = true;
                            executeSend();
                        }
                    },
                    normal : {
                        'label' : 'Reservation',
                        'callback' : function(e){
                            executeSend();
                        }
                    },
                    cancel : {
                        'label' : 'Cancel',
                        'callback' : function(e){
                            return true;
                        }
                    }
                }
            });
        }
        else {
            executeSend();
        }
    });
    
    workshop.buttons.email.click( function (e) {
        e.preventDefault();
        
        workshop.form.formDisable();
        
        var titleStr = workshop.val('email_subject').trim();
        var email = workshop.sendEmail({ title : titleStr });
        
        if (email) {
            email.always(function(){
                workshop.form.formEnable();
            });
        }
        else {
            workshop.form.formEnable();
        }
    });
    
    workshop.buttons.cancel.click( function (e) {
        bootbox.dialog({
            message: 'Are you sure you want to cancel this '+bookingTypeName.toLowerCase()+'?',
            title: 'Cancellation',
            buttons: {
                ok : {
                    label : 'Yes',
                    className : 'btn-danger',
                    'callback' : function(e){
                        workshop.cancel();
                    }
                },
                no : {
                    'label' : 'No',
                    'callback' : function(e){
                        return true;
                    }
                },
                cancel : {
                    'label' : 'Cancel',
                    'callback' : function(e){
                        return true;
                    }
                }
            }
        });
    });
    
    workshop.inputsPayment.cc_number.bind('change keyup', function(){ workshop.selectiveDisablePayment() });
    workshop.inputsPayment.cc_exp.bind('change keyup', function(){ workshop.selectiveDisablePayment() } );
    
    //workshop.inputs.phoneNumber.mask("(999) 999-9999");
    
    if (typeof loadConfirmCode !== 'undefined')
    {
        workshop.form.formDisable();
        
        $.when(
            sBook.promise(),
            $.ajax({
               url: "booking/"+loadConfirmCode,
               dataType: "json"
            })
        )
        .then(function(engine, book) {
            workshop.form.formEnable();

            var data = book[0];

            booking.load(data.booking);
            
            $('#customCode').text(loadConfirmCode);
            
            workshop.set('totalPrice', booking.getPrice());
            workshop.set('totalPaid', booking.getPricePaid());
            workshop.set('name', booking.data.name);
            workshop.set('phoneNumber', booking.getPhoneNumber());
            workshop.set('email', booking.data.email.join(', '));
            workshop.set('specialRequest', booking.data.specialRequest);
            workshop.set('referral', booking.data.referral);
            workshop.set('notes', booking.data.notes);
            
            if (booking.data.referral) $('#referral button').bs_dropdowntext(booking.data.referral);

            var trips = booking.getTrips();
            var dirs = ['origin', 'dest'];
            for (var i=0; i<trips.length; i++) {
                var dTrip = addTrip();

                for (var j=0; j<dirs.length; j++) {
                    var dir = dirs[j];

                    dTrip.find('input[name=auto_price]').prop('checked', trips[i].isSuggestedPrice());

                    directionMenuChange(dTrip, dir, trips[i].getLocationKey(dir), true);

                    //dTrip.find('.date-picker').datepicker('update', trips[i].getDate());
                    //console.log(dTrip.find('.date-picker-control').data('datepicker'));
                    
                    try {
                        dTrip.find('.date-picker-control').data('datepicker').setDate(trips[i].getDate());
                    }
                    catch (e) {
                        dTrip.find('.date-picker-control').datepicker('setDate', trips[i].getDate());
                    }
                    
                    dTrip.find('.date-picker-control').val(trips[i].getDate());
                    
                    dTrip.find('.direction-menu[data-direction="'+dir+'"] li a[data-id="'+trips[i].getLocation(dir)+'"]').bs_dropdowntext();
                    dTrip.find('.trip-place[data-direction="'+dir+'"] .locationPlace li a[data-id="'+trips[i].getPlace(dir)+'"]').click();
                    dTrip.find('.time-picker li a[data-time="'+trips[i].getTime()+'"]').bs_dropdowntext();
                    dTrip.find('input.tripPrice').val(trips[i].getPrice());
                    dTrip.find('.trip-traveler-amount a[data-num="'+trips[i].getNum()+'"]').bs_dropdowntext();
                    
                    var custData = trips[i].getCustomData(dir);
                    for (var custKey in custData) {
                        var cData = custData[custKey];

                        var custHome = dTrip.find('.customData[data-type="'+custKey+'"]');

                        if (typeof cData == 'string') {
                            custHome.find(':input[name="'+custKey+'"]').val(cData);
                        }
                        else {
                            for (var index in cData) {
                                custHome.find(':input[name="'+index+'"]').val(cData[index]);
                            }
                        }
                    }
                }
                
                dTrip.find('.direction-menu li a[data-origin="'+trips[i].getLocationKey('origin')+'"][data-dest="'+trips[i].getLocationKey('dest')+'"]').bs_dropdowntext();
            }

            $('input[name=auto_total_price]').prop('checked', booking.isSuggestedPrice());
            cyclePricesFromDb();
            generatePaymentHtml();
            
            workshop.selectiveEmailButton();
            workshop.ready(true);
            
            $.uniform.update();
            getBookingPreview();
            watchBooking();
        })
        .fail(function(){
            workshop.form.formEnable();
            workshop.ready(true);
        });
    }
    else {
        $('#addTrip').click();
        $('input[name=auto_price],input[name=auto_total_price]').prop('checked', true);
        
        $.uniform.update();
        
        workshop.ready(true);
        
        generatePaymentHtml();
        watchBooking();
    }
});

var generatePaymentHtml = function() {
    $('#bookingPayments .contain').empty();
    
    if (booking.payments.length == 0) {
        $('#bookingPayments').hide();
        return;
    }
    
    $('#bookingPayments').show();
    
    for (var i=0; i<booking.payments.length; i++) {
        var payment = booking.payments[i];
        
        var str = '<small>#'+payment.authNum+' <i>$'+payment.paidAmount+'</i> On '+payment.paidDate.toLocaleString()+'</small><br>';
        
        $('#bookingPayments .contain').append(str);
    }
};

var minDate = null;//new Date(Date.now()+(60*60*24*1000));
var numTrips = 0;

var booking = new SmartBooking();
var bookingTypeName = 'Reservation';

var directionMenuChange = function(trip, direction, key, validate) {
    var tripIndex = +$(trip).attr('data-num');
    var tripObj = booking.getTrip(tripIndex-1);
    
    if (typeof validate === 'undefined' && tripObj.getLocationKey(direction) === key) {
        return;
    }
    var newHome = $(trip).find('.trip-place[data-direction='+direction+']');
    var newPlaceHome = newHome.find('.holder.place');
    var newOptionsObj = $('.tripComponent[data-key='+key+']').clone(true).removeClass('tripComponent');

    newHome.data('type_id', key);
    newHome.removeClass('hide');
    newPlaceHome.empty().append(newOptionsObj);

    if (!validate)
        tripObj.setPlace(direction, null);
    
    if (direction == 'origin')
    {
        cycleTimes(key, trip);
    }

    var addFields = {
        'origin' : [],
        'dest' : []
    };

    var field = sBook.lockeys[key].fields;

    if (field) {
        if (field.length == 1) {
            addFields['origin'] = field;
            addFields['dest'] = field;
        }
        else {
            if (field[0].length > 0)
                addFields['origin'] = field[0];
            if (field[1].length > 0)
                addFields['origin'] = field[1];
        }
    }

    for (var i=0; i<addFields[direction].length; i++) {
        var baseField = $('.tripComponentGlobal[data-type="'+addFields[direction][i]+'"]');
        var newField = baseField.clone();
        newField.removeClass('tripComponentGlobal');

        customControls(newField);
        
        newPlaceHome.append(newField);
    }

    tripObj.setLocationByKey(direction, key);
    
    trip.find('.time-picker').removeClass('hide');
};

var customControls = function(form) {
    form.find(':input[data-control=autocomplete]').each(function(i){
        var options = $(this).attr('data-controlData').split(',').sort();

        $(this).autocomplete({source : options, minLength : 1});
    });
};

var addTrip = function(e) {
    //$.uniform.restore('.trip input.uni');
    
    var newTrip = $('.trip.hide').clone();

    numTrips++;
    newTrip.attr('data-num', numTrips);
    newTrip.removeClass('hide').addClass('active');
    
    var tripObj = (typeof e == 'undefined') ? 
                    booking.getTrip(numTrips-1).setDom(newTrip) :
                    booking.addTrip().setDom(newTrip);

    var datePickerObj = newTrip.find('.date-picker');
    var datePicker = newTrip.find('.date-picker-control').datepicker({
        format : 'mm/dd/yyyy',
        autoclose : true,
        orientation: 'bottom',
        startDate : minDate
    })
    .on('changeDate', function(ev){
        if (ev.date) {
            tripObj.setDate($.format.date(ev.date, "MM/dd/yyyy"));
            cycleTrips();
        }
        else {
            tripObj.setDate($(this).val());
        }
    })
    .data('datepicker');

    if (typeof e !== 'undefined') {
        if (numTrips > 1) {
            var lastTrip = booking.getTrip(numTrips-2);
            var lastNum = lastTrip.getNum();
            var origin = lastTrip.getLocationKey('dest');
            var dest = lastTrip.getLocationKey('origin');
            var originPlace = lastTrip.getPlace('dest');
            var destPlace = lastTrip.getPlace('origin');
            
            var ev = jQuery.Event('click');
            ev.target = newTrip.find('.direction-menu a[data-origin="'+origin+'"][data-dest="'+dest+'"]')[0];
            
            if (ev.target)
                $(document).trigger(ev);

            tripObj.setPlace('origin', originPlace);
            tripObj.setPlace('dest', destPlace);
            newTrip.find('.trip-place[data-direction="origin"] .locationPlace li a[data-id="'+originPlace+'"]').bs_dropdowntext();
            newTrip.find('.trip-place[data-direction="dest"] .locationPlace li a[data-id="'+destPlace+'"]').bs_dropdowntext();
            
            tripObj.setNum(lastNum);
            newTrip.find('.trip-traveler-amount a[data-num="'+lastNum+'"]').bs_dropdowntext();
        }
    }
    
    newTrip.find('input[name=auto_price]').prop('checked', 'checked');
    
    $('#addTrip').before(newTrip);

    cycleTrips();
    
    newTrip.find('input.uni').uniform();
    
    bindFormControls(newTrip);
    
    return newTrip;
};

var removeTrip = function() {
    if (numTrips <= 1)
        return;
    
    var trip = $(this).closest('.trip');

    booking.delTrip(+trip.attr('data-num')-1);

    trip.remove();

    numTrips--;

    cycleTrips();
    cyclePricesFromDb();
};

var cycleTrips = function() {
    $('.trip.active .tripNumber').text('');
    var lastpicker = null;
    
    $('.tripClose').show();

    $('.trip.active').each(function(i){
        var tripObj = booking.getTrip(i);
        
        $(this).find('.tripNumber').text(i+1);
        $(this).attr('data-num', i+1);
        
        if (numTrips == 1 && i == 0) {
            $(this).find('.tripClose').hide();
        }
    
        var datepicker = $(this).find('.date-picker-control').data('datepicker');
        var currentDate = tripObj.getDate();

        if (lastpicker) {
            //try
            {
                datepicker.setStartDate(lastpicker);
            }
            /*catch (e) { console.log(e);
                datepicker.input.datepicker('option', 'minDate', lastpicker);
            }*/
        }
            
        if (currentDate)
            lastpicker = new Date(currentDate);
    });
};

var cycleTimes = function(key, trip) {
    var list = trip.find('.time-picker .dropdown-menu');

    list.empty();
    list.bs_dropdowntext('Time', false);
    
    var locTimes = sBook.lockeys[key].times;
    var locNonStdTime = sBook.lockeys[key].timesNonStd;
    
    if (!locNonStdTime) locNonStdTime = [];
    
    for (var i=0; i<locTimes.length; i++) {
        var time = locTimes[i];

        var li  = $('<li>');
        var a   = $('<a>', {'data-time' : time, 'tabindex' : '0'}).text(time);

        if (locNonStdTime.indexOf(time) !== -1)
            li.addClass('red');

        li.append(a);
        list.append(li);
    }
    
    var li = $('<li>');
    var a = $('<a>', { 'data-time' : '', 'tabindex' : '0'}).html('<i>Empty</i>');
    
    li.append(a);
    list.prepend(li);
};

var cyclePricesFromDb = function() {
    var trips = booking.getTrips();
    var totalPrice = 0;
    var totalPriceDb = 0;

    for (var i=0; i<trips.length; i++) {
        var dom = trips[i].dom();
        if (!dom)
            continue;

        var origin = trips[i].getLocationKey('origin');
        var dest = trips[i].getLocationKey('dest');

        var domPrice = dom.find('input.tripPrice');
        var sugg = dom.find('input[name=auto_price]').is(':checked');
        var domSuggText = dom.find('.suggestedPrice');

        var useNum = trips[i].getNum();
        
        trips[i].setPriceId(null);

        domPrice.data('price', null)
            .prop('disabled', 'disabled')
            .removeClass('price-db price-dirty');
            //.val(0);
        dom.find('.tripWarn').text('');

        if (!useNum || !origin || !dest) {
            continue;
        }
        else {
            var value;
            try {
                value = sBook.priceMap[origin][dest][useNum];
            }
            catch (e) {
                dom.find('.tripWarn').text('No Price for this Origin/Destination');
                if (sugg) domPrice.val(0);
                continue;
            }

            var price = sBook.tripPriceCalc(value, i, booking);
            if (!price)
                continue;
            
            domSuggText.text(price);

            domPrice.data('price', price);

            if (sugg) {
                domPrice.val(price.toFixed(2));
            }
            else {
                domPrice.prop('disabled', false);
            }

            priceFeedback.call(domPrice);

            trips[i].setPriceId(value._id);

            totalPriceDb += +price;
            totalPrice += +domPrice.val();
            trips[i].setPrice(+domPrice.val());
        }
    }

    $('.suggestedTotal').text(totalPrice);
    $('#custom-price').prop('disabled', 'disabled');

    if ($('input[name=auto_total_price]').is(':checked')) {
        $('#custom-price').data('price', totalPrice);
        $('#custom-price').val(totalPrice.toFixed(2)).prop('disabled', 'disabled');
    }
    else {
        $('#custom-price').prop('disabled', false);
    }

    priceFeedback.call($('#custom-price'));
    
    if (totalPrice > 0 || totalPriceDb > 0 || booking.getPrice() > 0) {
        $('.show-price').show();
        updateAmountDue();
    }
    else {
        $('.show-price').hide();
        updateAmountDue(0);
    }
    //totalPrices();
};

var updateAmountDue = function(val) {
    var bookPrice = +workshop.val('totalPrice');
    booking.setPrice(bookPrice);
    
    var priceDue = +(val || (bookPrice - booking.getPricePaid()));

    workshop.set('amountDue', priceDue.toFixed(2));
};

var collectDates = function() {
    var dates = [];
    
    var tripLength = booking.tripCount();
    
    for (var i=0; i<tripLength; i++) {
        var date = booking.getTrip(i).getDate();
        var time = booking.getTrip(i).getTime();
        
        if (!date)
            continue;
        
        if (time)
            date += ' '+time;

        var tripDate = moment.utc(date, 'MM/DD/YYYY' + (time ? ' hh:mm A' : '')).subtract(sBook.TZO, 'm');
        
        dates.push({ date : tripDate.toISOString(), time : +!!time }); //converts to bool, then int
    }

    return dates;
};

var getBookingPreview = function() {
    if (!workshop.ready())
        return;
    
    var dates = collectDates();
    
    $.ajax({
        url : 'trips/dates',
        type : 'POST',
        dataType: "json",
        data : {dates : dates},
        success : function(res) {
            var html = [];
            var data = res.trips;
            var lastdate;
            var total = 0;
            
            for (var k=0; k<data.length; k++) {
                var _trip = data[k];
                
                if (_trip.booking) {
                    var dateTrip = (new SmartTrip()).load(_trip);
                    var dateBooking = new SmartBooking(_trip.booking);

                    var _bookTable = $("<table>");
                    _bookTable.append($('<tr>'));

                    for (var j=0; j<3; j++)
                        _bookTable.find('tr').append($('<td>'));

                    var dateContent = sBook.dateDisplayText(dateTrip.getDatetime(), {twodigit : false});
                    var typeContent = dateTrip.getTravelTypeString();
                    
                    if (lastdate && lastdate != dateContent) {
                        html.push('<b>'+total+' Total Pass '+lastdate+'</b>');
                        html.push('<hr class="slim bold ">');
                        total = 0;
                    }
                    
                    lastdate = dateContent;
                    
                    _bookTable.find('td:eq(0)').html(dateContent);
                    _bookTable.find('td:eq(1)').html(typeContent);
                    _bookTable.find('td:eq(2)').html('('+dateTrip.getNum()+')');
                    
                    total += dateTrip.getNum();
                    
                    var _html = _bookTable[0].outerHTML + dateBooking.data.name;

                    html.push(_html); 
                    html.push('<hr class="slim">');
                }
                /*else {
                    html.pop();
                    html.push('<br />');
                    html.push('<b>'+data[k].total+' Total Pass '+sBook.dateDisplayText(new Date(data[k].date), {date : false, twodigit : false})+'</b>');
                    html.push('<hr class="slim bold ">');
                }*/
            }
            
            if (lastdate && total > 0) {
                html.push('<b>'+total+' Total Pass '+lastdate+'</b>');
                //html.push('<hr class="slim bold ">');
                total = 0;
            }
            else
                html.pop();
            
            $('#booking-date-preview').html(html.join(''));
            
             if (data.length > 0) {
                $('#booking-date-preview').removeClass('hide');
            }
        }
    });
};

//DOM interactions
$(document).on('click', '.dropdown-menu a', function() {
    $(this).bs_dropdowntext($(this).text());
});

$(document).on('click', '.direction-menu a', function(e) {
    var trip = $(this).closest('.trip');
    var origin = $(this).data('origin');
    var dest = $(this).data('dest');

    directionMenuChange(trip, 'origin', origin);
    directionMenuChange(trip, 'dest', dest);
    cyclePricesFromDb();
});

$(document).on('click', '.locationPlace li a', function(e) {
    e.preventDefault();
    var trip = $(this).closest('.trip');
    var direction = $(this).closest('.trip-place').attr('data-direction');
    var id = $(this).attr('data-id');

    booking.getTrip(+trip.attr('data-num')-1).setPlace(direction, id);
});

$(document).on('click', '#traveler-amount li a', function(e){
    var numOfTravelers = +$(this).attr('data-num');

    booking.setNum(numOfTravelers);
    cyclePricesFromDb();
});

$(document).on('click', '.trip-traveler-amount a', function(e){
    var numOfTravelers = +$(this).attr('data-num');
    var tripNum = +$(this).closest('.trip').attr('data-num');
    
    booking.getTrip(tripNum-1).setNum(numOfTravelers);
    cyclePricesFromDb();
});

var priceFeedback = function() {
    var value = +$(this).val();
    var dataValue = +$(this).data('price');

    $(this).removeClass('price-db price-dirty');

    if (!dataValue)
        return;

    if (value == dataValue) {
        $(this).addClass('price-db');
    }
    else {
        $(this).addClass('price-dirty');
    }
    
    updateAmountDue();
};

$(document).on('change keyup', 'input.tripPrice, #custom-price', priceFeedback);

$(document).on('change', 'input.tripPrice, #custom-price', cyclePricesFromDb);

$(document).on('shown.bs.dropdown', '.locationPlace .dropdown', function (e) {
    //$(this).splitColumn();
    //        return;
    var dropdown = $(e.target).children('.dropdown-menu');
    
    var offset = dropdown.offset();
    var parentOffset = dropdown.parent().offset();
    var position = dropdown.position();
    
    var dim = {
        'width' : dropdown.width(),
        'height' : dropdown.height()
    };
    var screen = {
        'width' : $(window).width(),
        'height' : $(window).height()
    };
    var scroll = {
        top : $(window).scrollTop(),
        left : $(window).scrollLeft()
    };
    
    if (offset.left + dim.width > screen.width) {
        var pos_x = Math.max(0, screen.width - dim.width) - offset.left;
        dropdown.css('left', pos_x);
    }
    if (offset.top + dim.height > screen.height+scroll.top) {
        var pos_y = dim.height - (screen.height - parentOffset.top) + position.top/2;//-((screen.height - dim.height) - position.top);
        dropdown.css('top', -pos_y + scroll.top);
    }
    
    /*console.log(position);
    console.log(parentOffset);
    console.log(offset);
    console.log(dim);
    console.log(screen);*/
  // do something…
});
$(document).on('hide.bs.dropdown', '.locationPlace .dropdown', function (e) {
    var dropdown = $(e.target).find('.dropdown-menu');
    
    dropdown.removeAttr('style');
});

$(document).on('click', '.time-picker li a', function(e){
    var tripNum = +$(this).closest('.trip').attr('data-num');
    var time = $(this).attr('data-time');

    if (time == '') {
        time = null;
        $(this).bs_dropdowntext('Time', false);
    }
    
    booking.getTrip(tripNum-1).setTime(time);
    getBookingPreview();
});
$(document).on('changeDate', '.date-picker', getBookingPreview);
//$(document).on('change', '.date-picker input', getBookingPreview);
$(document).on('click', '.place .dropdown li, .trip-type.dropdown li', function(e){
    e.preventDefault();
    //collectPrices();
});

$(document).on('change', 'input[name=auto_total_price], input[name=auto_price]', function(e){
    var parent = $(this).closest('.trip-price');
    var checked = $(this).prop('checked');
    var priceBox = parent.find('.price-input');
    
    cyclePricesFromDb();
    
    if (!checked) {
        var len = priceBox.val().length;
        priceBox[0].focus();
        priceBox[0].setSelectionRange(len, len);
    }
});

$('#addTrip').on('click', function(e){
    var trip = addTrip(e);
    var focus = trip.find('.people-amount button').focus();

    cyclePricesFromDb();
});
$(document).on('click', '.tripClose', removeTrip);

$(document).ready(function(e){
    $('input[name="phone-number[]"]').intlMask();
});

$(document).on('click', '.side-button', function(e){
    e.preventDefault();
    
    var textarea = $(this).siblings('textarea');

    textarea.toggleClass('high').fadeIn();
});

$.fn.intlMask = function() {
    var mainMask = '~00-000-0000';
    var intlMask = '+~!!-~~~-~~~-~~~~';

    var _translation = {
            '~' : { pattern : /[\+0-9]/ },
            '!' : { pattern : /[\+0-9]/, optional : true }
        };
      
    $(this).each(function(i){
        var _this = $(this);
        var options =  {
          onKeyPress: function(cep, e, field, options) {
              var masks = [mainMask, intlMask],
                  mask = (cep.charAt(0) === '+') ? masks[1] : masks[0];

              _this.mask(mask, options);
          },

          translation : _translation
        };

        $(this).mask(mainMask, options);
    });
};

var bucketNotify = function(options, fixed) {
    var home = $('#noty-home');
    
    if (fixed)
        return noty(options);
    else {
        $.smoothScroll({ scrollTarget : home, offset : -50 });
        return home.noty(options);
    }
};

var bindFormControls = function(form) {
    /*form.find('select:not(.select2-offscreen)').select2({
        minimumResultsForSearch : 100
    });
    form.find('.select2-arrow').remove();*/
};

var watchBooking = function() {
    if (booking.cancelled)
        $('#cancelled-ribbon').removeClass('hide');
    else
        $('#cancelled-ribbon').addClass('hide');
    
    if (booking.quote) {
        $('#quote-ribbon').removeClass('hide');
        bookingTypeName = 'Quote';
    }
    else {
        $('#quote-ribbon').addClass('hide');
        bookingTypeName = 'Reservation';
    }
    $('#cancel-button').text('Cancel '+bookingTypeName);
    
    if (booking.cancelled || typeof loadConfirmCode === 'undefined')
        $('#cancelBox').addClass('hide');
    else
        $('#cancelBox').removeClass('hide');
};

$(document).on('keypress', '.dropdown-menu li a', function(e){
    if (e.keyCode == 13) {
        $(this).click();
        
        var dropdown = $(this).closest('.dropdown');
        
        dropdown.find('button').focus();
        //var next = dropdown.nextAll(':input,.dropdown').eq(0).focus();
        //var inputs = $(this).closest('#addResForm').find(':input:visible:enabled,.dropdown:visible:enabled');
        //var that = this;
        //window.setTimeout(function(){
        //    inputs.eq( inputs.index(that)+ 2 ).focus();
        //}, 100);
        //console.log(inputs.eq( inputs.index(this)+ 2 ));
    }
});

$.fn.moveToNext = function() {
    
    var that = this;
    window.setTimeout(function(){
        var inputs = $(that).closest('#addResForm').find('.active input:visible:enabled,.active .dropdown');
        var next = inputs.eq( inputs.index(that)+ 1 ).focus();
        console.log(inputs);
        console.log(that);
        console.log(inputs.index(that));
    }, 100);
    //console.log(next);
};