sBook.init();

var bookingsTable;
var tableType = 'trips';

var filter = {
    'sm_dateView' : Date.now(),//$('#calendar').fullCalendar('getDate').toDate()
    'sm_dateLength' : 1,
    'sm_dateStart' : Date.now(),
    'sm_dateEnd' : Date.now(),
    'sm_quote' : 0,
    'sm_cancelled' : 0,
    'sm_byCreated' : 0
};

function fnLengthCustom(obj) {
    var _this = this;

    obj.empty();
    var settings = _this.api().settings()[0];

    var cust = $('<div />', {
        class : 'cust_date_range clearfix'
    });

    var oDateStart  = new Date(filter.sm_dateStart);
    var oDateEnd    = new Date(filter.sm_dateEnd);

    var dateStart = $('<input />', {
        class : 'cust_date_start',
        name : settings.sTableId+'_cust_start',
        //'data-provide' : 'datepicker', //breaks stuff
        'data-date-autoclose' : true
    }).val(oDateStart.toLocaleDateString());

    var dateEnd = $('<input />', {
        class : 'cust_date_end',
        name : settings.sTableId+'_cust_end',
        //'data-provide' : 'datepicker', //breaks stuff
        'data-date-autoclose' : true
    }).val(oDateEnd.toLocaleDateString());

    var select = $('<select />', {
            name : settings.sTableId+'_cust_length',
            class : 'form-control'
        })
        .width(100);

    var options = 
            "<option value='custom'>Custom</option>" + 
            "<option value='"+(-2)+"'>Tomorrow</option>" + 
            "<option value='"+(1)+"'>Today</option>" + 
            "<option value='"+(2)+"'>Yesterday</option>" + 
            "<option value='"+(7)+"'>Last 7 days</option>" + 
            "<option value='"+(14)+"'>Last 14 days</option>" + 
            "<option value='"+(30)+"'>Last 30 days</option>" 
            ;

    select.change(function(e){
       var length = $(this).val();

       if (length == 'custom') {
           filter.sm_dateLength = 0;
           cust.show();
       }
       else {
           filter.sm_dateLength = +length;
           cust.hide();
       }

       _this.api().ajax.reload();
    });

    dateStart.datepicker().change(function(e){
        e.preventDefault();
        var date = new Date($(this).val());

        filter.sm_dateStart = date.getTime();
        _this.api().ajax.reload();
    });

    dateEnd.datepicker().change(function(e){
        e.preventDefault();
        var date = new Date($(this).val());

        filter.sm_dateEnd = date.getTime();
        _this.api().ajax.reload();
    });

    cust.append(dateStart)
        .append(' - ')
        .append(dateEnd);

    if (filter.sm_dateLength != 0)
        cust.hide();

    select.html(options);
    //select.find('option[value='+filter.sm_dateLength+']')
    select.val(filter.sm_dateLength == 0 ? 'custom' : filter.sm_dateLength);
    
    var createdButton = $('<button />', {
        class : 'btn btn-sm dataFilterCreate form-control'
    }).text('Made');
    
    
    var typeSelect = $('<select />', {
        name : settings.sTableId+'_type',
        class : 'form-control'
    })
    .width(100);
        
    var typeOptions = 
            "<option value=''>Current</option>" + 
            "<option value='sm_quote'>Quotes</option>" + 
            "<option value='sm_cancelled'>Cancellations</option>"
            ;
   
    typeSelect.html(typeOptions);
    
    typeSelect.val(filter.sm_quote == 1 ? 'sm_quote' :
              (filter.sm_cancelled == 1 ? 'sm_cancelled' : '')
            );
    
    typeSelect.change(function(e){
        var val = $(this).val();
        
        filter.sm_quote = 0;
        filter.sm_cancelled = 0;
        
        if (val)
            filter[val] = 1;
        
        switchTableDetect();
    });
    
    if (filter.sm_byCreated == 1) {
        createdButton.addClass('active');
    }
    
    createdButton.click(function(e){
        e.preventDefault();
        
        if ($(this).is('.active')) {
            $(this).removeClass('active');
            fnToggleCreated(false);
        }
        else {
            $(this).addClass('active');
            fnToggleCreated(true);
        }
    });
    
    var inputLeft = $('<div />', {
        'class' : 'pull-left'
    });
    
    var inputRight = $('<div />', {
        'class' : 'pull-right form-inline'
    });
        
    createdButton.appendTo(inputLeft);
        
    cust.appendTo(inputRight);
    typeSelect.appendTo(inputRight);
    inputRight.append('&nbsp;');
    select.appendTo(inputRight);
    
    inputLeft.appendTo(obj);
    inputRight.appendTo(obj);
}

function fnToggleCreated(on) {
    if (on) {
        bookingsTable.column(9).visible(true);
        bookingsTable.order([[9, 'desc'], [1, 'asc']]);
        
        filter.sm_byCreated = 1;
    }
    else {
        bookingsTable.column(9).visible(false);
        bookingsTable.order([[1, 'asc']]);
        
        filter.sm_byCreated = 0;
    }
    
    switchTableDetect();
};

var switchTableDetect = function() {
    var toDo;
    
        /*toDo = (filter.sm_byCreated == 1 ||
        filter.sm_quote == 1 ||
        filter.sm_cancelled == 1) ? 'bookings' : 'trips';*/

    tableType = (filter.sm_byCreated == 1) ? 'bookings' : 'trips';
    
    bookingsTable.ajax.url(
        (tableType == 'bookings') ? 'datatable/bookingDataTable' : 'datatable/tripDataTable'
    ); 
    //if (toDo == tableType)
    //    return;
    //console.log(toDo);
    //tableType = toDo;
    
    bookingsTable.ajax.reload();
};
// ok so datatables doesn't let us modify the languages directly, BLAH
// so, we hackishly have a function object that will always return a string
// based on the current state

var currentInfoString = function() {
    this.toString = function() {
        return tableType == 'bookings' ? 
        'Showing _START_ to _END_ of _TOTAL_ bookings' :
        'Showing _START_ to _END_ of _TOTAL_ reservations';
    };
};

$(document).ready(function () {
    
    var bookingStorage = {};
    
    var makeTable = function(){
        var _fnSearchDisable = function(table) {
            var value = table.find('.dataTables_filter input').val();

            if (value == '') {
                $('.dataTables_length :input').prop('disabled', false);
            }
            else {
                $('.dataTables_length :input').prop('disabled', true);
            }
        };
        
        bookingsTable = $('#bookingTable').DataTable({
            "processing": true,
            "serverSide": true,
            "pageLength" : 100,
            "language": {
                "searchPlaceholder": "Search",
                "info": new currentInfoString,
                "infoEmpty": " "
            },
            "dom": 'flrtsip',
            "ajax": {
                url : "datatable/tripDataTable",
                data : function(data) {
                    var clientDate = new Date();
                    
                    data.filter = filter;
                    data.clientDate = clientDate.toDateString();
                    data.clientTime = clientDate.toTimeString();
                    
                    return data;
                },
                dataSrc : function(json) {
                    for (var i=0; i<json.data.length; i++) {

                       var trip_base = json.data[i];
                       var booking_base = json.tableType == 'bookings' ?
                        trip_base :
                        trip_base.booking;
                       //console.log(tableType);
                       if (tableType == 'trips') {
                           json.data[i].trip_obj = new SmartTrip();
                           json.data[i].trip_obj.load(trip_base);
                       } else {
                            json.data[i].booking = json.data[i];
                            json.data[i].booking_obj = new SmartBooking(booking_base);
                            json.data[i].tripIndex = json.data[i].booking_obj.getTripIndex(json.data[i]._id);
                        }
                    }
                    return json.data;
                }
            },
            "columns": [
            {
                data : '_id',  //we need this because it is global to both models
                // if we didn't have it, the server-side datables might say we
                // haven't selected any valid fields
                width : '30px',
                sortable : false,
                className : 'rowLinks',
                render : function(obj, type, data){
                    return "<a href=\"create/"+data.booking.confirmationCode+"\"><i class=\"icon-pencil\"></i></a>" + 
                           "&nbsp;<a href=\"#\" class=\"bookingRowOpen\"><i class=\"icon-folder-open\"></i></a>";
                },
                searchable : false
            },

            {
                data: 'dateTime',
                defaultContent : ' ',
                width : '110px',
                render : function(obj, type, data) {
                    var html = [];
                    
                    if (data.trip_obj) {
                        html.push(data.trip_obj.getDatetimeString());
                    }
                    else {
                        for (var i=0, len = data.booking_obj.tripCount(); i<len; i++) {
                            var date = data.booking_obj.getTripDatetime(i);
                            var dateStr = data.booking_obj.getTripDatetimeString(i);

                            //if (!moment(date).isSame(data.dateTime, 'day')) {
                            if (i != data.tripIndex && tableType == 'trips') {
                                html[i] = '<span class="text-heavy-muted">'+data.booking_obj.getTripDatetimeString(i)+'</span>';
                            }
                            else {
                                html[i] = data.booking_obj.getTripDatetimeString(i);
                            }
                        }
                    }
                    return html.join('<br />');
                },
                searchable : false
            },

            {
                data: 'booking.name',
                width : '120px',
                sortable : false,
                defaultContent : ' ', 
                render : function(obj, type, data) {
                    return data.booking.name;
                }
            },
            
            {
                data: null,
                width : '15px',
                sortable : false,
                searchable : false,
                defaultContent : ' ', 
                render : function(obj, type, data) {
                    try {
                        var trip = data.booking_obj.getTrip(0);
                    }
                    catch (e) {
                        return obj.travelers;
                    }
                    
                    return tableType == 'bookings' ? (trip ? trip.getNum() : 0) : obj.travelers;
                }
            },
            
            {
                data: null,
                width: '125px',
                defaultContent : ' ', 
                render : function(data, type, row){
                    if (tableType != 'trips') {
                        try {
                            return row.booking_obj.getTravelTypeString(row.tripIndex, tableType == 'trips');
                        }
                        catch (e) {
                            return row.trip_obj.getTravelTypeString();
                        }
                    }
                    else {
                        return row.trip_obj ? row.trip_obj.getTravelTypeString() : '';
                    }
                },
                sortable : false,
                searchable : false
            },
            
            {   
                data: 'booking.specialRequest',
                width : '180px',
                sortable : false,
                render : function(data, type, row){
                    var text = row.booking.specialRequest;

                    var maxLength = 64;
                    var part1 = text.slice(0, maxLength);
                    var part2 = text.slice(maxLength);

                    var compiled = part1;
                    if (part2.length > 0) {
                        compiled += '<b class="dt_text_hidden_ellips">...</b>';
                        compiled += '<span class="dt_text_overflow">'+part2+'</span>';
                    }
                    return compiled;
                }
            },
            
            {
                data: 'booking.phoneNumber',
                width: '90px',
                render : function(data, type, row){
                    return row.booking.phoneNumber.join("\n<br />");
                },
                sortable : false,
                searchable : false
            },
            
            {
                data: 'booking.email',
                width : '100px',
                sortable : false,
                render : function(data, type, row) {
                    return row.booking.email.join(', ');
                }
            },
            
            {
                data: 'booking.confirmationCode',
                width : '45px',
                sortable : false,
                render : function(obj, type, data) {
                    return data.booking.confirmationCode;
                }
            },
            
            {
                data: 'dateCreated',
                width : '120px',
                visible : false,
                defaultContent : ' ',
                searchable : false,
                render : function(obj, type, data){
                    var date = new Date(data.dateCreated);
                    return sBook.dateDisplayText(date);
                }
            }  
            ],
            'destroy' : true,
            "createdRow": function( nRow, aData, iDataIndex ) {
                $(nRow).attr('data-dbid', aData._id);
                $(nRow).attr('data-code', aData.booking.confirmationCode);
                //$(nRow).data('booking', new SmartBooking());
            },
            "preDrawCallback": function() {
                var wrapper = $(this).closest('.dataTables_wrapper');
                
                var length_input = wrapper.find('.dataTables_length'); 
                if (typeof fnLengthCustom !== 'undefined')
                    fnLengthCustom.call(this, length_input);
                
                _fnSearchDisable(wrapper);
            },
            'order' : [[1, 'asc']],
            autoWidth :     false,
            deferRender:    true,
            scrollY:        400,
            scrollCollapse: true
        });
        
        bookingsTable.on('search.dt', function(e, settings){
            _fnSearchDisable($(this));
        });
        
        //$('.dataTables_filter input').focus();
    };
    
    sBook.promise().then(makeTable);
    
    $(document).on('click', '.bookingRowOpen', function(e){
        e.preventDefault();
        
        var row = bookingsTable.row($(this).closest('tr[role="row"]'));
        
        var load = function(booking) {
            var table = $('<table>').addClass('details');
            var tableRow = $('<tr>');

            var price = booking.getPrice();
            var tableCell = $('<td>');
            var tableCellText = '<b>Total Price: </b>'+(price ? '$'+price : '?');

            tableCell.html(tableCellText);
            tableRow.append(tableCell);

            for (var i=0; i<booking.tripCount(); i++) {
                var tableCell = $('<td>');
                var tableCellText = '<b>'+booking.getTripDatetimeString(i)+'</b>' +
                                    '<br /><b>From:</b> '+booking.getTripPlaceString(i, 'origin') +
                                    '<br /><b>To:</b> '+booking.getTripPlaceString(i, 'dest');

                tableCell.html(tableCellText);
                tableRow.append(tableCell);
            }

            var dateCreated = new Date(booking.data.dateCreated);
            var dateCreatedStr = dateCreated.toLocaleDateString() + ' ' + dateCreated.toLocaleTimeString(navigator.language, {hour: '2-digit', minute:'2-digit'});

            var notesHandle = "<br  /><textarea name=\"notes\" rows=\"2\" cols=\"50\" data-code=\""+booking.data.confirmationCode+"\" data-id=\""+booking._id+"\"></textarea> <a href=\"#\" class=\"noteSave\"><i class='icon-save'></i></a>";
            tableRow.append("<td><b>Created:</b> "+dateCreatedStr+notesHandle+"</td>");

            table.append(tableRow);

            if (booking.data.notes) {
                table.find('textarea[name=notes]').val(booking.data.notes);
            }
            row.child( table[0] ).show();
            $(row.child()).addClass('detailRow');
        };
        
        if ( row.child.isShown() ) {
            row.child.hide();
            $(this).removeClass('open');
        }
        else {
            var code = $(this).closest('tr[role="row"]').attr('data-code');
            
            row.child( 'Loading...' ).show();
            $(row.child()).addClass('detailRow');
                
            if (bookingStorage[code]) {
                load(bookingStorage[code]);
            }
            else {
                $.ajax({
                    url : 'booking/'+code
                })
                .then(function(data){
                    var booking = new SmartBooking(data);

                    load(booking);
                    bookingStorage[code] = booking;
                    //$(this).addClass('open');
                });
            //var data = row.data();
            //var booking = data.booking_obj;
            }
        }
    });

    $(document).on('click', '.noteSave', function(e){
        e.preventDefault();
        
        var noteObj = $(this).siblings('textarea[name=notes]');
        var code = noteObj.attr('data-code');
        var sendData = {
            notes : noteObj.val()
        };
        
        var row = bookingsTable.row($(this).closest('tr.detailRow').prev('tr'));

        $.ajax({
            url  : 'booking/'+code+'/note',
            method : 'post',
            data : sendData,
            success : function(d) {
                var data = row.data();
                data.notes = sendData.notes;
                row.data(data);
                data.booking_obj.data.notes = sendData.notes;
            }
        });
    });
}); //end ready function
