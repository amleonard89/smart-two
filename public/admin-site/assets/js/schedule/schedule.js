var app = angular.module('schedule', ['ngSanitize']);

sBook.init();

app.directive('scheduleDay', function(){
    return {
        templateUrl : 'views/sch_day.html',
        restirct : 'A'
    };
});
app.directive('scheduleTrip', function(){
    return {
        templateUrl : 'views/sch_trip.html',
        restirct : 'A',
        controller : 'tripCtrl'
    };
});
app.directive('scheduleDayStaff', function(){
    return {
        templateUrl : 'views/sch_day_staff.html',
        restirct : 'A',
        controller : 'staffCtrl'
    };
});
app.directive('scheduleDrivers', function($timeout){
    return {
        templateUrl : 'views/sch_drivers.html',
        restirct : 'E',
        controller : 'driverCtrl',
        link : function(scope, elem, attrs, ctrl) {
            $timeout(function(){
                elem.find('select').removeClass('hide').select2({
                    width : '150px'
                });
            });
        }
    };
});

app.controller('dayCtrl', ['$scope', '$http', 'staff', function($scope, $http, staff) {
    var today = moment().utcOffset(sBook.TZO);
    
    var format = 'M/D/YYYY';
    var dateStart = today.startOf('day').format(format);
    var dateEnd = today.endOf('week').add(1, 'day').format(format);
    
    /*if (typeof Cookies !== 'undefined') {
        var cDateStart = Cookies.get('schedule_dateStart');
        var cDateEnd = Cookies.get('schedule_dateEnd');
        
        if (cDateStart)
            dateStart = cDateStart;
        
        if (cDateEnd)
            dateEnd = cDateEnd;
    }*/
    
    $scope.dateStart = dateStart;
    $scope.dateEnd = dateEnd;
    $scope.tripsByDay = {};
    $scope.staffSchedule = {};
    $scope.downloadLink = null;
    $scope.isLoading = false;
    
    var retriveTrips = function() {
        sBook.promise().then(function(){
            var trips = {};
            var schedule = {};
            
            var mCurrent = moment($scope.dateStart+' '+sBook.TZ, format+' ZZ').startOf();
            var mEnd = moment($scope.dateEnd+' '+sBook.TZ, format+' ZZ').startOf();

            if (typeof Cookies !== 'undefined') {
                Cookies.set('schedule_dateStart', $scope.dateStart);
                Cookies.set('schedule_dateEnd', $scope.dateEnd);
            }
            
            while (mCurrent <= mEnd) {
                trips[mCurrent.valueOf()]= [];
                schedule[mCurrent.valueOf()]= [];
                mCurrent.add(1, 'd');
            }
            
            $scope.isLoading = true;
            
            $http({
                method : 'GET',
                url : 'scheduleDays',
                params : {
                    start : $scope.dateStart,
                    end : $scope.dateEnd
                }
            }).then(function(res){
                var _trips = res.data.trips;
                var _staff = res.data.schedule;
                
                for (var i=0; i<_trips.length; i++) {
                    var booking = new SmartBooking(_trips[i]);
                    var trip = booking.getTripById(_trips[i]._id);

                    var dayIndex = moment(trip.getDatetime())
                            .utcOffset(sBook.TZO)
                            .startOf('day')
                            .valueOf();

                    if (!trips[dayIndex])
                        continue;
                    
                    trips[dayIndex].push(trip);
                }

                $scope.tripsByDay = trips;
                                
                $scope.staffSchedule = staff.staffDay(_staff, schedule);

                //$scope.staffSchedule = schedule;
            })
            .finally(function(){
                $scope.isLoading = false;
            });
            
            $scope.downloadLink = null;
        });
    };
    
    $scope.$watchGroup(['dateStart', 'dateEnd'], retriveTrips);
}]);

app.controller('tripCtrl', ['$scope', '$sce', '$http', function($scope, $sce, $http){
   var afterTrips = function() {
       var booking = $scope.trip.parent;
       var trip = $scope.trip;
       var rt = [];
       
       for (var i=trip.index+1; i<booking.tripCount(); i++) {
           
           rt.push(booking.getTrip(i).getTime());
       }
       
       return rt.length ? 'RT '+rt.join('/') : '';
   };
   
   var saveScheduleNotes = function() {
        var trip = $scope.trip;
        var scheduleNotes = trip.data.scheduleNotes;
        
        $http({
                method : 'POST',
                url : 'scheduleNotes/'+trip.id,
                data : {
                    trip_id : trip.id,
                    scheduleNotes : scheduleNotes
                }
            })
            .then(function(res){
                        
            });
   };
   
   $scope.txtPostName = afterTrips();
   
   $scope.saveScheduleNotes = saveScheduleNotes;
}]);

app.controller('driverCtrl', ['$scope', '$http', '$timeout', 'staff', function($scope, $http, $timeout, staff){
    $scope.staff = {};
    
    staff.promise.then(function(staff){
        $scope.staff = staff;
    });

    var saveDrivers = function() {
        var trip = $scope.trip;
        var drivers = trip.data.drivers;
        var driverNotes = trip.data.driverNotes;
        

        $http({
                method : 'POST',
                url : 'driver/'+trip.id,
                data : {
                    trip_id : trip.id,
                    drivers : drivers,
                    driverNotes : driverNotes
                }
            })
            .then(function(res){
                        
            });
    };
    
    $scope.saveDrivers = saveDrivers;
    
    /*$scope.$watch('trip.data.drivers', function(_new, _old){
        if (_new != _old)
            saveDrivers();
    });*/
}]);

app.controller('staffCtrl', ['$scope', '$http', 'staff', function($scope, $http, staff){
    $scope.staff = {};
    
    staff.promise.then(function(staff){
        $scope.staff = staff;
    });
    
    var addStaffDay = function(day) {
        if (!(day in $scope.staffSchedule))
            return;
        
        var start = '';
        var end = '';
        
        var _day = $scope.staffSchedule[day];
        if (_day.length === 0) {
            start = '7:00 AM';
            end = '6:00 PM';
        }
        else {
            start = _day[_day.length-1].end;
            end = '6:00 PM';
        }
        
        $scope.staffSchedule[day].push({
            name : '',
            start : start,
            end : end
        });
    };
    
    var delStaffDay = function(day, index) {
        $scope.staffSchedule[day].splice(index, 1);
    };
    
    var saveStaffDay = function() {
        var form = $scope.form;
        var day = $scope.day;
        var staffList = $scope.staffSchedule[day];

        $scope.$parent.isLoading = true;
        
        $http({
                method : 'POST',
                url : 'scheduleStaff/date',
                params : {
                    day : day
                },
                data : {
                    staff : staffList
                }
            }).then(function(res){
                $scope.staffSchedule[day] = staff.staffDay(res.data);
                
                form.$setPristine();
            })
            .finally(function(){
                $scope.$parent.isLoading = false;
            });
    };
    
    $scope.addStaffDay = addStaffDay;
    $scope.delStaffDay = delStaffDay;
    $scope.saveStaffDay = saveStaffDay;
}]);

app.service('staff', function($http) {
    var drivers = [];
    var staff = [];
    
    var staffDay = function(_staff, _schedule) {
        var schedule = _schedule || [];
        
        var _last = null;
        for (var i=0; i<_staff.length; i++) {
            var staffer = _staff[i];
            var dayIndex = moment(staffer.start)
                    .utcOffset(sBook.TZO)
                    .startOf('day')
                    .valueOf();

            if (!schedule[dayIndex])
                schedule[dayIndex] = [];
            
            schedule[dayIndex].push({
                start   : moment(staffer.start).utcOffset(sBook.TZO).format('h:mm A'),
                end     : moment(staffer.end).utcOffset(sBook.TZO).format('h:mm A'),
                name    : staffer.name
            });
            
            _last = dayIndex;
        }

        return _schedule || !_last ? schedule : schedule[_last];
    };
    
    var promise = $http({
        method : 'GET',
        url : 'staff'
    }).then(function(res){
        drivers = res.data.drivers;
        staff = res.data.staff;
        
        return {
            drivers : drivers,
            staff : staff
        };
    });
    
    return {
        promise : promise,
        
        staffDay : staffDay
    };
});

app.filter('day_suffix', function(){
    return function(stamp) {
        var date = (new Date(stamp)).getDate();

        if (date == 1)
            return 'st';
        if (date == 2)
            return 'nd';
        if (date == 3)
            return 'rd';
        return 'th';
    };
});

app.filter('join_br', function($sce){
    return function(arr) {
        var str = arr.join("<br />");
        return $sce.trustAsHtml(str);
    };
});
app.filter('join_phone', function($sce){
    return function(arr) {
        var html = [];
        for (var i=0; i<arr.length; i++) {
            var number = arr[i];
            var filtered = number.replace(/[^0-9x\+]/g, '');
            
            var code = '<a href="tel:'+filtered+'" class="tel">' + number + '</a>';
            
            html.push(code);
        }
        var str = html.join("<br />");
        return $sce.trustAsHtml(str);
    };
});

$(document).on('click', '.makePdf', function(e){
    var html = convertPDF($('#schedulePdfHolder'));
    var dayCtrl = angular.element($('#dayCtrl').eq(0)).scope();
    
    dayCtrl.isLoading = true;
    dayCtrl.$apply();
    
    $.ajax({
        url : 'schedule/pdf',
        method : 'post',
        data : { html : html, start : dayCtrl.dateStart, end: dayCtrl.dateEnd }
    })
    .then(function(data){
        dayCtrl.$apply(function(){
            dayCtrl.downloadLink = data.link;
        });
    })
    .done(function(){        
        dayCtrl.$apply(function(){
            dayCtrl.isLoading = false;
        });
    });
});

var convertPDF = function(elem) {
    var html = elem.clone(true, true);
    
    /********
     remove staff schedule styles
     process goes like:
        1) staff schedules
            a) strip buttons
            b) convert select to text
            c) convert input to text
        2) drivers
            a) store drivers as text, emtpy elemet, insert driver text
    */
    html.find('.pdf-remove').remove();
    
    html.find('.sch_day_staff').each(function(i){
        $(this).find('button').remove();
        
        var staff   = $(this).find('select option:selected').text();
        var start   = $(this).find('input[name=start]').val();
        var end     = $(this).find('input[name=end]').val();
        
        $(this).text( staff+' '+start+' - '+end );
    });
    html.find('.day-staff').find('li:not(.sch_day_staff)').remove();
        
    html.find('.sch_day_drivers').each(function(i){
        var driverSelect = $(this).find('select');
        var drivers = angular.element(driverSelect.eq(0))
                .scope()
                .trip.data.drivers;
        
        var notes = $(this).find('textarea[ng-model="trip.data.driverNotes"]').val();
        
        $(this).empty();
        
        var text = drivers.join(' & ');
        
        if (drivers)
            text += "<br /><span class=\"text-small\">"+notes+"</span>";
        
        $(this).html(text);
    });
    
    html.find('textarea').each(function(i){
        var text = $(this).val();
        
        if (text)
            $(this).replaceWith(text);
        else
            $(this).remove();
    });
    
    html.find('*').contents().each(function() {
        if(this.nodeType === Node.COMMENT_NODE) {
            $(this).remove();
        }
    });

    return html.wrap('<div>').parent().html();
};