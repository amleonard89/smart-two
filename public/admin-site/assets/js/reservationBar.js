$(document).ready( function  () {
    //These are the different options that can be selected for trip type
    var options = [ "ONE WAY - Port Canaveral to Orlando Airport", 
                    "ONE WAY - Orlando Airport to Port Canaveral",
                    "ROUND TRIP - Between Orlando Airport and Port Canaveral",
                    "ONE WAY - Orlando Airport to Hotel",
                    "ONE WAY - Hotel to Orlando Airport",
                    "ROUND TRIP - Between Orlando Airport and Hotel",
                    "ROUND TRIP - Orlando Airport and Hotel then Cruise Ship to Orlando Airport"];

    function changeTravelType (newTravelType) {
        //Set the current travel type to the new one in order to have stored for later usage when saving the cookie.
        travelType = newTravelType;
        //If this is a one way to airport
        if (newTravelType == options[0])
        {         
            changeTravelTypeToCruiseShipToAirport();
        }
        //Round trip
        else if (newTravelType == options[2])
        {            
            changeTravelTypeToRoundTripCruiseAndAirport();
        }
        //One way to cruise
        else if (newTravelType == options[1])
        {            
            changeTravelTypeToAirportToCruiseShip();
        }
        // Orlando Airport to Hotel
        else if (newTravelType == options[3])
        {
            changeTravelTypeToAirportToHotel();
        }
        else if (newTravelType == options[4])
        {
            changeTravelTypeToHotelToAirport();
        }
        else if (newTravelType == options[5])
        {
            changeTravelTypeToRoundTripAirportAndHotel();            
        }
        else if (newTravelType == options[6])
        {
            changeTravelTypeToRoundTripAirportAndHotelCruiseShipAndAirport();            
        }
    }

    function changeTravelTypeToRoundTripAirportAndHotel (argument) {
        $('#dropdown-button-type').text( "Roundtrip - Airport - Hotel" );

            flightSection.show();
            hotelSection.show();
            cruiseSection.hide();

            oneWayToAirport = false;
            roundTrip = false;
            oneWayToHotel = true;            
            travelTypeId = 3;
            // moveToNextBox(1,2);
            smartButton.prop('disabled', true);
    }

    function changeTravelTypeToRoundTripAirportAndHotelCruiseShipAndAirport (argument) {
        $('#dropdown-button-type').text( "Threeway Trip" );

            flightSection.show();
            cruiseSection.show();
            hotelSection.show();

            oneWayToAirport = false;
            roundTrip = false;
            oneWayToHotel = true;            
            travelTypeId = 3;
            // moveToNextBox(1,2);
            smartButton.prop('disabled', true);
    }

    function changeTravelTypeToAirportToHotel () {
        $('#dropdown-button-type').text( "Airport to Hotel" );

            flightSection.hide();
            cruiseSection.hide();
            hotelSection.show();

            oneWayToAirport = false;
            roundTrip = false;
            oneWayToHotel = true;            
            travelTypeId = 3;
            // moveToNextBox(1,2);
            smartButton.prop('disabled', true);
    }

        function changeTravelTypeToHotelToAirport () {
        $('#dropdown-button-type').text( "Hotel to Airport" );

            flightSection.show();
            cruiseSection.hide();
            hotelSection.hide();

            oneWayToAirport = false;
            roundTrip = false;
            oneWayToHotel = false;

            travelTypeId = 3;
            // moveToNextBox(1,2);
            smartButton.prop('disabled', true);
    }

    function changeTravelTypeToAirportToCruiseShip () {
        $('#dropdown-button-type').text( "Airport to Cruise Ship" );

            flightSection.show();
            cruiseSection.hide();
            hotelSection.hide();

            oneWayToAirport = false;
            roundTrip = false;
            oneWayToCruise = true;            
            travelTypeId = 1;
            moveToNextBox(1,2);
            smartButton.prop('disabled', true);
    }

    function changeTravelTypeToRoundTripCruiseAndAirport () {
            $('#dropdown-button-type').text( "Roundtrip - Airport - Cruise" );

            cruiseSection.show();
            flightSection.show(); 
            hotelSection.hide();

            oneWayToAirport = false;
            roundTrip = true;
            oneWayToCruise = false;                
            travelTypeId = 2;

            moveToNextBox(1, 2);
            smartButton.prop('disabled', true);
    }

    function changeTravelTypeToCruiseShipToAirport () {
            $('#dropdown-button-type').text( "Cruise Ship to Airport" );

            flightSection.hide();
            cruiseSection.show();
            hotelSection.hide();

            oneWayToAirport = true;
            roundTrip = false;
            oneWayToCruise = false;

            smartButton.prop('disabled', false);
            travelTypeId = 0;

            // checkBox("#cruise-date-finished");
            moveToNextBox(1, 6);
    }

	$('#traveler-amount li > a').click(function (e) {
        $('#traveler-amount-box').text( this.innerHTML );        

        numOfTravelers = parseInt(this.innerHTML.substring(0, 1));
        numOfTravelersSelected = true;
        checkIfFinished();
    });

    $('#travel-type li > a').click(function(e){
        reset();
    	//Change the travel type variables and move onto the next circles showing whether or not something has been finished
        changeTravelType(this.innerHTML);

        //Reset all the inputs and the dropdown boxes to their default settings.        
    	$('#dropdown-button-type').append( '<span class="caret"></span>' );

	});

    //Click on cruise time, and display the selected item
    $('#cruise-time li > a').click(function(e){
        $('#dropdown-button-cruise').text( this.innerHTML + " " );
        $('#dropdown-button-cruise').append( '<span class="caret"></span>' );

        cruiseTime = this.innerHTML;

        getTimeAvailability (cruiseTime, cruiseDateEntered, cruiseTimeEntered, cruiseDate, cruiseTime, cruiseSoldOut, 7, true);
        checkIfFinished();
    });

    function getTimeAvailability (time, dateEntered, timeEntered, date, time, displaySoldOutSection, finishedImageTag, isCruise) {
        //If the user has already entered the flight date information then we check to see if this particular date and time
        //is available for booking, otherwise do nothing.

        if (dateEntered)
        {            
            //If a date is already selected then we want to see if this time and date is already sold out
            timeEntered = checkDateAndTimeAvailability (date, time, isCruise);
            //If the cruise time is not sold out
            if (timeEntered)
            {
                showBookingSuccessful(airportTimeSoldOut, finishedImageTag, finishedImageTag + 1);
            }
            else {
                priceSection.empty();
                priceTaglineSection.empty();

                changeStatusImage(finishedImageTag, 'Not Finished');
            }
        }
        else {
            //Display that as of right now we have not checked to see if you can book at this time
            showBookingSuccessful(displaySoldOutSection, finishedImageTag, finishedImageTag + 1);
            timeEntered = true;
        }

        if (!isCruise)
        {
            flightTimeEntered = timeEntered;
            flightDateEntered = dateEntered;       
        }
        else if (isCruise)
        {
            cruiseTimeEntered = timeEntered;
            cruiseDateEntered = dateEntered;
        }
    }

    //Click on cruise time, and display the selected item
    $('#flight-time li > a').click(function(e){
        $('#dropdown-button-flight').text( this.innerHTML + " " );
        $('#dropdown-button-flight').append( '<span class="caret"></span>' );
        
        flightTime = this.innerHTML;

        getTimeAvailability (flightTime, flightDateEntered, flightTimeEntered, flightDate, flightTime, airportTimeSoldOut, 3, false);
        checkIfFinished();
    });

    function showBookingSuccessful (section, firstTag, secondTag) {
        section.hide();
        checkIfFinished();
        moveToNextBox(firstTag, secondTag);
    }

    function changeStatusImage (tag, status) {

        if (status == 'Not Finished') 
        {
            $(".finished[tag=" + tag + "]").attr('src', "/smart-images/check-required.png");
            $(".finished[tag=" + tag + "]").attr('height', 10);
            $(".finished[tag=" + tag + "]").attr('width', 10); 
        }
    }

    function checkDateAndTimeAvailability (date, time, cruise) {

        //If the date and time is available then we return true
        var available;
        var currentTravelTypeId;

        var currentTravelTypeId;

        //Make sure that we check the documents that have the correct travel type.  That way even if it's round trip
        //we'll still check the correct travel type.
        //For example - if this is One way to cruise, we'll check for all documents that have the matching id even though,
        //the user has selected round trip.
        if (cruise)
        {
            currentTravelTypeId = 0;
        }
        else
        {
            currentTravelTypeId = 1;
        }

        checkDateAndTime(date, time, function (response) {
                      
            //If the date and time is booked completely
            if (response != "Success")
            {
                if (!cruise)
                {
                    airportTimeSoldOut.show();
                    airportSoldOut.hide();
                }
                else
                {
                    cruiseTimeSoldOut.show();   
                    cruiseSoldOut.hide();
                }
                available = false;            
            }
            else {
                available = true;
            }
            
        }, currentTravelTypeId);

        return available;
    }

    //Check to see if the date given is available
    function checkDateAvailability (date, cruise) {
        //If the date is available then we return true
        var available;
        var currentTravelTypeId;

        //Make sure that we check the documents that have the correct travel type.  That way even if it's round trip
        //we'll still check the correct travel type.
        //For example - if this is One way to cruise, we'll check for all documents that have the matching id even though,
        //the user has selected round trip.
        if (cruise)
        {
            currentTravelTypeId = 0;
        }
        else
        {
            currentTravelTypeId = 1;
        }

        //Check to see if we can have reservations on these dates.  If there is no more availabilities then display to the customer that this is the case.
        checkDate(date, function (response) {

            if (response != "Success")
            {
                if (!cruise)
                {
                    airportSoldOut.show();
                    airportTimeSoldOut.hide();
                    $('#dropdown-button-flight').prop('disabled', true);
                }
                else
                {
                    cruiseSoldOut.show();
                    cruiseTimeSoldOut.hide();
                    //Disable the ability to get the time
                    $('#dropdown-button-cruise').prop('disabled', true);
                }
                available = false;            
            }
            else {
                if (!cruise)
                {
                    $('#dropdown-button-flight').prop('disabled', false);                         
                }
                else
                {
                    $('#dropdown-button-cruise').prop('disabled', false);   
                }
                available = true;
            }
        }, currentTravelTypeId);

        return available;
    }

    $('#airline-dropdown li > a').click(function(e){
        $('#dropdown-button-airline').text( this.innerHTML + " " );
        $('#dropdown-button-airline').append( '<span class="caret"></span>' );
        airlineSelected = true;        
        checkIfFinished();
        //If they're going one way to the cruise ship then skip the flight number circle
        if (!oneWayToCruise)
        {
            moveToNextBox(4, 6);
        }
        else {
            moveToNextBox(4, 9);
        }
    });

    $('#cruiseline-dropdown li > a').click(function(e){
        $('#dropdown-button-cruiseline').text( this.innerHTML + " " );
        $('#dropdown-button-cruiseline').append( '<span class="caret"></span>' );
        cruiselineSelected = true;
        checkIfFinished();

        moveToNextBox(8, 9);
    });

    $('#hotels-dropdown li > a').click(function(e){
        $('#dropdown-button-hotel').text( this.innerHTML + " " );
        $('#dropdown-button-hotel').append( '<span class="caret"></span>' );
        cruiselineSelected = true;
        checkIfFinished();

        moveToNextBox(8, 9);
    });

    var now = new Date();
    var earliestDate = new Date();

	$('#dp3').datepicker(
        { 
            format : 'MM/dd/yyyy',
            onRender: function (date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            //We don't want to show that the date has been sold out once the date changes unless it actually is.
            airportSoldOut.hide();
       		$('#dp3 input').val($.format.date(ev.date, "MM/dd/yyyy"));
            
            flightDate = $('#dp3 input').val();

            getAvailability (flightTimeEntered, flightDateEntered, flightDate, flightTime, airportSoldOut, false, 2, $('#dropdown-button-flight'));

            checkIfFinished();
            //If this is a round trip then we make sure that the earliest date for the return date is later then the date just selected
            if (roundTrip)
            {
                //Set what the earliest date to one day ahead of whatever date just selected
                earliestDate = new Date(ev.date);
                earliestDate.setDate(earliestDate.getDate() + 1);   

                returnDate.setValue(earliestDate);             
            }
                
            $(this).datepicker('hide');   
	}).data('datepicker');

    function getAvailability (timeEntered, dateEntered, date, time, displaySoldOutSection, isCruise, finishedImageTag, timeDropdown) {
        //If the user has entered in a time already, then we need to check for availability on date and time
            if (timeEntered) {                 

                //check to see if the date is available first                
                dateEntered = checkDateAvailability(date, isCruise);

                //If date is available then check the time
                if (dateEntered)
                {
                    dateEntered = checkDateAndTimeAvailability(date, time, isCruise); 
                    showBookingSuccessful(displaySoldOutSection, finishedImageTag, 999);

                    //both date and time are good
                    if (dateEntered)
                    {
                        showBookingSuccessful(displaySoldOutSection, finishedImageTag, 999);
                    }
                    else if (!dateEntered) //If the date is good, but not the time
                    {
                        priceSection.empty();
                        priceTaglineSection.empty();

                        changeStatusImage(finishedImageTag + 1, 'Not Finished');
                    }
                }
                else {
                    priceSection.empty();
                    priceTaglineSection.empty();

                    changeStatusImage(finishedImageTag, 'Not Finished');
                }
            }
            else {
                //check to see if this date has openings.
                dateEntered = checkDateAvailability(date, isCruise);

                if (dateEntered)
                {
                    showBookingSuccessful(displaySoldOutSection, finishedImageTag, finishedImageTag + 1);
                     //Enable the ability to get the time
                    timeDropdown.prop('disabled', false);
                }
                else {
                    priceSection.empty();
                    priceTaglineSection.empty();

                    changeStatusImage(finishedImageTag, 'Not Finished');  
                     //Enable the ability to get the time
                    timeDropdown.prop('disabled', true);                  
                }
            }

            if (!isCruise)
            {
                flightTimeEntered = timeEntered;
                flightDateEntered = dateEntered;       
            }
            else if (isCruise)
            {
                cruiseTimeEntered = timeEntered;
                cruiseDateEntered = dateEntered;
            }
    }

    //Get the return date information from the date picker that is displayed if the date is not sold out.
	var returnDate = $('#dp4').datepicker(
        { 
            format : 'MM/dd/yyyy',
            onRender: function (date) {
                return date.valueOf() < earliestDate.valueOf() ? 'disabled' : '';
            }

        }).on('changeDate', function(ev){

        //We don't want to show that the airport has been sold out once the date changes unless it actually is.
        cruiseSoldOut.hide();
        //Check the current selected date to see if it is a valid date.
   		$('#dp4 input').val($.format.date(ev.date, "MM/dd/yyyy"));

        cruiseDate = $('#dp4 input').val();

        var imageTagToChange;
        var secondImageTagToChange;

        getAvailability (cruiseTimeEntered, cruiseDateEntered, cruiseDate, cruiseTime, cruiseSoldOut, true, 6, $('#dropdown-button-cruise'));
        
        $(this).datepicker('hide');  
        checkIfFinished();      

    }).data('datepicker');

        //Get the return date information from the date picker that is displayed if the date is not sold out.
    var hotelCheckInDate = $('#dp5').datepicker(
        { 
            format : 'MM/dd/yyyy',
            onRender: function (date) {
                return date.valueOf() < earliestDate.valueOf() ? 'disabled' : '';
            }

        }).on('changeDate', function(ev){

        //We don't want to show that the airport has been sold out once the date changes unless it actually is.
        cruiseSoldOut.hide();
        //Check the current selected date to see if it is a valid date.
        $('#dp5 input').val($.format.date(ev.date, "MM/dd/yyyy"));

        cruiseDate = $('#dp5 input').val();

        var imageTagToChange;
        var secondImageTagToChange;
        
        $(this).datepicker('hide');  
        checkIfFinished();      

    }).data('datepicker');

    //When the flight number text box is changed we want to display that something has been inputed 
    //and update the UI accordingly.  But if this text box is changed to empty then we want to make sure
    //that we update the UI accordingly and make sure the user cannot advance.
    $('#flight-number').change(function  () {
        $(this).val($(this).val().toUpperCase());    
        handleFlightNumberInput($(this));    
    });

    function handleFlightNumberInput (flightNumberInput) {
        flightNumberEntered = true;
        //Enable the button.
        smartButton.prop('disabled', false);

        $(".finished[tag=9]").attr('src', "/smart-images/check-mark-green.png");
        $(".finished[tag=9]").attr('height', 15);
        $(".finished[tag=9]").attr('width', 15);

        //IF they change the value back to empty then we need to make note of that
        if ($(flightNumberInput).val().length == 0) {
            $(".finished[tag=9]").attr('src', "/smart-images/check-required.png");
            $(".finished[tag=9]").attr('height', 10);
            $(".finished[tag=9]").attr('width', 10);            

            flightNumberEntered = false;            
            //Remove the ability to press the button
            smartButton.prop('disabled', true);        
        }
        else {
            moveToNextBox(9, 5);
        } 
        
        $('#flight-number').blur();               
    }

    $('#flight-number').focus(function() {
      var input = $(this);
      if (input.val() == input.attr('placeholder')) {
        input.val('');
        input.removeClass('placeholder');
      }
    }).blur(function() {
    var input = $(this);
      if (input.val() == '' || input.val() == input.attr('placeholder')) {
        input.addClass('placeholder');
        input.val(input.attr('placeholder'));
      }
    }).blur();  

    $('#flight-number').keypress(function (e) {
        if(e.keyCode === 13) {
            $(this).val($(this).val().toUpperCase());
            handleFlightNumberInput($(this));
        }
    });


    $('#cruise-date').keydown(function () {
        //don't allow the user to enter in any text with the keyboard
        return false;
    })

    function checkBox (box) {
       $(box).attr('src', "/smart-images/check-mark-green.png"); 
    }

    //We check to see here if all the required fields have been entered
    function checkIfFinished () {
        //Make sure that the number of travelers has been put in then check for the rest
        if (numOfTravelersSelected)
        {
            if (roundTrip == true)
            {
                //If all fields are filled out
                if (flightDateEntered == true && flightTimeEntered == true && 
                    cruiseDateEntered == true && cruiseTimeEntered == true &&
                    cruiselineSelected == true && airlineSelected == true)
                {
                    displayPrice();
                    validated = true;
                }
            }
            else if (oneWayToCruise == true)
            {
                //If the flight information is filled out
                if (flightDateEntered == true && flightTimeEntered == true && airlineSelected == true)
                {
                    displayPrice();
                    validated = true;
                }
            }
            else if (oneWayToAirport == true)
            {
                //If the cruise information is filled out
                if (cruiseDateEntered == true && cruiseTimeEntered == true && cruiselineSelected == true)
                {
                    displayPrice();
                    validated = true;
                }
            }
            if (validated)
            {
                smartButton.show();
            }
            //If we have validated that this worked, then we execute the following code
            if (validated && roundTrip == true)
            {            
                $("html, body").animate({ scrollTop: 200 }, 'slow');            
            }
        }   
    }
});