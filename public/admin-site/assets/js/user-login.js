$(document).ready(function () {

	var signUpLink = $(".sign-up");
	var signUpButton = $("#sign-up-button");
	var signInButton = $("#sign-in-button");
	var resetPassButton = $("#reset-password-button");
	
	var INPUT_USERNAME = "username";
        var INPUT_PASSWORD = "password";
   	var INPUT_EMAIL = "email";

	var canSignup = false;
	var uCount = 0;
	
	var feedbackForm = $('.login-form .alert');
	var feedbackFormContent = $('.login-form .alert .content');
	
        //Update the initialization call with my Application ID and Javascript Key
	(new Parse.Query(Parse.User)).count({
		success : function(results) {
			uCount = results;
			
			if (uCount == 0) {
				canSignup = true;
				signUpLink.show();
			}
		}
	});
	
	signUpButton.click(function (e) {
		e.preventDefault();
		if (!canSignup)
			return;
		
		var user = new Parse.User();

		// get all the inputs into an array.
	    var $inputs = $('#signup-form :input');

	    //traverse through all the inputs and store them by name in the values array
	    var values = {};
	    $inputs.each(function() {
	        values[this.name] = $(this).val();
	    });

	    var username = values[INPUT_USERNAME];
	    var password = values[INPUT_PASSWORD];
	    var email = values[INPUT_EMAIL];

	 	user.set("username", username);
	 	user.set("password", password);
	 	user.set("email", email);

	 	user.signUp(null, {
	 		success: function user () {
	 			window.location.replace('index.html');
	 		},
	 		error: function  () {
	 			//Show error message and let the user try again.
	 		}
	 	})
	});
	//Get the username and the password from the input boxes and then sign in using parse
	signInButton.click(function  (e) {
		e.preventDefault();
		
		feedbackForm.hide();
		
		var username = $('input[name=' + INPUT_USERNAME + ']').val();
		var password = $('input[name=' + INPUT_PASSWORD + ']').val();

		Parse.User.logIn(username, password, {
                    success: function (user) {
                        $.cookie('_st_userToken', user._sessionToken, { expires: 30, path: '/' });
                        window.location.replace('/admin-site');
                    },
                    error: function (user, error) {
                        var errorText = '';
                        switch (error.code) {
                            case 101:
                                    errorText = 'Invalid login credentials!';
                                    break;

                            default:
                                    errorText = error.message;
                        }

                        feedbackFormContent.text(errorText);
                        feedbackForm.show();
                    }
		});
	});
	
	resetPassButton.click(function  (e) {
		var email = $('.forgot-password-form input[name="email"]').val();
		
		Parse.User.requestPasswordReset(email, {
		  success: function() {
			  console.log('sucessful reset');
			// Password reset request was sent successfully
		  },
		  error: function(error) {
			// Show the error message somewhere
			console.log("Error: " + error.code + " " + error.message);
		  }
		});
	});
});