var app = angular.module('sellout', []);

app.directive('selloutCalendar', function(){
    return {
        template : '<div id="selloutCalendar"></div>',
        restirct : 'E',
        link : function(scope, elem, attrs, ctrl) {
            var calendar = elem.find('#selloutCalendar');
            
             calendar.fullCalendar({
                timezone : 'America/New_York',
                events: {
                    url : 'sellout/calendar'
                },
                dayClick: function(date, allDay, jsEvent, view)
                {
                    scope.selectedDate = date;
                    scope.$apply();
                    
                    scope.getOnDay(date.format('YYYY-M-D'));
                } 

            });
            
            scope.refreshCalendar = function(){
                calendar.fullCalendar('refetchEvents');
            };
        }
    };
});

app.directive('selloutForm', function(){
    return {
        restrict : 'A',
        
        link : function(scope, elem, attrs, ctrl) {
            scope.enable = function() {
                elem.formEnable();
            };
            
            scope.disable = function() {
                elem.formDisable();
            };
        }
    };
});
   
app.controller('selloutCtrl', ['$scope', '$http', 'times', 'sellout', function($scope, $http, times, sellout) {
    $scope.dateEntries = {};
    
    $scope.selectedId = null;
    $scope.selectedDate = null;
    $scope.selectedSeats = null;
    $scope.selectedStartTime = '';
    $scope.selectedEndTime = '';
    
    $scope.times = [];
    
    $scope.dateFormatted = function() {
        if (!$scope.selectedDate) return;
        
        return $scope.selectedDate.format('M/D/YYYY');
    };
    
    $scope.setTime = function(time, direction) {
        if (direction == 'end')
            $scope.selectedEndTime = time;
        else
            $scope.selectedStartTime = time;
    };
    
    $scope.setSeats = function(seats) {
        $scope.selectedSeats = seats;
    };
    
    $scope.isDateSelected = function() {
        return $scope.selectedDate != null;
    };
    $scope.hasEntries = function() {
        return Object.keys($scope.dateEntries).length > 0;
    };
    
    $scope.resetEntry = function() {
        $scope.selectedStartTime = '';
        $scope.selectedEndTime = '';
        $scope.selectedSeats = null;
        $scope.selectedId = null;
    };
    
    $scope.save = function() { 
        var obj = sellout.toObj(
                $scope.selectedDate,
                $scope.selectedStartTime,
                $scope.selectedEndTime,
                $scope.selectedSeats,
                $scope.selectedId
            );
    
        $scope.disable();
        
        sellout.save(obj).then(function(data){
            $scope.getOnDay($scope.selectedDate.format('YYYY-M-D'))
             .finally(function(){
                $scope.enable();
             });
            $scope.refreshCalendar();
            
            
        });
    };
    
    $scope.del = function(id) {
        $scope.disable();
        
        sellout.del(id).then(function(data){
            $scope.getOnDay($scope.selectedDate.format('YYYY-M-D'))
            .finally(function(){
                $scope.enable();
             });
            $scope.refreshCalendar();
            
            $scope.enable();
        });
    };
    
    $scope.edit = function(id) {
        load($scope.dateEntries[id]);
    };
    
    $scope.getOnDay = function(date) {
        return sellout.getOnDay(date).then(function(data){
            $scope.dateEntries = data;
        });
    };
    
    times.getData().then(function(data){
        $scope.times = data.data;
    });
    
    var load = function(obj) {
        var data = sellout.toStr(obj);
        
        $scope.selectedId = data.id;
        $scope.selectedDate = data.date;
        $scope.selectedSeats = data.seats;
        $scope.selectedStartTime = data.start;
        $scope.selectedEndTime = data.end;
    };
}]);


app.service('times', function($http) {
    var promise = $http({
        method : 'GET',
        url : 'workshop/times'
    });
    
    return {
        getData: function() {
            return promise;
        }
    };
});

app.service('sellout', function($http) {
    var stringsToObj = function(date, start, end, seats, id) {
        var _start = moment.utc(date.format('YYYY-M-D')+' '+start, 'YYYY-M-D h:m A').subtract(sBook.TZO, 'm');
        var _end = end ?
            moment.utc(date.format('YYYY-M-D')+' '+end, 'YYYY-M-D h:m A').subtract(sBook.TZO, 'm') :
            null;
    
        return {
            _id : id,
            start: _start.toDate(),
            end: _end ? _end.toDate() : null,
            seats : seats
        };
    };
    
    var objToForm = function(obj) {
        var _start = moment(obj.start).utcOffset(sBook.TZO);
        var _end = obj.end ? moment(obj.end).utcOffset(sBook.TZO) : null;
        
        return {
            id : obj._id,
            date : _start,
            start : _start.format('h:mm A'),
            end : _end ? _end.format('h:mm A') : null,
            seats : obj.seats
        };
    };
    
    var save = function(obj) {
        return $http({
            method : 'POST',
            url : 'sellout/add',
            data : { entry : obj }
        });
    };
    
    var del = function(id) {
        return $http({
            method : 'GET',
            url : 'sellout/delete/'+id
        });
    };
    
    var getOnDay = function(day) {
        return $http({
            method : 'GET',
            url : 'sellout/byDates',
            params : { start : day }
        })
        .then(function(data){
            var entries = {};
            
            for (var i=0; i<data.data.length; i++) {
                var entry = data.data[i];
                entry.start = moment(entry.start).utcOffset(sBook.TZO);
                entry.end = entry.end ? moment(entry.end).utcOffset(sBook.TZO) : null;
                
                entries[entry._id] = entry;
            }
            
            return entries;
        });
    };
    
    return {
        toObj : stringsToObj,
        toStr : objToForm,
        
        save : save,
        del : del,
        
        getOnDay : getOnDay
    };
});

app.filter('empty_text', function(){
    return function(str, defaultText) {
        return str ? str : defaultText;
    };
});