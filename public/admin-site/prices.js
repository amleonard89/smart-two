$(document).ready(function () { //Begin ready function
	//getReservations();
	var dateString;
	var reservationTable;
	var date = new Date();

	$('#calendar').fullCalendar('addEvent', {
		id			: 1,
		title		: "PRICE: 55",
		start		: new Date(2014, 1, 27)
	});

	function getReservations (date) { //begin function getData
	
		var response = $.ajax({
			url: "/mongo/get", 
			type: "GET", 
			dataType : "json",
			data : date,
			success: function onSuccess (response) {  //Begin Success Function
				console.log("Successfully made ajax call");
				
			}, //End Success Function
			error: 
	    		function onError () { //Begin Error Function
	    			alert('failed to get data');
	    		}	//End Error Function
		});
	};  //end function getData
	$('#calendar').fullCalendar({ //Begin Full Calendar Method
        // put your options and callbacks here
        dayClick: function(date, allDay, jsEvent, view)
        {	//Begin Day Click Method
        	dateString = formatDate(date.toDate());

        	$("#enterPrice").empty();
        	$("#enterPrice").append('<h3>Enter the new prices for the date: </h3><h5>' + dateString + '</h5>');
        	$("#enterPrice").append('<br/>' +
        		'<div class = "span6" >' +
        			'<div style = "font-size : 18px; padding-top : 10px;">One Way Price :</div>' +
        			'<input id="price-input" style =" font-size: 18px;" type="text"></input>' + 
        			'<div style = "font-size : 18px; padding-top : 10px;">Round Trip :</div>' +
        			'<input id="price-input" style =" font-size: 18px;" type="text"></input>' + 
        			'<div style = "font-size : 18px; padding-top : 10px;">Three Way Price :</div>' +
        			'<input id="price-input" style =" font-size: 18px;" type="text"></input>' +         			
        		'</div>' +
        		'<div>' +
        			'<br/>' +
        			'<input type = "submit" id="changePrice" value = "Change Price"></input>' +        			
        		'</div>');

        } //End Day Click method
    })  //End Full Calendar

	$("#changePrice").live("click", function () {

		var price = $('#price-input').val();

		var response = $.ajax({
			url: "/mongo/post/updateprice", 
			type: "POST", 
			dataType : "json",
			data : JSON.stringify(
			{
				date :
				{
					price : price,
					date : dateString
				}
			}),
			contentType : "application/json",
			success: function onSuccess (response) {  //Begin Success Function
				console.log("Successfully made ajax call");
				
			}, //End Success Function
			error: 
	    		function onError () { //Begin Error Function
	    			alert('failed to get data');
	    		}	//End Error Function
		});
	})
}); //end ready function


