/**
 * Created by adeiji on 12/11/14.
 * Updated on 8/24/15
 */
function getPricesAndNumberOfTravelers(numOfTravelers, roundTrip, roundTripAllWays, oneWayHotel)
{
    var price = 0;
    var originalPrice = 0;
    var strNumOfTravelers = '';
    switch (numOfTravelers) {
        case 2:
            if (!roundTrip && !roundTripAllWays && !oneWayHotel)
            {
                price = 69;
                originalPrice = 80;
            }
            else if (roundTripAllWays)
            {
                price = 139;
                originalPrice = 180;
            }
            else if (oneWayHotel)
            {
                price = 79;
                originalPrice = 90;
            }
            else {
                price = 129;
                originalPrice = 150;
            }

            strNumOfTravelers = "TWO";
            break;
        case 3:
            if (!roundTrip && !roundTripAllWays && !oneWayHotel)
            {
                price = 89;
                originalPrice = 105;
            }
            else if (roundTripAllWays)
            {
                price = 179;
                originalPrice = 210;
            }
            else if (oneWayHotel)
            {
                price = 89;
                originalPrice = 105;
            }
            else {
                price = 179;
                originalPrice = 210;
            }

            strNumOfTravelers = "THREE";
            break;
        case 4:
            if (!roundTrip && !roundTripAllWays && !oneWayHotel)
            {
                price = 99;
                originalPrice = 120;
            }
            else if (roundTripAllWays)
            {
                price = 199;
                originalPrice = 240;
            }
            else if (oneWayHotel)
            {
                price = 99;
                originalPrice = 120;
            }
            else {
                price = 199;
                originalPrice = 240;
            }

            strNumOfTravelers = "FOUR";
            break;
        case 5:
            if (!roundTrip && !roundTripAllWays && !oneWayHotel)
            {
                price = 119;
                originalPrice = 150;
            }
            else if (roundTripAllWays)
            {
                price = 229;
                originalPrice = 280;
            }
            else if (oneWayHotel)
            {
                price = 119;
                originalPrice = 150;
            }
            else {
                price = 229;
                originalPrice = 280;
            }

            strNumOfTravelers = "FIVE";
            break;
        case 6:
            if (!roundTrip && !roundTripAllWays && !oneWayHotel)
            {
                price = 139;
                originalPrice = 180;
            }
            else if (roundTripAllWays)
            {
                price = 269;
                originalPrice = 320;
            }
            else if (oneWayHotel)
            {
                price = 139;
                originalPrice = 180;
            }
            else {
                price = 269;
                originalPrice = 320;
            }

            strNumOfTravelers = "SIX";
            break;
        case 7:
            if (!roundTrip && !roundTripAllWays && !oneWayHotel)
            {
                price = 159;
                originalPrice = 210;
            }
            else if (roundTripAllWays)
            {
                price = 299;
                originalPrice = 360;
            }
            else if (oneWayHotel)
            {
                price = 159;
                originalPrice = 210;
            }
            else {
                price = 299;
                originalPrice = 360;
            }

            strNumOfTravelers = "SEVEN";
            break;
        case 8:
            if (!roundTrip && !roundTripAllWays && !oneWayHotel)
            {
                price = 169;
                originalPrice = 240;
            }
            else if (roundTripAllWays)
            {
                price = 329;
                originalPrice = 400;
            }
            else if (oneWayHotel)
            {
                price = 169;
                originalPrice = 240;
            }
            else {
                price = 329;
                originalPrice = 400;
            }

            strNumOfTravelers = "EIGHT";
            break;
    }

    originalPrice = (originalPrice).toFixed(2);

    return {
        price               : price,
        originalPrice       : originalPrice,
        numOfTravelers      : strNumOfTravelers
    };
}