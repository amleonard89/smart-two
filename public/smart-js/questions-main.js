/**
 * Created by adeiji on 1/4/15.
 */
$(document).ready( function  ()
{

    isMobilePhone();

    function isMobilePhone () {
        var phone = $('.phone');
        var headerMessage = $('.headerMessage');
        var videoiFrame = $('.video').find('iframe');
        var sideNav = $('.sideNav');

        if ($('#isPhone').css('display') == 'block')
        {
            $('.row').css('width', '106%');
            headerMessage.css('margin-top', '0px');
            headerMessage.css('font-size', '16px');
            $('.phone-questions').css('cssText', 'margin-top: -29px !important')
            videoiFrame.css('margin-left', '14px');
            sideNav.css('margin-right: 35px;');

            phone.toggleClass('phone', false);
            phone.toggleClass('headerPhone', true);
        }
    }

});