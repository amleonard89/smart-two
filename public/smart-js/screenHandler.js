/**
 * Created by adeiji on 12/17/14.
 */
function resetScreenView (cruiseTimeDropdown, smartButton, ) {
    //Remove all the Cruise Info from Cruise Ship to Airport
    $('#airport-date[placeholder]').val("MM/DD/YYYY");

    $('#dropdown-button-flight').empty();
    $('#dropdown-button-flight').append('Select Pick Up Time<span class="caret"></span>');

    $('#dropdown-button-cruiseline').empty();
    $('#dropdown-button-cruiseline').append('Select Your Cruise Ship<span class="caret"></span>');

    $('#price-tagline').empty();
    $('#price').empty();

    //Remove all the Flight info from Airport to Cruise Ship
    $('#cruise-date[placeholder]').val("MM/DD/YYYY");

    cruiseTimeDropdown.empty();
    cruiseTimeDropdown.append('Select Pick Up Time<span class="caret"></span>');

    $('#dropdown-button-airline').empty();
    $('#dropdown-button-airline').append('Select Your Airline<span class="caret"></span>');

    $('#flight-number').val('');
    smartButton.hide();
    boxes = [];
    for (var i = 1; i < 10; i++) {
        boxes[i] = "Not Selected"
    };

    changeBoxes('check-not-finished');

    $('#dropdown-button-flight').prop('disabled', true);
    cruiseTimeDropdown.prop('disabled', true);

    return boxes;
}