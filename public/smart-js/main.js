var myTravelTypeId;
var myNumOfTravelers;

$(document).ready( function  () {
	//document.addEventListener("touchstart", function() {},false);
	$('.dropdown-menu a').each(function(i){
		this.addEventListener("touchstart", function() {},false);
	});
	
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-50393215-1', 'smart-two.com');
    ga('send', 'pageview');

    setErrorMessages();
    editMainScreen();

    isMobilePhone();

    // If this is a mobile phone than change the order of what they see from top to bottom
    function isMobilePhone() {
        if ($('#isPhone').css('display') == 'block')
        {
            var contentParagraphContainer = $('.content-paragraph-container');
            var sliderWrapper = $('#slides-wrapper');
            var header = $('.navigation-bar');
            var video = $('.video');
            var mainAreaWrapper = $('.row.full-body');
            var phone = $('.main-phone');

            contentParagraphContainer.remove();
            sliderWrapper.remove();
            header.remove();
            video.remove();
            phone.remove();

            header.prepend(video)
            mainAreaWrapper.prepend(header);
            mainAreaWrapper.prepend(contentParagraphContainer);
            mainAreaWrapper.prepend(sliderWrapper);
            sliderWrapper.append(phone);

            header.css('');
            header.find('button').css('margin-top', '20px');
        }
    }

    $('#pdf-overlay').hide();

    $('.dropdown-toggle').dropdown();

    //$('#dropdown-button-flight').prop('disabled', true); 
    //cruiseTimeDropdown.prop('disabled', true);


    function editMainScreen () {
        flightSection.show();
        hotelSection.hide();
        cruiseSection.hide();
        smartButton.hide();
        airportSoldOut.hide();
        cruiseSoldOut.hide();
    }
    //Add the error messages to the variables 
    function setErrorMessages () {

        // cruiseSoldOut.append('Sorry this date is booked completely.  Please call 702-809-1436 for availability');
        // airportSoldOut.append('Sorry this date is booked completely.  Please call 702-809-1436 for availability');

        cruiseTimeSoldOut.hide();
        airportTimeSoldOut.hide();

        // cruiseTimeSoldOut.append('Sorry this time is booked completely.  Please call 702-809-1436 for availability');
        // airportTimeSoldOut.append('Sorry this time is booked completely.  Please call 702-809-1436 for availability');
    }

    var reservation;

    if ($.cookie('reservation'))
    {
        reservation = JSON.parse($.cookie('reservation'));
        changeTravelType(options[reservation.travelType]);
        reloadReservation(reservation);
        flightDate = $('#dp3 input').val();
        cruiseDate = $('#dp4 input').val();
    }



    $('#pdf-overlay').click(function () {
        $('#pdf-overlay').hide();
    })

    $('#fast-rates').click(function () {
        $('#pdf-overlay').show();  
    })



    function resetScreen () {
        //Remove all the Cruise Info from Cruise Ship to Airport
        $('#airport-date[placeholder]').val("MM/DD/YYYY");

        $('#dropdown-button-flight').empty();
        $('#dropdown-button-flight').append('Select Pick Up Time<span class="caret"></span>');

        $('#dropdown-button-cruiseline').empty();
        $('#dropdown-button-cruiseline').append('Select Your Cruise Ship<span class="caret"></span>');
        $('#dropdown-button-cruiseline-in-airport').empty();
        $('#dropdown-button-cruiseline-in-airport').append('Select Your Cruise Ship<span class="caret"></span>');

        $('#price-tagline').empty();
        $('#price').empty();

        //Remove all the Flight info from Airport to Cruise Ship
        $('#cruise-date[placeholder]').val("MM/DD/YYYY");

        cruiseTimeDropdown.empty();
        cruiseTimeDropdown.append('Select Pick Up Time<span class="caret"></span>');

        $('#dropdown-button-airline').empty();
        $('#dropdown-button-airline').append('Select Your Airline<span class="caret"></span>');
        $('#dropdown-button-airline-in-cruise').empty();
        $('#dropdown-button-airline-in-cruise').append('Select Your Airline<span class="caret"></span>');
        $('#flight-number').val('').prop('disabled', false);
        smartButton.hide();
        boxes = [];
        for (var i = 1; i < 12; i++) {
            boxes[i] = "Not Selected"
        };

        changeBoxes('check-not-finished');

        // $('#dropdown-button-flight').prop('disabled', true);
        //cruiseTimeDropdown.prop('disabled', true);
    }

    function resetReservationStatusVariables () {
        roundTrip = false;
        oneWayFromAirport = false;
        oneWayToAirport = false;
        roundTripAllWays = false;
        cruiseDateEntered = false;
        cruiseTimeEntered = false;
        flightTimeEntered = false;
        flightDateEntered = false;
        airlineSelected = false;
        oneWayToHotel = false;
        cruiselineSelected = false;   
        flightNumberEntered = false; 
        validated = false;
        price;
        setEarliestDate();
    }

    function reset () {
        resetScreen();
        resetReservationStatusVariables();
    }

    function changeTravelType (newTravelType) {
        //Set the current travel type to the new one in order to have stored for later usage when saving the cookie.
        travelType = newTravelType;

        airlineDropdownOuterClone.hide();
        cruiselineDropdownOuterClone.hide();
        
        cruiseDateEntered = false;
        cruiseTimeEntered = false;
        flightTimeEntered = false;
        flightDateEntered = false;
        flightTime = null;
        cruiseTime = null;
        
        //If this is a one way to airport
        if (newTravelType == options[0])
        {         
            changeTravelTypeToCruiseShipToAirport();
        }
        //Round trip
        else if (newTravelType == options[2])
        {            
            changeTravelTypeToRoundTripCruiseAndAirport();
        }
        //One way to cruise
        else if (newTravelType == options[1])
        {            
            changeTravelTypeToAirportToCruiseShip();
        }
        // Orlando Airport to Hotel
        else if (newTravelType == options[3])
        {
            changeTravelTypeToAirportToHotel();
        }
        else if (newTravelType == options[4])
        {
            changeTravelTypeToRoundTripAirportAndHotelCruiseShipAndAirport();            
        }

        setTravelTypeId(travelTypeId);
        cruiseTimeSoldOut.hide();
        airportTimeSoldOut.hide();
    }

    function changeTravelTypeToRoundTripAirportAndHotelCruiseShipAndAirport () {
        $('#dropdown-button-type').text( "Airport to Hotel | Cruise to Airport" );

        flightSection.show();
        cruiseSection.show();
        hotelSection.show();

        oneWayToAirport = false;
        roundTrip = false;
        oneWayToHotel = false;
        roundTripAllWays = true;
        travelTypeId = 4;
        // moveToNextBox(1,2);
        smartButton.prop('disabled', true);
        moveToNextBox(1, 2);
        changeTravelTimesForHotel();
    }

    function changeTravelTypeToAirportToHotel () {
        $('#dropdown-button-type').text( "Airport to Hotel" );

        flightSection.show();
        cruiseSection.hide();
        hotelSection.show();

        roundTrip = false;
        oneWayToHotel = true;
        travelTypeId = 3;
        // moveToNextBox(1,2);
        smartButton.prop('disabled', true);
        moveToNextBox(1, 2);

        // Change the travel times
        changeTravelTimesForHotel();
    }

    function changeTravelTimesForNotHotel ()
    {
        $('#flight-time').empty();
        $('#flight-time').append(
            '<li role="presentation"><a role="menuitem" tabindex="-1" id="time">9:20  AM</a></li>' +
            '<li role="presentation"><a role="menuitem" tabindex="-1" id="time">11:55 AM</a></li>'
        );
		
		$('#flight-time a').each(function(i){
			this.addEventListener("touchstart", function() {},false);
		});
    }

    function changeTravelTimesForHotel ()
    {
        $('#flight-time').empty();
        $('#flight-time').append(
            '<li role="presentation"><a role="menuitem" tabindex="-1" id="time">11:55 AM</a></li>' +
            '<li role="presentation"><a role="menuitem" tabindex="-1" id="time">2:15 PM</a></li>' +
            '<li role="presentation"><a role="menuitem" tabindex="-1" id="time">3:15 PM</a></li>' +
            '<li role="presentation"><a role="menuitem" tabindex="-1" id="time">4:15 PM</a></li>' +
            '<li role="presentation"><a role="menuitem" tabindex="-1" id="time">5:15 PM</a></li>' +
            '<li role="presentation"><a role="menuitem" tabindex="-1" id="time">6:15 PM</a></li>' +
            '<li role="presentation"><a role="menuitem" tabindex="-1" id="time">7:15 PM</a></li>' +
            '<li role="presentation"><a role="menuitem" tabindex="-1" id="time">8:15 PM</a></li>'
        );
		
		$('#flight-time a').each(function(i){
			this.addEventListener("touchstart", function() {},false);
		});
    }


    function changeTravelTypeToAirportToCruiseShip () {
        $('#dropdown-button-type').text( "Airport to Cruise Ship" );

        flightSection.show();
        cruiseSection.hide();
        hotelSection.hide();

        oneWayToAirport = false;
        roundTrip = false;
        oneWayFromAirport = true;
        travelTypeId = 1;
        moveToNextBox(1,2);
        smartButton.prop('disabled', true);
        cruiselineDropdownOuterClone.show();
        changeTravelTimesForNotHotel();
    }

    function changeTravelTypeToRoundTripCruiseAndAirport () {
        $('#dropdown-button-type').text( "Roundtrip - Airport - Cruise" );

        cruiseSection.show();
        flightSection.show();
        hotelSection.hide();

        oneWayToAirport = false;
        roundTrip = true;
        oneWayFromAirport = false;
        travelTypeId = 2;

        moveToNextBox(1, 2);
        smartButton.prop('disabled', true);
        changeTravelTimesForNotHotel();
    }

    function changeTravelTypeToCruiseShipToAirport () {
        $('#dropdown-button-type').text( "Cruise Ship to Airport" );

        flightSection.hide();
        cruiseSection.show();
        hotelSection.hide();

        oneWayToAirport = true;
        roundTrip = false;
        oneWayFromAirport = false;

        smartButton.prop('disabled', false);
        travelTypeId = 0;

        // checkBox("#cruise-date-finished");
        moveToNextBox(1, 6);
        airlineDropdownOuterClone.show();
        changeTravelTimesForNotHotel();
    }
    
    $('#traveler-amount').on('click', 'li > a', function (e) {
        $('#traveler-amount-box').text( this.innerHTML );        

        numOfTravelers = parseInt(this.innerHTML.substring(0, 2).trim());
        setNumberOfTravelers(numOfTravelers);
        moveToNextBox(0, 1);
        numOfTravelersSelected = true;

        try {
            getTimeAvailability (flightTime, flightDateEntered, flightTimeEntered, flightDate, flightTime, airportTimeSoldOut, 3, false);
        }
        catch (err) {
            // If a failure just simply don't do anything
        }

        try {
            getTimeAvailability (cruiseTime, cruiseDateEntered, cruiseTimeEntered, cruiseDate, cruiseTime, cruiseTimeSoldOut, 7, true);
        }
        catch (err) {
            // If a failure just simply don't do anything
        }
        
        checkIfFinished();

    });
	
    function stringWithStrippedTags (html) {
        var strippedString = html.replace(/(<([^>]+)>)/ig,"");        
        return strippedString;
    }

    $('#travel-type').on('click', 'li > a', function(e){
        reset();
    	//Change the travel type variables and move onto the next circles showing whether or not something has been finished
        changeTravelType(stringWithStrippedTags(this.innerHTML));

        //Reset all the inputs and the dropdown boxes to their default settings.        
    	$('#dropdown-button-type').append( '<span class="caret"></span>' );

	});

    //Click on cruise time, and display the selected item
    $('#flight-time').on('click', '#time', function(e){
        $('#dropdown-button-flight').text( this.innerHTML + " " );
        $('#dropdown-button-flight').append( '<span class="caret"></span>' );
        flightTime = this.innerHTML.replace(/\s/ig, '');
        getTimeAvailability (flightTime, flightDateEntered, flightTimeEntered, flightDate, flightTime, airportTimeSoldOut, 3, false);
        checkIfFinished();
    });

    //Click on cruise time, and display the selected item
    $('#cruise-time').on('click', 'li > a', function(e){
        cruiseTimeDropdown.text( this.innerHTML + " " );
        cruiseTimeDropdown.append( '<span class="caret"></span>' );
        cruiseTime = this.innerHTML;
        getTimeAvailability (cruiseTime, cruiseDateEntered, cruiseTimeEntered, cruiseDate, cruiseTime, cruiseTimeSoldOut, 7, true);
        checkIfFinished();
    });


    function showBookingSuccessful (section, firstTag, secondTag) {
        section.empty();
        checkIfFinished();
        moveToNextBox(firstTag, secondTag);
    }

    function showTimeBooked (section, tag) {
        validated = false;
        section.show();
        changeStatusImage(tag, 'Not Finished');
    }

    function changeStatusImage (tag, status) {

        if (status == 'Not Finished') 
        {
            $(".finished[tag=" + tag + "]").attr('src', "/smart-images/check-required.png");
            $(".finished[tag=" + tag + "]").attr('height', 10);
            $(".finished[tag=" + tag + "]").attr('width', 10); 
        }
    }

    function checkDateAndTimeAvailability (date, time, cruise) {

        //If the date and time is available then we return true
        var available;

        var currentTravelTypeId;

        return checkDateAndTime(date, time, numOfTravelers)
         .then(function (response) {
            var valid = response.valid;
            var message = response.message;
            
            //If the date and time is booked completely
            if (!valid)
            {
                if (!cruise)
                {
                    airportTimeSoldOut.empty();
                    airportTimeSoldOut.show();                    
                    airportTimeSoldOut.append(message);
                }
                else
                {
                    cruiseTimeSoldOut.empty();
                    cruiseTimeSoldOut.show();   
                    cruiseTimeSoldOut.append(message);
                }
                available = false;            
            }
            else {
                available = true;
            }
            
            return available;
        });

        //return available;
    }

    function airlineDropdownClicked (dropdown, dropdownButton) {
		var text = '';
        if ($(dropdown).text().indexOf('Orlando') != -1)
        {
            text = $(dropdown).text().substring(0, dropdown.innerHTML.indexOf('Orlando'));
        }
        else {
            text = $(dropdown).text();   
        }
		
		if (text.indexOf('NO FLIGHT')!=-1)
		{
			$('#flight-number').val('0000').prop('disabled', 'disabled').change();
		}
		else
		{
			$('#flight-number').prop('disabled', false);
			if ($('#flight-number').val() == '0000')
				$('#flight-number').val('').change();
			
			$('#flight-number').focus();
		}
		dropdownButton.text( text );
        dropdownButton.append( ' <span class="caret"></span>' );
        airlineSelected = true;
        checkIfFinished();

        //If they're going one way to the cruise ship then skip the flight number circle
        if (oneWayToHotel)
        {
            moveToNextBox(4, 9);
        }
        else if (roundTrip || roundTripAllWays)
        {
            moveToNextBox(4, 6);
        }
        else if (oneWayFromAirport) {
            moveToNextBox(4, 11);
        }
        else {
            moveToNextBox(10, 999);
        }
    }
	$(document).on('show.bs.dropdown', '.seedeals-reservationBar .input-section', function(e){
		var ulList = $(e.target).find('ul');
		var buttonWidth = $(e.relatedTarget).outerWidth();
		var dropdownWidth = ulList.width();

		ulList
			.css('position', 'relative')
			.css('margin-left', '0px')
			.css('left', buttonWidth-dropdownWidth);
	});
	
    $('#airline-dropdown').on('click', 'li > a', function(e){
        var dropdownButton = $('#dropdown-button-airline');
        airlineDropdownClicked(this, dropdownButton);
    });

    $('#cruiseline-in-airport-section').on('click', 'li > a', function (e) {
        var dropdownButton;
        dropdownButton = cruiselineDropdownOuterClone.children().find('#dropdown-button-cruiseline-in-airport');
        cruiselineDropdownClicked(this, dropdownButton);
    });

    $('#airline-in-cruise-section').on('click', 'li > a', function (e) {
        var dropdownButton;
        dropdownButton = airlineDropdownOuterClone.children().find('#dropdown-button-airline-in-cruise');
        airlineDropdownClicked(this, dropdownButton);
    });

    function cruiselineDropdownClicked (dropdown, dropdownButton) {
    
        dropdownButton.text( stringWithStrippedTags(dropdown.innerHTML.substring(0, dropdown.innerHTML.indexOf('Port')) + " ") );
        dropdownButton.append( '<span class="caret"></span>' );
        cruiselineSelected = true;
        checkIfFinished();

        if (oneWayToHotel || roundTripAllWays)
        {
            moveToNextBox(8, 9);
        }
        else if (oneWayToAirport) {
            moveToNextBox(8, 10);
        }
        else if (oneWayFromAirport) {
            moveToNextBox(11, 5);
        }
        else if (roundTrip) {
            moveToNextBox(8, 999)
        }
    }


    $('#cruiseline-dropdown').on('click', 'li > a', function(e){
        var dropdownButton = $('#dropdown-button-cruiseline');
        cruiselineDropdownClicked(this, dropdownButton);
    });
	
    $('#cruiseline-airport-dropdown').on('click', 'li > a', function(e){
        var dropdownButton = $('#dropdown-button-cruiseline-in-airport');
        cruiselineDropdownClicked(this, dropdownButton);
    });

    $('#hotels-dropdown').on('click', 'li > a', function(e){
        $('#dropdown-button-hotel').text( $(this).text() );
        $('#dropdown-button-hotel').append( '<span class="caret"></span>' );
        hotelSelected = true;
        checkIfFinished();

        moveToNextBox(9, 5);
    });

    var now = new Date();
    now.setDate(now.getDate());
    var firstTimeShowing = true;
    var earliestDate = setEarliestDate();

    function setEarliestDate () {
        earliestDate = new Date();
        earliestDate.setDate(earliestDate.getDate());
        //console.log(earliestDate);
        return earliestDate;
    }

	$('#dp3').datepicker(
        { 
            format : 'yyyy/MM/dd',
            onRender: function (date) {                
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) { console.log('!!!');
            //We don't want to show that the date has been sold out once the date changes unless it actually is.
            airportSoldOut.hide();
       		$('#dp3 input').val($.format.date(ev.date, "MM/dd/yyyy"));
            
            flightDate = $('#dp3 input').val();

            getAvailability (flightTimeEntered, flightDateEntered, flightDate, flightTime, airportSoldOut, false, 2, $('#dropdown-button-flight'));
            
            if (flightTime) {
                airportTimeSoldOut.empty();
                getTimeAvailability (flightTime, flightDateEntered, flightTimeEntered, flightDate, flightTime, airportTimeSoldOut, 3, false);
            }
            checkIfFinished();
            //If this is a round trip then we make sure that the earliest date for the return date is later then the date just selected
            
            
            //Set what the earliest date to one day ahead of whatever date just selected
            earliestDate = new Date(ev.date);
            earliestDate.setDate(earliestDate.getDate()+1);   

            returnDate.setValue(earliestDate);             
            
            var parseDate = new Date(returnDate.element.find('input').val());
            
            if (parseDate < earliestDate) {
                returnDate.element.find('input').val('');
            }
            $(this).datepicker('hide');   
	}).on('show', function () {
        if (firstTimeShowing == true)
        {
            var nextDay = new Date(Date.now() + (60*60*24*1000));
            $('#dp3').datepicker('setValue', nextDay);
        }

        firstTimeShowing = false;
    }).data('datepicker');

    //Get the return date information from the date picker that is displayed if the date is not sold out.
    var returnDate = $('#dp4').datepicker(
        { 
            format : 'yyyy/MM/dd',
            onRender: function (date) {
                //var myDate = new Date(earliestDate);
                //myDate.setDate(myDate.getDate() + 1);

                return date.valueOf() < earliestDate.valueOf() ? 'disabled' : '';
            }

        }).on('changeDate', function(ev){

            //We don't want to show that the airport has been sold out once the date changes unless it actually is.
            cruiseSoldOut.hide();
            //Check the current selected date to see if it is a valid date.
                    $('#dp4 input').val($.format.date(ev.date, "MM/dd/yyyy"));

            cruiseDate = $('#dp4 input').val();

            var imageTagToChange;
            var secondImageTagToChange;

            getAvailability (cruiseTimeEntered, cruiseDateEntered, cruiseDate, cruiseTime, cruiseSoldOut, true, 6, cruiseTimeDropdown);
            if (cruiseTime){
                cruiseTimeSoldOut.empty();
                getTimeAvailability (cruiseTime, cruiseDateEntered, cruiseTimeEntered, cruiseDate, cruiseTime, cruiseTimeSoldOut, 7, true);
            }
            $(this).datepicker('hide');  
            checkIfFinished();      

        }).on('show', function () {

            $('#dp4').datepicker('setValue', earliestDate);
            //$('#dp4').datepicker('update');
        }).data('datepicker');
        
    function getAvailability (timeEntered, dateEntered, date, time, displaySoldOutSection, isCruise, finishedImageTag, timeDropdown) {

        showBookingSuccessful(displaySoldOutSection, finishedImageTag, finishedImageTag + 1);
        //Enable the ability to get the time
        timeDropdown.prop('disabled', false);

        if (!isCruise)
        {            
            flightDateEntered = true;       
        }
        else if (isCruise)
        {
            cruiseDateEntered = true;
        }
    }


    function getTimeAvailability (time, dateEntered, timeEntered, date, time, displaySoldOutSection, finishedImageTag, isCruise) {
        //If the user has already entered the flight date information then we check to see if this particular date and time
        //is available for booking, otherwise do nothing.
        var dateWithEuropeFormat = changeDate(date);
        
        smartButton.prop('disabled', true);
        // If either time is not good, we need to disable it's selection from the combobox
        checkDateAndTimeAvailability(dateWithEuropeFormat, time, isCruise)
         .then(function(_timeEntered){
            timeEntered = _timeEntered;
            dateEntered = timeEntered;

            if (!isCruise)
            {
                flightTimeEntered = timeEntered;
            }
            else if (isCruise)
            {
                cruiseTimeEntered = timeEntered;
            }

            if (timeEntered)
            {
                showBookingSuccessful(displaySoldOutSection, finishedImageTag, finishedImageTag + 1);
            }
            else {
                showTimeBooked(displaySoldOutSection, finishedImageTag);
                smartButton.prop('disabled', true);
            }
        });
    }
    



    //When the flight number text box is changed we want to display that something has been inputed 
    //and update the UI accordingly.  But if this text box is changed to empty then we want to make sure
    //that we update the UI accordingly and make sure the user cannot advance.
    $('#flight-number').change(function  () {
        $(this).val($(this).val().toUpperCase());    
        handleFlightNumberInput($(this));    
    });

    function handleFlightNumberInput (flightNumberInput) {
        flightNumberEntered = true;
        //Enable the button.
        smartButton.prop('disabled', false);

        $(".finished[tag=5]").attr('src', "/smart-images/check-mark-green.png");
        $(".finished[tag=5]").attr('height', 15);
        $(".finished[tag=5]").attr('width', 15);

        //IF they change the value back to empty then we need to make note of that
        if ($(flightNumberInput).val().length == 0) {
            $(".finished[tag=5]").attr('src', "/smart-images/check-required.png");
            $(".finished[tag=5]").attr('height', 10);
            $(".finished[tag=5]").attr('width', 10);            

            flightNumberEntered = false;            
            //Remove the ability to press the button
            smartButton.prop('disabled', true);        
        }
        else if (roundTripAllWays)
        {
            moveToNextBox(5, 6);
        }
        else if (oneWayToHotel) {
            moveToNextBox(5, 9);
        }
        else {
            moveToNextBox(5, 11);
        }
        
        $('#flight-number').blur();
		checkIfFinished();
    }

    $('#flight-number').focus(function() {
      var input = $(this);
      if (input.val() == input.attr('placeholder')) {
        input.val('');
        input.removeClass('placeholder');
      }
    }).blur(function() {
    var input = $(this);
      if (input.val() == '' || input.val() == input.attr('placeholder')) {
        input.addClass('placeholder');
        input.val(input.attr('placeholder'));
      }
    }).blur();  

    $('#flight-number').keypress(function (e) {
        if(e.keyCode === 13) {
            $(this).val($(this).val().toUpperCase());
            handleFlightNumberInput($(this));
        }
    });


    $('#cruise-date').keydown(function () {
        //don't allow the user to enter in any text with the keyboard
        return false;
    })

    function checkBox (box) {
       $(box).attr('src', "/smart-images/check-mark-green.png"); 
    }

    smartButton.click( function  (response) {        
        //We set the cruise date and the cruise times so that we can add them to the database
        var cruiseDate = $('#cruise-date').val();
        var cruiseTime = cruiseTimeDropdown.text();
        var airportDate = $('#airport-date').val();
        var airportTime = $('#dropdown-button-flight').text();
        var cruiseShip = $('#dropdown-button-cruiseline').text();
        var airline = $('#dropdown-button-airline').text();
        var airlineInCruise = $('#dropdown-button-airline-in-cruise').text();
        var cruiseShipInAirport = $('#dropdown-button-cruiseline-in-airport').text();
        var flightNumber = $('#flight-number').val();
        var numTravelers = numOfTravelers;
        var hotel = $('#dropdown-button-hotel').text();

        //Create a dictionary that will be stored as a cookie
        var reservation = {
            cruiseDate          : cruiseDate,
            cruiseTime          : cruiseTime,
            airportDate         : airportDate,
            airportTime         : airportTime,
            cruiseShip          : cruiseShip,
            cruiseShipInAirport : cruiseShipInAirport,
            travelType          : travelTypeId,
            flightNumber        : flightNumber,
            airline             : airline,
            airlineInCruise     : airlineInCruise,
            price               : price,
            numOfTravelers      : numTravelers,
            hotel               : hotel
        };

        //Make the expiration time five minutes from the current time
        var minutes = 5;

        var expirationTime = new Date();
        expirationTime.setTime(expirationTime.getTime() + (minutes * 60 * 1000));
        //create a cookie storing the reservation information
        $.cookie('reservation', JSON.stringify(reservation), { expires : expirationTime } );
		
		for (var i in reservation) {
			reservation[i] = encodeURIComponent(reservation[i]);
		}
        createURL(reservation);
    });

    function createURL (reservation) {
        switch (travelTypeId)
        {
            case ONE_WAY_TO_AIRPORT_ID:
                window.location.replace("/customerinfo.html?cruiseDate=" + reservation.cruiseDate + 
                "&cruiseTime=" + reservation.cruiseTime + "&cruiseShip=" + reservation.cruiseShip +
                "&airline=" + reservation.airlineInCruise + "&travelTypeId=" + reservation.travelType +
                "&price=" + price + "&numOfTravelers=" + numOfTravelers);      

                break; 
            case ONE_WAY_TO_CRUISE_ID:
                window.location.replace("/customerinfo.html?airportDate=" + reservation.airportDate + 
                "&airportTime=" + reservation.airportTime + "&cruiseShip=" + reservation.cruiseShipInAirport +
                "&travelTypeId=" + reservation.travelType + "&flightNumber=" + reservation.flightNumber +
                "&airline=" + reservation.airline + "&price=" + price + "&numOfTravelers=" + numOfTravelers);       

                break;
            case ROUNDTRIP_ID:
                window.location.replace("/customerinfo.html?cruiseDate=" + reservation.cruiseDate + 
                "&cruiseTime=" + reservation.cruiseTime + "&airportDate=" + reservation.airportDate + 
                "&airportTime=" + reservation.airportTime + "&cruiseShip=" + reservation.cruiseShip +
                "&travelTypeId=" + reservation.travelType + "&flightNumber=" + reservation.flightNumber +
                "&airline=" + reservation.airline + "&price=" + price + "&numOfTravelers=" + numOfTravelers);       

                break;
            case ONE_WAY_TO_HOTEL_ID:
				window.location.replace("/customerinfo.html?airportDate=" + reservation.airportDate + 
                "&airportTime=" + reservation.airportTime +
                "&travelTypeId=" + reservation.travelType + "&flightNumber=" + reservation.flightNumber +
                "&hotel=" + reservation.hotel + 
                "&airline=" + reservation.airline + "&price=" + price + "&numOfTravelers=" + numOfTravelers);
            case THREEWAY_TRIP_ID:
                window.location.replace("/customerinfo.html?airportDate=" + reservation.airportDate +
                "&airportTime=" + reservation.airportTime +
                "&travelTypeId=" + reservation.travelType + "&flightNumber=" + reservation.flightNumber +
                "&hotel=" + reservation.hotel + "&cruiseDate=" + reservation.cruiseDate +
                "&cruiseTime=" + reservation.cruiseTime + "&cruiseShip=" + reservation.cruiseShip +
                "&airline=" + reservation.airline + "&price=" + price + "&numOfTravelers=" + numOfTravelers);
        }
        
    }

    $('#clear-bg-button').click( function (e) {
		var toggled = $(this).is('.toggled');
		
		$(this).toggleClass('toggled');
		
        if (!toggled)
        {
            $(".content-paragraph").css('height', "auto");
            $(this).text('Show Less');
        }
        else {
            $(".content-paragraph").css('height', 140);
            $(this).text('Read More');
        }

		e.preventDefault();
    });

});

function setTravelTypeId (travelTypeId) {
    myTravelTypeId = travelTypeId;
}

function getTravelTypeId () {
    return myTravelTypeId;
}

function setNumberOfTravelers (numberOfTravelers) {
    myNumOfTravelers = numberOfTravelers;
}

function getNumberOfTravelers () {
    return myNumOfTravelers;
}

function displayPrice () {
    getPrice();

    $('#price-tagline').empty();
    $('#price-tagline').append('<span style = "color: yellow">TOTAL PRICE FOR ' + strNumOfTravelers + ':</span>');
    //price = '1';
    $('#price').empty();
    $('#price').append('<span>$' + price + ' USD*</span><p> *special rate (all fees included) </p><p> (reg. price: '+ originalPrice +')</p>');
}

function getPrice () {

    var results = getPricesAndNumberOfTravelers(numOfTravelers, roundTrip, roundTripAllWays, oneWayToHotel);

    price = results.price;
    originalPrice = results.originalPrice;
    strNumOfTravelers = results.numOfTravelers;
}

function changeBoxes (image) {
    for (var i = 1; i < boxes.length; i++) {
        $(".finished[tag=" + i + "]").attr('src', "/smart-images/" + image + ".png");
        $(".finished[tag=" + i + "]").attr('height', 10);
        $(".finished[tag=" + i + "]").attr('width', 10);
    };
}
