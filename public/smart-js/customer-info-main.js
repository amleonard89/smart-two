$(document).ready( function  () {

    var inputs = $('#customer-info-form :input').validator();
    var reservationParams = new Array();
    var firstNameEntered = false;
    var lastNameEntered = false;
    var phoneNumberEntered = false;
    var emailEntered = false;

    var creditCardNumberInputBox = $('input[name=credit-card-number]');
    var csvNumberInputBox = $('input[name=csv-number]');
    var firstNameInputBox = $('input[name=first-name]');
    var lastNameInputBox = $('input[name=last-name]');
    var phoneNumberInputBox = $('input[name=phone-number]');
    var phoneNumberInputBox2 = $('input[name=phone-number2]');
    var firstNameInputBox = $('input[name=first-name]');
    var emailInputBox = $('input[name=email]');
    var requestCommentsInputBox = $('textarea[name=request-comments]');
    var expirationDateInputBox = $('input[name=expiry-date]');

    var airportDate;
    var cruiseDate;
    var airportTime;
    var cruiseTime;
    var travelTypeId;
    var numberOfTravelers;
    var flightNumber;


    var amount;

    var cookieAvailable = false;

    $('#dim').hide();
    $('#loading').hide();

    var APPROVED = '201';

    var responseJSON = {    'Bad Request (25) - Invalid Expiry Date' : 'Invalid Expiration Date',
                            'Bad Request (22) - Invalid Credit Card Number' : 'Invalid Credit Card Number' 
                        };

    emptyTextBoxes();
    getParams();

    

    function emptyTextBoxes () {
        creditCardNumberInputBox.val('');
        csvNumberInputBox.val('');
        firstNameInputBox.val('');
        lastNameInputBox.val('');
        phoneNumberInputBox.val('');
        firstNameInputBox.val('');
        emailInputBox.val('');
        requestCommentsInputBox.val('');
        expirationDateInputBox.val('');
    }

    function addDataToSave () {
        var fullName = firstNameInputBox.val() + ' ' + lastNameInputBox.val();
        
        var phone1 = phoneNumberInputBox.val();
        var phone2 = phoneNumberInputBox2.val();
        
        var phone = [phone1];
        if (phone2.length > 0) phone.push(phone2);
        
        reservationParams[nameKey] = fullName;
        reservationParams[emailKey] = emailInputBox.val();
        reservationParams[phoneNumberKey] = phone.join(', ');
        reservationParams[specialRequestKey] = requestCommentsInputBox.val(); 
    }

    creditCardNumberInputBox.val('');

    if($.cookie('reservation'))
    {
        //get the cookie containing the reservation information that was just entered
        var reservation = JSON.parse($.cookie('reservation'));    
    }
    else {
        cookieAvailable = false;
    }

    var boxes = [];
    //Go through each of the text boxes on the screen and set the image next to them to not selected
    for (var i = 1; i < 10; i++) {
        boxes[i] = "Not Selected";
    };    

    $('#smart-button').click( function  (response) {
        
        //Placeholders.disable();
        placeholderOff();
        //Remove the ability to press the button
        $('#smart-button').prop('disabled', true); 
        $('#error').empty();
        
        $('#dim').show();
        $('#loading').show();

        //First we need to make sure that all the values inserted are valid
        var valid = inputs.data("validator").checkValidity();

        if (valid)
        {
            addDataToSave();
            if (cookieAvailable)
            {
                amount = reservation.price;
            }
            
            var data = {
                booking: getReservationDocument(reservationParams).reservation,
                card: {
                    cc_number : creditCardNumberInputBox.val(),
                    cc_expiry : expirationDateInputBox.val()
                }
            };
            
            $.ajax({
                url: 'reservation',
                type: 'POST',
                data: JSON.stringify(data),
                contentType : 'application/json',
                dataType: 'json'
            })
            .then(function(data){
                if (data.error)
                {
                    if (data.error == 'PaymentError') {
                        $('#error').append('<div>' + 'Sorry your card was declined by the bank, please make sure you entered the information correctly' + '</div>');
                    }
                    else {
                        $('#error').append('<div>' + 'There was an error. We apologize for the inconvenience.' + '</div>');
                    }
                    
                    $(window).scrollTop($('#error').offset().top);
                    $('#smart-button').prop('disabled', false); 
                }
                else
                {
                    window.location = 'confirmation.html?confirmation=' + data.confirmationCode;
                }
            },
            function(err){
                console.log(err);
            });
            
            /*$.ajax ({
                url: "/e4",
                type: "POST",
                async: true,
                data : JSON.stringify({
                    cardholder_name : reservationParams[nameKey],   
                    cc_number       : creditCardNumberInputBox.val(),
                    cc_expiry       : expirationDateInputBox.val(),  
                    travelTypeId    : reservationParams[travelTypeKey],  
                    numberOfTravelers : reservationParams[numberOfTravelersKey]            
                }),
                contentType : 'application/json',
                dataType: "json",
                success: function  (response) {
                    //Check to see if the transaction was approved or not
                    if (response.hasOwnProperty('transaction_approved')) {
                        if (response.transaction_approved == 1)
                        {
                            var customerCode = createCode().substring(1, 8).toUpperCase();
                            reservationParams[confirmationCodeKey] = customerCode;
                            //Method in setDate.js which will save the information to the mongo database.

                            var mongo = saveDataToMongo(reservationParams, false, response);

                            mongo.then(function(ok){
                                return sendEmail(customerCode);
                            }, function(err){
                                console.log(arguments);
                            })
                            .then(function(data){
                                window.location = 'confirmation.html?confirmation=' + customerCode;
                            });
                        }
                        else {
                            $('#error').append('<div>' + 'Sorry your card was declined by the bank, please make sure you entered the information correctly' + '</div>');   
                            $('#smart-button').prop('disabled', false); 
                        }                        
                    }
                    else if (response.code == '400') {
                        $('#error').empty();
                        //Get the user friendly error message from the JSON file containing the non user friendly error messages as keys
                        $('#error').append('<div>' + responseJSON[response.body] + '</div>');
                        $('#smart-button').prop('disabled', false); 
                    }
                    //Don't show the dim screen so that it doesn't just roll around forever
                    $('#dim').hide();
                    $('#loading').hide();
                    
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log(errorThrown);

                    $('#dim').hide();
                    $('#loading').hide();
                    $('#smart-button').prop('disabled', false); 
                },
                complete: function() {
                    //
                }
            });*/
        }
        else {
            //Remove the ability to press the button
            $('#smart-button').prop('disabled', false); 

            $('#dim').hide();
            $('#loading').hide();
            //placeholderOn();
        }
        
        placeholderOn();
    });

    creditCardNumberInputBox.change(function  () {

        handleInputChange(1, 2, $(this));
    });

    creditCardNumberInputBox.keypress(function (e) {
        if(e.keyCode === 13) {
            handleInputChange(1, 2, $(this));
        }
    });    

    expirationDateInputBox.change(function  () {
        handleInputChange(2, 3, $(this));
    });

    expirationDateInputBox.keypress(function (e) {
        if(e.keyCode === 13) {
            handleInputChange(2, 3, $(this));
        }
    });    

    firstNameInputBox.change(function  () {
        handleInputChange(3, 4, $(this));
    });

    firstNameInputBox.keypress(function (e) {
        if(e.keyCode === 13) {
            handleInputChange(3, 4, $(this));
        }
    });    

    lastNameInputBox.change(function  () {
        handleInputChange(4, 5, $(this));
    });

    lastNameInputBox.keypress(function (e) {
        if(e.keyCode === 13) {
            handleInputChange(4, 5, $(this));
        }
    });    

    phoneNumberInputBox.change(function  () {
        handleInputChange(5, 6, $(this));
    });

    phoneNumberInputBox.keypress(function (e) {
        if(e.keyCode === 13) {
            handleInputChange(5, 6, $(this));
        }
    });    

    emailInputBox.change(function  () {
        handleInputChange(6, 999, $(this));
    });

    emailInputBox.keypress(function (e) {
        if(e.keyCode === 13) {
            handleInputChange(6, 999, $(this));
        }
    });    

    function handleInputChange (tag, nextTag, inputBox) {
        if ($(inputBox).val().length == 0) {
            $(".finished[tag=" + tag + "]").attr('src', "/smart-images/check-required.png");
            $(".finished[tag=" + tag + "]").attr('height', 10);
            $(".finished[tag=" + tag + "]").attr('width', 10);            

            //Remove the ability to press the button
            $('#smart-button').prop('disabled', true);        
        }
        else
        {
            moveToNextBox(tag, nextTag);
            checkIfFinished();
        }
    }

    requestCommentsInputBox.change(function  () {
        
    });
     // Regular Expression to test whether the value is valid
    // $.tools.validator.fn("[type=phone-number]", "Please supply a valid phone number", function(input, value) {
    // return /^\d\d\d-\d\d\d-\d\d\d\d$/.test(value);
    // });

    function checkIfFinished () {
        if (firstNameEntered && lastNameEntered && phoneNumberEntered && emailEntered)
        {
			if (flightNumberEntered || travelTypeId == 0)
				$('#smart-button').prop('disabled', false);
        }
    }

    function moveToNextBox (oldTag, newTag) {

        $(".finished[tag=" + oldTag + "]").attr('src', "/smart-images/check-mark-green.png");

        if (boxes[newTag] != "selected")
        {
            if (newTag == 9)
            {
                $(".finished[tag=" + newTag + "]").attr('src', "/smart-images/check-required.png");        
            }
            else
            {
                $(".finished[tag=" + newTag + "]").attr('src', "/smart-images/check-current.png");   
            }
        }      
        
        $(".finished[tag=" + oldTag + "]").attr('height', 15);
        $(".finished[tag=" + oldTag + "]").attr('width', 15);

        boxes[oldTag] = "selected";
    };

    function createCode () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    }

    function getParams () {
        var queryString = new Array();

         $(function () {
            if (queryString.length == 0) {
                if (window.location.search.split('?').length > 1) {
                    var params = window.location.search.split('?')[1].split('&');
                    for (var i = 0; i < params.length; i++) {
                        var key = params[i].split('=')[0];
                        var value = decodeURIComponent(params[i].split('=')[1]);
                        queryString[key] = value;
                    }
                }
            }

            reservationParams = queryString;            

            var numberString;
             numberOfTravelers = queryString[numberOfTravelersKey];
            switch( numberOfTravelers ) {
                case "2":
                    numberString = "TWO";
                    break;
                case "3": 
                    numberString = "THREE";
                    break;
                case "4":
                    numberString = "FOUR";
                    break;
                case "5":
                    numberString = "FIVE";
                    break;
                case "6":
                    numberString = "SIX";
                    break;
                case "7":
                    numberString = "SEVEN";
                    break;
                case "8":
                    numberString = "EIGHT";
                    break;
            }
            $('#price-tagline').empty();
            $('#price-tagline').append('<span style = "color: yellow">TOTAL PRICE FOR ' + numberString + ':</span>');                
            $('#price').empty();
            $('#price').append('<span>$' + reservationParams[priceKey] + ' USD</span>');  

        });     
    };

    function sendEmail (confirmationNumber) {
        return $.ajax({
                url : 'smartEmail/'+confirmationNumber
            });
            
        var emailed;
        var fullName = firstNameInputBox.val() + ' ' + lastNameInputBox.val();

        $.ajax ({
            url: "/mandrill/sendEmail",
            type: "POST",            
            async: false,
            contentType : 'application/json',
            dataType: "json",
            data : JSON.stringify({
                email : emailInputBox.val(),
                phone : reservationParams[phoneNumberKey],
                travelTypeId : reservationParams[travelTypeKey],
                confirmationNumber : reservationParams[confirmationCodeKey],
                price : reservationParams[priceKey],
                cruiseDate : reservationParams[cruiseDateKey],
                cruiseTime : reservationParams[cruiseTimeKey],
                flightDate : reservationParams[airportDateKey],
                flightTime : reservationParams[airportTimeKey],
                hotel : reservationParams[hotelKey],
                name : fullName,
                numberOfTravelers : reservationParams[numberOfTravelersKey],
                airline : reservationParams[airlineKey],
                flightNumber : reservationParams[flightNumberKey],
                specialRequest : reservationParams[specialRequestKey],
                cruiseShip : reservationParams[cruiseShipKey]
            }),
            success: function  (response) {
                //alert(response);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    }
});


