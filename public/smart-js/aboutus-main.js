$(document).ready( function  ()
{

    isMobilePhone();

    function isMobilePhone () {
        var navBar = $('.top-section');
        var video = $('.video').find('iframe');
        var socialNetwork = $('#social-network');
        var phone = $('.phone');
        var headerMessage = $('.headerMessage');

        if ($('#isPhone').css('display') == 'block')
        {
            $('.row').css('width', '105%');

            navBar.css('width', '100%');
            navBar.css('margin-left', '1px');
            navBar.css('margin-top', '50px');
            video.css('margin-left', '15px');
            socialNetwork.css('margin-right', '-0px');

            headerMessage.css('margin-top', '0px');
            headerMessage.css('font-size', '16px');

            phone.toggleClass('phone', false);
            phone.toggleClass('headerPhone', true);
        }
        else {
            navBar = $('.about-us-nav-bar');
            navBar.remove();
        }
    }

});