// $(document).ready( function () {
//     var images = ['smart-images/slider-gettingYouThere.png', 'smart-images/slider-shuttleBeyond.png'];
//     var index = 0;

//     setInterval (rotateImage, 6000);

//     function rotateImage () {
//         $('.tagline').animate({ "margin-right" : '400px', opacity : 0}, 300, function(){
//         $('#image-tagline').attr('src', images[index]);
//         $(this).css("margin-right", "0").animate({opacity : 1,}, 300, function(){
//             if (index == 0){
//                 index = 1;
//             }
//             else if (index == 1){
//                 index = 0;
//             }
//         });
//     })
        

//     }


// });


$(function(){
    if ($('.slides').length != 0) {
        var currentIndex = 0, slides = $('.slides > li');
        $(slides[currentIndex]).css('left','0%');
        function slide() {
            var current, next;
            current = $(slides[currentIndex]);
            currentIndex++;
            if(currentIndex === slides.length){
              currentIndex = 0;   
            }
            next = $(slides[currentIndex]);
            next.css('left','100%');
            next.css('z-index',parseInt(current.css('z-index'), 10)+1);
            next.animate({left: '0%'}, 1000);
        }
        var s = window.setInterval(slide, 10000);
    }
})