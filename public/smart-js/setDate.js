function createCode () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });    
  };

//Check to make sure that the date does not have too many reservations already.
function checkDate(date, callback, travelTypeId) {
  var request = $.ajax({
      url: "/mongo/post/numberOfReservations",
      type: 'POST',
      async: false,
      data: JSON.stringify(
        { 
          "date.date" : date,
          "date.travelTypeId" : travelTypeId
        }),
      contentType: 'application/json',
      success: function  (response) {
        callback(response);
      },
      error: function  (xhr, status, error) {
        alert(error);
      }
  });
}

function checkDateAndTime (date, time, numberOfTravelers, callback) {
  return $.ajax({
      url: "/isAvailable",
      type: 'POST',
      data: JSON.stringify(
        { 
          "date" : date,
          "time" : time,
          "seats" : numberOfTravelers
        }),
      contentType: 'application/json',
      
      error: function  (xhr, status, error) {
        alert(error);
      }
  });
}

function getCruiseJSONDocument (params) {
  return { 
    reservation : {
      type                 : params[travelTypeKey],
      name                 : params[nameKey],
      email                : params[emailKey],
      phoneNumber          : params[phoneNumberKey],
      specialRequest       : params[specialRequestKey],
      cruiseInfo : { 
        date : changeDate(params[cruiseDateKey]),
        time : params[cruiseTimeKey],
        ship : params[cruiseShipKey]
      },
      airline              : params[airlineKey],
      confirmationCode     : params[confirmationCodeKey],
      numberOfTravelers    : params[numberOfTravelersKey]            
    } 
  };
}

function getAirportJSONDocument (params) {
  return { 
    reservation : {
      type                 : params[travelTypeKey],
      name                 : params[nameKey],
      email                : params[emailKey],
      phoneNumber          : params[phoneNumberKey],
      specialRequest       : params[specialRequestKey],
      airportInfo : {
        date : changeDate(params[airportDateKey]),
        time : params[airportTimeKey],
        airline : params[airlineKey],
        flightNumber : params[flightNumberKey]
      },
      cruiseShip           : params[cruiseShipKey],
      confirmationCode     : params[confirmationCodeKey],
      numberOfTravelers    : params[numberOfTravelersKey]            
    } 
  };
}

function getAirportAndHotelJSONDocument (params) {
  return { 
    reservation : {
      type                 : params[travelTypeKey],
      name                 : params[nameKey],
      email                : params[emailKey],
      phoneNumber          : params[phoneNumberKey],
      specialRequest       : params[specialRequestKey],
      airportInfo : {
        date : changeDate(params[airportDateKey]),
        time : params[airportTimeKey],
        airline : params[airlineKey],
        flightNumber : params[flightNumberKey]
      },
      hotel                : params[hotelKey],
      confirmationCode     : params[confirmationCodeKey],
      numberOfTravelers    : params[numberOfTravelersKey]            
    } 
  }; 
}

function getRoundtripJSONDocument (params) {
  return { 
      reservation : {
        type                 : params[travelTypeKey],
        name                 : params[nameKey],
        email                : params[emailKey],
        phoneNumber          : params[phoneNumberKey],
        specialRequest       : params[specialRequestKey],
        cruiseInfo : { 
          date : changeDate(params[cruiseDateKey]),
          time : params[cruiseTimeKey],
          ship : params[cruiseShipKey]
        },
        airportInfo : {
          date : changeDate(params[airportDateKey]),
          time : params[airportTimeKey],
          airline : params[airlineKey],
          flightNumber : params[flightNumberKey]
        },
        confirmationCode     : params[confirmationCodeKey],
        numberOfTravelers    : params[numberOfTravelersKey]            
      } 
    };
}

function getThreewayTripJSONDocument (params) {
    return {
        reservation : {
            type                 : params[travelTypeKey],
            name                 : params[nameKey],
            email                : params[emailKey],
            phoneNumber          : params[phoneNumberKey],
            specialRequest       : params[specialRequestKey],
            cruiseInfo : {
                date : changeDate(params[cruiseDateKey]),
                time : params[cruiseTimeKey],
                ship : params[cruiseShipKey]
            },
            airportInfo : {
                date : changeDate(params[airportDateKey]),
                time : params[airportTimeKey],
                airline : params[airlineKey],
                flightNumber : params[flightNumberKey]
            },
            hotel                : params[hotelKey],
            confirmationCode     : params[confirmationCodeKey],
            numberOfTravelers    : params[numberOfTravelersKey]
        }
    };
}

function getReservationDocument(params)
{
    var data,
        travelTypeId = parseInt(params[travelTypeKey]);

    switch (travelTypeId)
    {
      case ONE_WAY_TO_AIRPORT_ID:
        data = getCruiseJSONDocument(params);
        break;
      case ONE_WAY_TO_CRUISE_ID:
        data = getAirportJSONDocument(params);   
        break;
      case ONE_WAY_TO_HOTEL_ID:
        data = getAirportAndHotelJSONDocument(params);
        break;
      case ROUNDTRIP_ID:
        data = getRoundtripJSONDocument(params);
        break;
      case THREEWAY_TRIP_ID:
        data = getThreewayTripJSONDocument(params);
        break;
    }
    
    return data;
};

function changeDate (date)
{
    var delimiter = '/';
    var start = 0;
    var tokens = date.split(delimiter).slice(start);

    //console.log(tokens);
    return tokens[2] + '/' + tokens[0] + '/' + tokens[1];
}

function saveDataToMongo (params, options, card) {

  var data;
  var travelTypeId = parseInt(params[travelTypeKey]);


  switch (travelTypeId)
  {
    case ONE_WAY_TO_AIRPORT_ID:
      data = getCruiseJSONDocument(params);
      break;
    case ONE_WAY_TO_CRUISE_ID:
      data = getAirportJSONDocument(params);   
      break;
    case ONE_WAY_TO_HOTEL_ID:
      data = getAirportAndHotelJSONDocument(params);
      break;
    case ROUNDTRIP_ID:
      data = getRoundtripJSONDocument(params);
      break;
    case THREEWAY_TRIP_ID:
      data = getThreewayTripJSONDocument(params);
      break;
  }

  if (card) {
    data.paymentInfo = card;
  }

  var ajaxParams = {
      url: "/mongo/post/reservation",
      type: 'POST',
      dataType: 'json',
      data: JSON.stringify(data),
      contentType: "application/json"
  };
  
  if (options) {
      $.extend(ajaxParams, options);
  }
  
  return $.ajax(ajaxParams);
};

