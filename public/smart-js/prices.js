var calculatePrices = function(number, trips) {
    var types = [];
    var prices = {};
    var pricesIdx = {};
    var totalPrice = 0;
    
    for (var i in trips) {
        var trip = trips[i];
        
        var tag = trip.origin+'-'+trip.destination;
        var price = 0;
        
        switch (tag) {
            case 'airline-cruise':
            case 'cruise-airline':
                var reverse = types.indexOf((tag == 'airline-cruise') ? 'cruise-airline' : 'airline-cruise') !== -1;

                switch (number) {
                    case 1:
                    case 2:
                        price += !reverse ? 69 : 60; break;
                        
                    case 3:
                        price += !reverse ? 89 : 90; break;
                        
                    case 4:
                        price += !reverse ? 99 : 100; break;
                        
                    case 5:
                        price += !reverse ? 119 : 110; break;
                        
                    case 6:
                        price += !reverse ? 139 : 130; break;
                        
                    case 7:
                        price += !reverse ? 159 : 140; break;
                        
                    case 8:
                        price += !reverse ? 169 : 160; break;
                }
                break;
                
            case 'airline-hotel':
            case 'hotel-airline':
                var reverse = types.indexOf((tag == 'airline-hotel') ? 'hotel-airline' : 'airline-hotel') !== -1;
                
                switch (number) {
                    case 1:
                    case 2:
                        price += !reverse ? 79 : 60; break;
                        
                    case 3:
                        price += !reverse ? 89 : 90; break;
                        
                    case 4:
                        price += !reverse ? 99 : 100; break;
                        
                    case 5:
                        price += !reverse ? 119 : 110; break;
                        
                    case 6:
                        price += !reverse ? 139 : 130; break;
                        
                    case 7:
                        price += !reverse ? 159 : 140; break;
                        
                    case 8:
                        price += !reverse ? 169 : 160; break;
                }
                break;
                
            case 'airline-address':
            case 'address-airline':
                var reverse = types.indexOf((tag == 'airline-address') ? 'address-airline' : 'airline-address') !== -1;
                
                switch (number) {
                    case 1:
                    case 2:
                        price += !reverse ? 89 : 90; break;
                        
                    case 3:
                        price += !reverse ? 99 : 100; break;
                        
                    case 4:
                        price += !reverse ? 109 : 100; break;
                        
                    case 5:
                        price += !reverse ? 129 : 120; break;
                        
                    case 6:
                        price += !reverse ? 139 : 140; break;
                        
                    case 7:
                        price += !reverse ? 159 : 140; break;
                        
                    case 8:
                        price += !reverse ? 169 : 160; break;
                }
                break;
                
            case 'cruise-address':
            case 'address-cruise':
            case 'hotel-address':
            case 'address-hotel':
                if (tag == 'hotel-address' || tag == 'address-hotel')
                    var reverse = types.indexOf((tag == 'hotel-address') ? 'address-hotel' : 'hotel-address') !== -1;
                else
                    var reverse = types.indexOf((tag == 'cruise-address') ? 'address-cruise' : 'cruise-address') !== -1;
                
                switch (number) {
                    case 1:
                    case 2:
                        price += !reverse ? 89 : 90; break;
                        
                    case 3:
                        price += !reverse ? 109 : 100; break;
                        
                    case 4:
                        price += !reverse ? 129 : 120; break;
                        
                    case 5:
                        price += !reverse ? 159 : 140; break;
                        
                    case 6:
                        price += !reverse ? 189 : 180; break;
                        
                    case 7:
                        price += !reverse ? 209 : 200; break;
                        
                    case 8:
                        price += !reverse ? 229 : 220; break;
                }
                break;
        }
        
        types.push(tag);
        prices[tag] = price;
        pricesIdx[i] = price;
        totalPrice += price;
    };
    
    return [totalPrice, prices, pricesIdx];
};