//We check to see here if all the required fields have been entered
function checkIfFinished () {
	var isMobile = false;
	if ($('#isPhone').css('display') == 'block')
	{
		isMobile = true;
	}
	validated = false;
	//Make sure that the number of travelers has been put in then check for the rest
	if (numOfTravelersSelected)
	{
		if (roundTrip == true)
		{
			//If all fields are filled out
			if (flightDateEntered == true && flightTimeEntered == true &&
				cruiseDateEntered == true && cruiseTimeEntered == true &&
				cruiselineSelected == true && airlineSelected == true && flightNumberEntered == true)
			{
				displayPrice();
				validated = true;
			}
		}
		else if (oneWayFromAirport == true)
		{
			//If the flight information is filled out
			if (flightDateEntered == true && flightTimeEntered == true && airlineSelected == true && cruiselineSelected && flightNumberEntered == true)
			{
				displayPrice();
				validated = true;
			}
		}
		else if (oneWayToAirport == true)
		{
			//If the cruise information is filled out
			if (cruiseDateEntered == true && cruiseTimeEntered == true && cruiselineSelected == true && airlineSelected)
			{
				displayPrice();
				validated = true;
			}
		}
		else if (oneWayToHotel)
		{
			if (hotelSelected && flightDateEntered && flightTimeEntered && airlineSelected && flightNumberEntered == true)
			{
				displayPrice();
				validated = true;
			}
		}
		else if (roundTripAllWays)
		{
			if (flightDateEntered&& flightTimeEntered &&
				cruiseDateEntered && cruiseTimeEntered &&
				cruiselineSelected && airlineSelected &&
				hotelSelected && flightNumberEntered == true)
			{
				displayPrice();
				validated = true;
			}
		}

		if (validated)
		{
			smartButton.show();
			smartButton.prop('disabled', false);
		}
		else
		{
			$('#price-tagline').empty();
			$('#price').empty();
			
			smartButton.prop('disabled', true).hide();
		}
		//If we have validated that this worked, then we execute the following code
		if (validated && roundTrip == true && !isMobile)
		{
			//$("html, body").animate({ scrollTop: 200 }, 'slow');
		}
	}
}
