/**
 * Created by adeiji on 12/17/14.
 */
function reloadReservation (reservation) {

    //Reload the page entirely as it was before the user left the price-tagline.  We reload everything because we know that the only way
    //to go to the next page is to have entered in everything

    //Cruise Info
    cruiseTimeDropdown.text(reservation.cruiseTime);
    $('#dropdown-button-cruiseline').text(reservation.cruiseShip);
    $('#cruise-date').val(reservation.cruiseDate);
    $('#dropdown-button-airline-in-cruise').text(reservation.airlineInCruise);

    //Airline Info
    $('#airport-date').val(reservation.airportDate);
    $('#dropdown-button-flight').text(reservation.airportTime);
    $('#dropdown-button-airline').text(reservation.airline);
    $('#flight-number').val(reservation.flightNumber);
    $('#dropdown-button-cruiseline-in-airport').text(reservation.cruiseShipInAirport);

    $('#dropdown-button-hotel').text(reservation.hotel);

    travelTypeId = reservation.travelType;
    price = reservation.price;
    numOfTravelers = reservation.numOfTravelers;

    moveToNextBox(0, 1);
    moveToNextBox(1, 2);
    numOfTravelersSelected = true;

    $('#traveler-amount-box').text(numOfTravelers + " People");

    //  One way to airport
    if (reservation.travelType == ONE_WAY_TO_AIRPORT_ID)
    {
        loadReservationOneWayToAirport();
    }
    else if (reservation.travelType == ROUNDTRIP_ID)   // Roundtrip
    {
        loadReservationRoundtrip();
    }
    else if (reservation.travelType == ONE_WAY_TO_CRUISE_ID)   // One way to cruise
    {
        loadReservationoneWayFromAirport();
    }
    else if (reservation.travelType == ONE_WAY_TO_HOTEL_ID)   // One way to hotel
    {
        loadReservationOneWayToHotel();
    }
    else if (reservation.travelType == THREEWAY_TRIP_ID)
    {
        loadReservationThreewayTrip();
    }

    changeBoxes('check-mark-green');
    validated = true;
    smartButton.prop('disabled', false);
    checkIfFinished();
}

function loadReservationOneWayToAirport () {
    //Cruise Validations
    cruiseDateEntered = true;
    cruiseTimeEntered = true;
    cruiselineSelected = true;
    airlineSelected = true;

    moveToNextBox(6, 999);
    moveToNextBox(7, 999);
    moveToNextBox(8, 999);
    moveToNextBox(10, 999);

    $('#dropdown-button-type').text('One Way to Airport');

    displayPrice();

    validated = true;
    travelType = options[0];

    flightSection.hide();
    cruiseSection.show();
    cruiseTimeDropdown.prop('disabled', false);

    oneWayToAirport = true;
    airlineDropdownOuterClone.show();
}

function loadReservationRoundtrip () {
    //Cruise Validations
    cruiseDateEntered = true
    cruiseTimeEntered = true
    cruiselineSelected = true

    //Airport Validations
    flightTimeEntered = true
    flightDateEntered = true
    airlineSelected = true
    flightNumberEntered = true

    moveToNextBox(2, 999);
    moveToNextBox(3, 999);
    moveToNextBox(4, 999);
    moveToNextBox(5, 999);
    moveToNextBox(6, 999);
    moveToNextBox(7, 999);
    moveToNextBox(8, 999);
    moveToNextBox(9, 999);

    $('#dropdown-button-type').text('Roundtrip');

    validated = true;
    travelType = options[2];

    flightSection.show();
    cruiseSection.show();
    $('#dropdown-button-flight').prop('disabled', false);
    cruiseTimeDropdown.prop('disabled', false);

    displayPrice();
    roundTrip = true;
}


function loadReservationThreewayTrip () {
    //Cruise Validations
    cruiseDateEntered = true
    cruiseTimeEntered = true
    cruiselineSelected = true

    //Airport Validations
    flightTimeEntered = true
    flightDateEntered = true
    airlineSelected = true
    flightNumberEntered = true
    hotelSelected = true;

    moveToNextBox(2, 999);
    moveToNextBox(3, 999);
    moveToNextBox(4, 999);
    moveToNextBox(5, 999);
    moveToNextBox(6, 999);
    moveToNextBox(7, 999);
    moveToNextBox(8, 999);
    moveToNextBox(9, 999);

    $('#dropdown-button-type').text('Roundtrip');

    validated = true;
    travelType = options[4];

    flightSection.show();
    cruiseSection.show();
    hotelSection.show();
    $('#dropdown-button-flight').prop('disabled', false);
    cruiseTimeDropdown.prop('disabled', false);

    displayPrice();
    roundTrip = true;
}

function loadReservationoneWayFromAirport () {
    //Airport Validations
    flightTimeEntered = true
    flightDateEntered = true
    airlineSelected = true
    flightNumberEntered = true;
    cruiselineSelected = true;

    moveToNextBox(2, 999);
    moveToNextBox(3, 999);
    moveToNextBox(4, 999);
    moveToNextBox(9, 999);
    moveToNextBox(11, 999);

    $('#dropdown-button-type').text('One Way to Cruise');

    validated = true;
    travelType = options[1];

    flightSection.show();
    cruiseSection.hide();
    $('#dropdown-button-flight').prop('disabled', false);

    displayPrice();

    oneWayFromAirport = true;
    // If one way from airport we want to allow the user to select a cruise
    cruiselineDropdownOuterClone.show();
}

function loadReservationOneWayToHotel () {
    //Airport Validations
    flightTimeEntered = true
    flightDateEntered = true
    airlineSelected = true
    flightNumberEntered = true
    hotelSelected = true;

    moveToNextBox(2, 999);
    moveToNextBox(3, 999);
    moveToNextBox(4, 999);
    moveToNextBox(9, 999);
    moveToNextBox(11, 999);

    $('#dropdown-button-type').text('One Way to Hotel');

    validated = true;
    travelType = options[3];

    flightSection.show();
    cruiseSection.hide();
    hotelSection.show();
    $('#dropdown-button-flight').prop('disabled', false);

    displayPrice();

    oneWayToHotel = true;

}

//Make the circle with the old tag finished, and the new tag current.
function moveToNextBox (oldTag, newTag) {

    $(".finished[tag=" + oldTag + "]").attr('src', "/smart-images/check-mark-green.png");

    if (boxes[newTag] != "selected")
    {
        if (newTag == 5)
        {
            $(".finished[tag=" + newTag + "]").attr('src', "/smart-images/check-required.png");
        }
        else
        {
            $(".finished[tag=" + newTag + "]").attr('src', "/smart-images/check-current.png");
        }
    }

    $(".finished[tag=" + oldTag + "]").attr('height', 15);
    $(".finished[tag=" + oldTag + "]").attr('width', 15);

    boxes[oldTag] = "selected";
}

