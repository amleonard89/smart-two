var travelTypeKey = "travelTypeId";
var nameKey = "name";
var emailKey = "email";
var phoneNumberKey = "phoneNumber";
var specialRequestKey = "specialRequest";
var cruiseDateKey = "cruiseDate";
var cruiseTimeKey = "cruiseTime";
var cruiseShipKey = "cruiseShip";
var airportDateKey = "airportDate";
var airportTimeKey = "airportTime";
var airlineKey = "airline";
var flightNumberKey = "flightNumber";
var confirmationCodeKey = "confirmationCode";
var numberOfTravelersKey = "numOfTravelers";
var priceKey = 'price';
var hotelKey = 'hotel';

var ONE_WAY_TO_AIRPORT_ID = 0;
var ONE_WAY_TO_CRUISE_ID = 1;
var ONE_WAY_TO_HOTEL_ID = 3;
var ROUNDTRIP_ID = 2;
var THREEWAY_TRIP_ID = 4;

//Initialize all of our variables

// Trip Type Variables
// One way
var oneWayFromAirport = false;
var oneWayToAirport = false;
var oneWayToHotel = false;

// Round Trip
var roundTrip = false;
var roundTripAirportAndHotel = false;
var roundTripAllWays = false;

// ********** Reservation Form Status *********************
var cruiseDateEntered = false;
var cruiseTimeEntered = false;
var flightTimeEntered = false;
var flightDateEntered = false;
var hotelSelected = false;
var airlineSelected = false;
var cruiselineSelected = false;
var flightNumberEntered = false;
var numOfTravelersSelected = false;
var validated = false;

// *********** Travel Information **********************
var travelType;
var travelTypeId;
var price;
var originalPrice;
var boxes = [];

// HTML DOM elements
var hotelSection = $('.info.hotel');
var flightSection = $('.info.flight');
var cruiseSection = $('.info.cruise');
var smartButton = $('#smart-button');
var airportSoldOut = $('#airport-sold-out');
var cruiseSoldOut = $('#cruise-sold-out');
var priceSection = $('#price');
var priceTaglineSection = $('#price-tagline');
var cruiseTimeSoldOut = $('#cruise-time-sold-out');
var airportTimeSoldOut = $('#airport-time-sold-out');
var cruiselineDropdownOuterClone = $('#cruiseline-in-airport-section');
cruiselineDropdownOuterClone.hide();
var airlineDropdownOuterClone = $('#airline-in-cruise-section');
airlineDropdownOuterClone.hide();
var cruiseTimeDropdown = $('#dropdown-button-cruise');

var cruiseTime;
var flightTime;
var cruiseDate;
var flightDate;

var numOfTravelers;
var strNumOfTravelers;

//These are the different options that can be selected for trip type
var options = [ "ONE WAY SHUTTLE - Port Canaveral to Orlando Airport MCO",
    "ONE WAY SHUTTLE - Orlando Airport MCO to Port Canaveral",
    "ROUND TRIP SHUTTLE - Between Orlando Airport MCO and Port Canaveral",
    "ONE WAY SHUTTLE - Orlando Airport MCO to Cocoa Beach/Cape Canaveral Hotel",
    "ROUND TRIP SHUTTLE - Orlando Airport MCO to Cocoa Beach/Cape Canaveral Hotel | Port Canaveral Cruise to Orlando Airport"];
