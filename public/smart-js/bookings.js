function SmartBooking(data) {
    this._id = null;

    this.data = {
        'name'              : null,
        'phoneNumber'       : [],
        'specialRequest'    : null,
        'email'             : [],
        'confirmationCode'  : null,
        'referral'          : null,
        'notes'             : null,
        'scheduleNotes'     : null,
        'totalPrice'        : 0,
        'totalPaid'         : 0,
        'dateCreated'       : null
    };
    this.dom = null;
    this.trips = [];
    this.tripsUnloaded = false;
    this.payments = [];
    this.legacy = false;
    this.cancelled = false;
    this.quote = false;
    
    this.addTrip = function(trip) {
       trip = trip || new SmartTrip(this);

       this.trips.push(trip);
       trip.setParent(this);

       return trip;
    };
    this.delTrip = function(index) {
        var remove = this.trips.splice(index, 1);
        delete remove;

        return this;
    };

    this.getTrips = function() {
        return this.trips;
    };
    
    this.getTrip = function(index) {
        return this.trips[index];
    };
    this.getTripById = function(id) {
        for (var i=0; i<this.trips.length; i++) {
            if (this.trips[i].id == id)
                return this.trips[i];
        }
        return null;
    };
    this.tripCount = function() {
        return this.trips.length;
    };

    this.getTripDatetime = function(index) {
        return this.trips[index].getDatetime();
    };

    this.getTripDatetimeString = function(index) {
        var date = sBook.dateDisplayText(this.trips[index].getDatetime(), {twodigit : false});

        return date;
    };
    
    this.getTripIndex = function(id) {
        var len = this.trips.length;
        for (var i=0; i<len; i++) {
            var trip = this.trips[i];
            
            if (this.trips[i].id == id)
                return i;
        }
        
        return -1;
    };

    this.getTripPlaceString = function(i, type) {
        return this.trips[i].getPlaceString(type);
    };

    this.getTravelTypeString = function(index, mute) {
        index = typeof index == 'undefined' ? null : index;
        mute = mute || false;
        
        var mainTrip = [];
        for (var i=0; i < this.trips.length; i++) {
            var str = this.trips[i].getTravelTypeString();
            
            if (index!==null && i != index && mute) {
                str = '<span class="text-heavy-muted">'+str+'</span>';
            }
            mainTrip.push(str);
        }

        return mainTrip.join('\n<br />');
    };

    this.setPrice = function(price) {
        this.data.totalPrice = +price;
    };

    this.getPrice = function() {
        if (this.data.totalPrice)
            return this.data.totalPrice;

        var total = 0;
        //for (var i in this.trips) {
        for (var i=0; i < this.trips.length; i++) {
            total += this.trips[i].getPrice();
        }

        return total;
    };

    this.getPricePaid = function() {
        return +this.data.totalPaid;
    };
    
    this.setPricePaid = function(val) {
        this.data.totalPaid = val;
    };
    
    this.isSuggestedPrice = function() {
        var total = 0;
        //for (var i in this.trips) {
        for (var i=0; i < this.trips.length; i++) {
            total += this.trips[i].getPrice();//this.trips[i].getPriceSuggested();
        }
        return this.data.totalPrice == total;
    };
    
    this.setPhoneNumber = function(n) {
        if (typeof n === 'string') {
            this.data.phoneNumber = n.trim().split(/\n,\//g);
        }
        else {
            try {
                this.data.phoneNumber = n.trim();
            }
            catch (e) {
                this.data.phoneNumber = n;
            }
        }
    };
    
    this.getPhoneNumber = function(options) {
        options = options || {
            as_string : false
        };
        
        if (options.as_string)
            return this.data.phoneNumber ? this.data.phoneNumber.join("\n") : '';
        else
            return this.data.phoneNumber ? this.data.phoneNumber : [];
    };
    
    this.addPayment = function(payment) {
        if (typeof payment.paidDate == 'string')
            payment.paidDate = new Date(payment.paidDate);
        
        this.quote = false;
        this.payments.push(payment);
    };
    
    this.reset = function() {

    };

    this.load = function(data) {
        this._id = data._id;
        this.data.name = data.name;
        this.data.confirmationCode = data.confirmationCode;
        this.data.specialRequest = data.specialRequest;
        this.data.email = Array.isArray(data.email) ? data.email : [data.email];
        this.data.totalPrice = data.totalPrice;
        this.data.totalPaid = +data.totalPaid || 0;
        this.data.referral = data.referral;
        this.data.notes = data.notes;
        this.data.dateCreated = data.dateCreated;
        
        this.setPhoneNumber(data.phoneNumber);
        
        //for (var i in data.trips) {
        for (var i=0; i < data.trips.length; i++) {
            var trip = this.addTrip();

            if (typeof data.trips[i] === 'string') {
                this.tripsUnloaded = true;
                continue;
            }
           
            trip.setParent(this);
            trip.load(data.trips[i]);
        }
        
        //for (var i in data.payment) {
        if (data.payment) {
            for (var i=0; i < data.payment.length; i++) {
               this.addPayment(data.payment[i]); 
            };
        }
        if (data.legacy) {
            this.legacy = true;
        }
        if (data.cancelled) {
            this.cancelled = true;
        }
        if (data.quote) {
            this.quote = true;
        }
    };

    this.toJSON = function(prop) {
        var _ready = {};
        
        for (var k in this) {
            var v = this[k];
            
            if (k == 'dom' ||
                k =='parent')
                continue;

            if (typeof v == 'function')
                continue;
            
            _ready[k] = v;
        }
        
        return _ready;
    };
    
    // constructor
    (function(data){
        if (data)
        {
            if (data.booking) {
                var obj = data.booking;
                //obj.trips = [data];
                this.load(obj);
            }
            else {
                this.load(data);
            }
        }
    }).call(this, data);

    return this;
}

function SmartTrip(book) {
    this.id = null;
    
    this.data = {
        date         : {
            obj      : null,
            dateStr  :  null,
            timeStr  :  null
        },
        travelers    : null,
        priceId      : null,
        priceNum     : 0,
        drivers      : null,
        driverNotes  : '',
        origin       : { loc: null, place : null, cust : {} },
        dest         : { loc: null, place : null, cust : {} }
    };

    this.index = book ? book.tripCount() : 0;
    this.parent = book || null;
    this._dom = null;
    
    this.setParent = function(booking) {
        this.parent = booking; return this;
    };
    
    // date related
    this.setDatetime = function(datetime) {
        this.data.date.obj = datetime; return this;
    };
    this.setDate = function(date) {
        this.data.date.dateStr = date;
        _refreshDatetime.call(this);
    };
    this.setTime = function(time) {
        this.data.date.timeStr = time;
        _refreshDatetime.call(this);
    };
    
    var _refreshDatetime = function() {
        this.data.date.obj = new Date(this.data.date.dateStr + (this.data.date.timeStr ? ' '+this.data.date.timeStr+' '+sBook.TZ : ''));
    };
    
    this.getDate = function() {
        return this.data.date.dateStr;
    };
    this.getTime = function() {
        return this.data.date.timeStr;
    };
    this.getDatetime = function() {
        return this.data.date.obj;
    };
    
    this.getDatetimeString = function() {
        var date = sBook.dateDisplayText(this.getDatetime(), {twodigit : false});

        return date;
    };
    // place related
    this.setPlace = function(dir, id) {
        this.data[dir].place = id; return this;
    };
    this.setOriginPlace = function(id) {
        this.setPlace('origin', id); return this;
    };
    this.setDestPlace = function(id) {
        this.setPlace('dest', id); return this;
    };
    this.setLocation = function(dir, id) {
        this.data[dir].loc = id; return this;
    };
    this.setLocationByKey = function(dir, key) {
        this.data[dir].loc = sBook.lockeys[key]._id.toString(); return this;
    };
    
    this.getPlace = function(dir) {
        return this.data[dir].place ? this.data[dir].place.toString() : null;
    };
    this.getOriginPlace = function() {
        return this.getPlace('origin');
    };
    this.getDestPlace = function() {
        return this.getPlace('dest');
    };
    this.getLocation = function(dir) {
        return this.data[dir].loc;
    };
    this.getLocationKey = function(dir) {
        try {
            return sBook.loc[this.data[dir].loc].key;
        }
        catch (e) {
            return null;
        }
    };
    
    this.getPlaceString = function(type, internal) {
        var index = this.getPlace(type);

        var custData = this.getCustomData(type);
        var appendix = '';

        for (var i in custData) {
            switch (i) {
                case 'customFlightnum':
                    if (!custData[i])
                        continue;
                    
                    appendix += '(#'+custData[i]+')';
                    break;

                case 'customAddress':
                    appendix += custData[i].address + "<br />" +
                                custData[i].city + ", " + custData[i].state + " " +
                                custData[i].zip;
                    break;
            }
        }

        return !index ? (appendix ? appendix : '<i>None</i>') : (sBook.place[index] ? sBook.place[index].label : '') + (appendix ? ' '+appendix : '');
    };
    
    this.getPlaceInternals = function(type, properties) {
        var index = this.getPlace(type);
        
        if (properties) {
            if (!Array.isArray(properties)) properties = [properties];
        }
        else
            properties = [];
        
        var place = sBook.place[index];
        if (!place)
            return [];

        var data = [];
        if (place.internals) {
            for (var i in place.internals) {
                if (properties.length > 0 && properties.indexOf(i)===-1)
                    continue;
                
                data.push(place.internals[i]);
            }
        }
        
        return data;
    };
    
    this.getTravelTypeString = function() {
        var subTrip = [];
        var origin = this.getLocation('origin');
        var dest = this.getLocation('dest');

        if (origin)
            subTrip.push(sBook.loc[origin].label);

        if (dest)
            subTrip.push(sBook.loc[dest].label);

        var str = subTrip.join(' to ');

        return str;
    };
    
    // custom data related
    this.setOriginCustomData = function(key, data) {
        this.setCustomData('origin', key, data); return this;
    };
    this.setDestCustomData = function(key, data) {
        this.setCustomData('dest', key, data); return this;
    };
    this.setCustomData = function(dir, key, data) {
        if (key === true) {
            this.data[dir].cust = data;

        } else {
            this.data[dir].cust[key] = data;
        }
        return this;
    };
    this.getCustomData = function(dir, key) {
        return key ? this.data[dir].cust[key] : this.data[dir].cust;
    };
    this.clearCustomData = function(dir) {
        this.data[dir].cust = {};
    };

    // price related
    this.setPrice = function(price) {
        this.data.priceNum = price;
        return this;
    };
    this.getPrice = function() {
        return this.data.priceNum;
    };
    this.setPriceId = function(priceId) {
        this.data.priceId = priceId;
        return this;
    };
    this.getPriceId = function() {
        return this.data.priceId;
    };
    this.getPriceSuggested = function() {
        sBook.init();
        try {
            var pp = sBook.tripPriceCalc(this.data.priceId, this.index, this.parent);
            return pp;//sBook.price[this.data.priceId].price;
        }
        catch (e) {
            return 0;
        }
    };
    this.isSuggestedPrice = function() {
        return this.data.priceNum == this.getPriceSuggested();
    };

    this.setNum = function(num) {
        if (!num) return;
        
        this.data.travelers = num;
    };
    
    this.getNum = function() {
        return this.data.travelers;
    };
    
    // dom related
    this.dom = function() {
        return this._dom;
    };
    this.setDom = function(dom) {
        this._dom = dom;
        
        return this;
    };

    this.load = function(data) {
         var baseDate = new Date(data.dateTime);

        var dateStr = sBook.dateDisplayText(baseDate, { time : false });
        var timeStr = sBook.dateDisplayText(baseDate, { date : false, twodigit : false });

        this.id = data._id;

        this.setDate(dateStr);
        this.setTime(timeStr);
        this.setDatetime(baseDate);
        
        this.setPrice(data.price);
        this.setNum(data.travelers);

        this.setLocation('origin', data.origin.location);
        this.setLocation('dest', data.destination.location);

        if (data.priceId)
            this.setPriceId(data.priceId.toString());

        if (data.origin.place)
            this.setOriginPlace(data.origin.place.toString());

        if (data.origin.custom)
            this.setCustomData('origin', true, data.origin.custom);

        if (data.destination.place)
            this.setDestPlace(data.destination.place.toString());

        if (data.destination.custom)
            this.setCustomData('dest', true, data.destination.custom);

        this.data.drivers = data.drivers;
        this.data.driverNotes = data.driverNotes;
        
        this.data.scheduleNotes = data.scheduleNotes === null && this.parent ? this.parent.data.specialRequest : data.scheduleNotes;
        
        return this;
    };
    
    this.toJSON = function(prop) {
        var _ready = {};
        
        for (var k in this) {
            var v = this[k];
            
            if (k == '_dom' ||
                k =='parent')
                continue;

            if (typeof v == 'function')
                continue;
            
            _ready[k] = v;
        }
        
        return _ready;
    };
    
    return this;
};

var sBook = new (function() {
    // internal
    this._load = false;
    this._state = $.Deferred();

    this.TZ = '-0500';
    this.TZS = 'America/New_York';
    this.TZO = ((-500/100) * 60);
    
    this.loc = {};
    this.lockeys = {};
    this.place = {};
    this.price = {};
    this.priceMap = {};

    this.init = function() {
        var _this  = this;

        if (_this._load)
            return this.promise();

        var promise = $.get('workshop/data');

        promise.then(function(data) {
            for (var i in data.locations) {
                if (!data.locations.hasOwnProperty(i)) continue;
                
                _this.loc[i] = data.locations[i];
                _this.lockeys[data.locations[i].key] = _this.loc[i];
                
                for (var j in data.locations[i].places) {
                    _this.place[j] = data.locations[i].places[j];
                }
            }
            
            for (var i in data.prices) {
                if (!data.prices.hasOwnProperty(i)) continue;
                
                var _price = data.prices[i];

                _this.price[_price._id] = _price;

                if (!_this.priceMap[_price.from])
                    _this.priceMap[_price.from] = {};
                if (!_this.priceMap[_price.from][_price.to])
                    _this.priceMap[_price.from][_price.to] = {};

                _this.priceMap[_price.from][_price.to][_price.passengers] =
                    _price;
            }
            
            _this._state.resolve(_this);
        },
        function(err){
            _this._load = false;
        });
        _this._load = true;
        
        return this.promise();
    };

    this.tripPriceCalc = function(priceId, index, booking) {
        var price;
        try {
            if (typeof priceId == 'string')
                price = sBook.price[priceId];
            else
                price = priceId;

            price.price; //throw exception if not there, invalid object!
        }
        catch (e) {
            return false;
        }

        if (price.specials.length == 0) {
            return +price.price;
        }

        var usedPrices = [];
        for (var i = 0; i < +index; i++) {
            var pId = booking.getTrip(i).getPriceId();
            if (pId) {
                var path = sBook.price[pId].from+'-'+sBook.price[pId].to;
                usedPrices.push(path);
            }
        }

        if (usedPrices.length === 0)
            return +price.price;

        var special = true;
        for (var i=0; i < price.specials.length; i++) {
            var specPaths = [];
            
            for (var k=0; k<price.specials[i].from.length; k++) {
                specPaths.push(price.specials[i].from[k]+'-'+price.specials[i].to[k]);
            }

            var special = true;
            for (var j=0; j<specPaths.length; j++) {
                if (usedPrices.indexOf(specPaths[j]) === -1) {
                    special = false;
                    break;
                }
            }
            if (special) {
                return +price.specials[i].price;
            }
        }

        return +price.price;
    };

    this.promise = function() {
        return this._state.promise();
    };

    this.dateDisplay = function(date) {
        var smOffset = ((+sBook.TZ/100) * 60)
        var myOffset = date.getTimezoneOffset();

        var displayOffset = (smOffset + myOffset)/60 * 100;
        var displayDate = new Date(date.toUTCString()+'-'+displayOffset);

        return displayDate;
    };
    
    this.dateDisplayText = function(date, opts) {
        var _settings = {
            'twodigit' : true,
            'time' : true,
            'date' : true
        };
        
        $.extend(_settings, opts);

        var smOffset = ((+sBook.TZ/100) * 60);

        var _date = moment(date).utcOffset(smOffset);
        
        var _format = '';
        
        if (_settings.date)
            _format += _settings.twodigit ? 'MM/DD/YYYY': 'M/D/YYYY';
        
        if (_settings.date && _settings.time)
            _format += ' ';
        
        if (_settings.time)
            _format += _settings.twodigit ? 'hh:mm A' : 'h:mm A';
        
        return _date.format(_format);
    };
    
    return this;
})();
